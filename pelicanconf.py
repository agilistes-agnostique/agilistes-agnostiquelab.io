#!/usr/bin/env python
# -*- coding: utf-8 -*- #

# Doc de référence : https://docs.getpelican.com/en/stable/settings.html

# Quand un article n’a pas d’auteur on lui attribu ce dernier par défaut
AUTHOR = 'Matthieu'

# Nom du site
SITENAME = 'Anargile & Leanbertaire'

# URL du site. Redéfini dans le publishconf.py pour la mise en prod
SITEURL = ''

# Liste des extensions utilisées
PLUGINS = ['neighbors','webassets', 'search', 'simple_footnotes']

# Voir
# https://python-markdown.github.io/extensions/
# https://jackdewinter.github.io/2019/10/16/fine-tuning-pelican-markdown-configuration/
MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.extra': {},
        'markdown.extensions.admonition': {},
        'markdown.extensions.codehilite': {
            'css_class': 'highlight'
        },
        'markdown.extensions.meta': {},
        'smarty' : {
            'smart_angled_quotes' : 'true'
        },
         'markdown.extensions.toc': {
            'anchorlink' : 'true'
        },
    }
}

# Thème du site utilisé
THEME = 'editorial'

TYPOGRIFY = True

# Répertoire ou mettre les contenus générés
PATH = 'content'
OUTPUT_PATH = 'public'

# Configuration des URLs suivant le type de contenu
ARTICLE_SAVE_AS = 'articles/{slug}.html'
PAGE_SAVE_AS = 'pages/{slug}.html'
DRAFT_SAVE_AS = 'drafts/{slug}.html'
AUTHOR_SAVE_AS = 'authors/{slug}.html'
CATEGORY_SAVE_AS = 'categories/{slug}.html'
TAG_SAVE_AS = 'tags/{slug}.html'

ARTICLE_URL = 'articles/{slug}.html'
PAGE_URL = 'pages/{slug}.html'
DRAFT_URL = 'drafts/{slug}.html'
AUTHOR_URL = 'authors/{slug}.html'
CATEGORY_URL = 'categories/{slug}.html'
TAG_URL = 'tags/{slug}.html'

# Conf de la localisation
TIMEZONE = 'Europe/Paris'
DEFAULT_LANG = 'fr'

# Pagination. Mettre un multiple de 3 + 1 pour le thème Editorial
DEFAULT_PAGINATION = 31

# Pour générer automatiquement un résumé a afficher
SUMMARY_MAX_LENGTH = 50
SUMMARY_END_SUFFIX = '…'

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
DELETE_OUTPUT_DIRECTORY = True

# Feed generation is usually not desired when developing
#FEED_DOMAIN = SITEURL
FEED_ATOM = None
FEED_RSS = None
FEED_ALL_ATOM = None
FEED_ALL_RSS = None
CATEGORY_FEED_ATOM = None
CATEGORY_FEED_RSS = None
TRANSLATION_FEED_ATOM = None
TRANSLATION_FEED_RSS = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),
)

# Social widget
SOCIAL = (
    ('twitter', 'https://twitter.com/'),
    ('linkedin', 'https://www.linkedin.com'),
    ('github', 'https://github.com/'),
    ('gitlab', 'https://gitlab.com/'),
    ('facebook', 'https://facebook.com'),
    ('instagram', 'https://instagram.com')
)

# Article share widgets
SHARE = (
    ("linkedin", "https://www.linkedin.com/shareArticle?mini=true&url="),
    ("twitter", "https://twitter.com/share?url="),
    ("facebook", "https://www.facebook.com/sharer.php?u=")
)

ABOUT = 'Nous sommes un groupement de professionnels de l’Agilité et de la transformation des entreprises au sens large. Dans une démarche de partage du savoir nous exposons notre point de vu et expériences au travers de ce blog. N’hésitez pas a nous contacter pour nous dire ce que vous en pensez !'

CONTACT = (
    ('envelope', '<a href="mailto:contact@lerteco.fr?Subject=Contact&Body=Bonjour,%0AJe%20voudrais%20en%20apprendre%20plus%20sur%20votre%20blog">contact@lerteco.fr</a>'),
    ('phone','+01 345-345-345'),
    ('home', '42 rue de l’oublie, Moncug')
)

# Conf pour le suivi des visites
#MATOMO_URL='stats.anargile-leanbertaire.info'
#MATOMO_SITE_ID='3'

# Pour le moteur de recherche
#SEARCH_MODE = "output"
#SEARCH_HTML_SELECTOR = "main"
STORK_INPUT_OPTIONS = {
    "html_selector" : "main",
    "stemming" : "French"
}

STATIC_PATHS = ['images']
