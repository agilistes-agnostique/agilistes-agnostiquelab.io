Title: Déclaration des droits Agile
Date: 2024-02-22
Tags: Agile Proprement, Robert C. Martin, livre
Keyword: livre, droits
Slug: Declaration-des-droits-Agile
Author: Matthieu
Description: En plus du manifeste Agile, certains signataires de ce document ont écrit la très méconnue déclaration des droits Agile. Elle dicte les attentes possibles des développeurs et des clients.
Summary: En plus du manifeste Agile, certains signataires de ce document ont écrit la très méconnue déclaration des droits Agile. Elle dicte les attentes possibles des développeurs et des clients.
Status: published
Cover: /images/Declaration_droits.jpg
Cover_Source: https://fr.wikipedia.org/wiki/Droits_de_l'homme_en_France

# Introduction

![Agile Proprement]({attach}AgileProprement.jpg){.image .right}

Qui, parmi tout agiliste qui se respecte, ne connait pas le Manifest Agile ! Si vous lisez ces lignes, ce document ne vous est pas inconnu. Vous en connaissez peut-être certaines valeurs, voire toutes et pour les plus académiques, les principes … par cœur.

Mais connaissez vous la déclaration des droits Agile ? Non ? Ce n’est pas surprenant, cette déclaration ne s’est manifestement pas fait une place sous les projecteurs comme l’a fait son cousin le manifeste. Pourquoi je dis cousin ? Car c’est une partie des signataires du manifeste, lors de cette même réunion à Snowbird qui ont écrit cette déclaration. (« sous la plume de Kent Beck, Ward Cunningham, Ron Jeffries et quelques autres »)

C’est en lisant l’excellent «[Agile Proprement](https://www.pearson.fr/book/?gcoi=27440100017670)» de Robert C. Martin que je l’ai découvert. Si vous ne l’avez pas encore lu, je ne peux que vous encourager à vous procurer un exemplaire de ce livre et à le dévorer, comme une histoire captivante que vous raconterait votre grand-oncle baroudeur. Je ferais surement un billet spécial pour en parler plus longuement, mais pour l’instant, je voudrais vous faire connaitre cette pépite méconnue qu’est la déclaration des droits Agile, découverte après les 50 premières pages de ce livre.

# Déclaration des droits

| Droits du client | Droits du dévelopeur |
| ---------------------- | ------------------------------- |
| Vous avez le droit d’obtenir un plan d’ensemble et de savoir ce qui peut être réalisé, quand et à quel coût. | Vous avez le droit de savoir ce que l’on attend de vous, avec une déclaration claire des priorités. |
| Vous avez le droit d’obtenir le plus possible de valeur à la fin de chaque itération. | Vous avez le droit de produire du code de grande qualité à tout moment. |
| Vous avez le droit de pouvoir constater l’avancement d’un système fonctionnel, dont la qualité a été prouvée par des tests reproductibles que vous avez spécifiés. | Vous avez le droit de demander et de recevoir de l’aide de vos collègues, vos managers et vos clients. |
| Vous avez le droit de changer d’avis, de remplacer une fonctionnalité par une autre et de modifier les priorités sans subir un surcoût exorbitant. | Vous avez le droit de faire vos propres estimations et de les mettre à jour. |
| Vous avez le droit d’être informé à tout moment des changements dans le planning et dans les estimations, et suffisamment à l’avance pour pouvoir décider de réduire la portée fonctionnelle si vous avez besoin de faire respecter une date fixe. Vous devez pouvoir annuler le projet à tout moment et obtenir un système fonctionnel en proportion du budget déjà consommé. | Vous avez le droit d’accepter les responsabilités au lieu de vous les voir infliger. |

J’ai volontairement présenté les droits des 2 parties sous forme de tableau, car, comme le dit l’auteur du livre, chaque droit se répond et vient compléter le droit de l’autre partie. Je vous laisserais découvrir les explications détaillées de chaque droit dans le livre, mais sans le paraphraser, je vais me permettre de vous donner ma vision.

# Point de vue

1. (client) Aussi étonnant que cela puisse paraître, il commence par nous dire que l’on doit faire des estimations et sur l’ensemble du projet ! Mais avec une certaine nuance, cette estimation doit être la plus précise et exacte possible, dans l’état actuel des connaissances de l’équipe. Elle ne doit pas non plus conduire à un engagement de date ou de périmètre. Il recommande de fournir des probabilités de délai de réalisation. Ce qui permet au client un pilotage correct de ses activités  
(dev) De leur côté, c’est la prioritisation qui est mise à l’honneur. Cette précieuse connaissance peut sembler en opposition à l’accueil du changement qui est normal en agilité. C’est pourquoi elle ne peut être fixe pour l’ensemble du projet, seulement, au minimum, pour l’itération en cours, au mieux pour 2 ou 3 itérations.

2. (client) C’est un droit qui se mord un peu la queue, puisque c’est au client de prioriser la production, mais c’est de la responsabilité des dev de la respecter  
(dev) C’est un droit qui est souvent rogné, alors qu’il est essentiel et inaliénable. Cela touche à la raison d’être de tout développeur qui se respecte. Le bafoué se paye cher sur le long terme.

3. (client) Tout client qui effectue un minimum de suivi devrait être très exigent avec ce droit. C’est son argent et son temps que les dev consomment. Autant vérifier qu’il est bien utilisé.  
(dev) La communication est le centre de ce droit. Communiquer les connaissances, les exigeances, les priorités. Sans cela, comment exiger de dev des estimations, de la qualité, de la production de valeur.

4. (client) De par la nature non matérielle d’un logiciel, tout changement devrait être possible, tout en acceptant les surcoût engendrés.  
(dev) C’est ceux qui font qui savent ! Et c’est en faisant que l’on peut mieux apprécier la charge de travail nécessaire. Toutefois, cela reste une estimation, qui peut être fausse ou inexacte, donc pas un engagement ferme.

5. (client) Le droit du client ici est d’être tenu informé, il n’a pas le droit d’exiger la tenue des délais. Cela lui est essentiel pour réagir en fonction des évènements, quitte à changer d’avis, d’orientation ou carrément annuler  le projet si nécessaire, sans que cela puisse lui être reproché. Ce n’est finalement que le droit d’être un bon gestionnaire  
(dev) D’un côté, on peut y voir du Lean avec un flux tiré. D’un autre côté, on peut y voir l’appréciation du dev. Se sent-il légitime ? La demande est elle légitime ? Vas t-elle à l’encontre de ses convictions ? Ce droit est finalement assez important et lourd de sens. Personne ne devrait être obligé (ruse ou manipulation inclus) de réaliser une tâche qui va à l’encontre de son bien-être.

# Conclusion

Cette déclaration des droits des clients et des dev n’est pas révolutionnaire. Elle vient appuyer et renforcer ce que l’on trouve déjà dans le manifeste. Mais son angle d’attaque différent permet de mieux éclairer la relation que devraient avoir les clients et les équipes de dev. Elle a été écrite dans cette optique, « réparer la fracture entre l’entreprise et les développeurs ».
