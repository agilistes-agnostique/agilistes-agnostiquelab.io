Title: Agile Tour Aix Marseille 2023
Date: 2023-12-31
Tags: Agile  Tour Aix Marseille, Conférences
Keyword: Agile Tour,  Aix Marseille
Slug: Agile-Tour-Aix-Marseille-2023
Author: Matthieu
Description: Retour en images de l’édition 2023 d’Agile Tour Aix Marseille. Avis tout a fait subjectif qui vous fait vivre de l’intérieur cet évènement.
Summary: Retour en images de l’édition 2023 d’Agile Tour Aix Marseille. Avis tout a fait subjectif qui vous fait vivre de l’intérieur cet évènement.
Status: published
Cover: /images/ATAM2023.jpg

Pour la dernière convention de l’année, j’ai essayé d’aller chercher le soleil à Aix & Marseille, mais malheureusement, je ne l’a pas trouvé. Bon, faut dire que le 7 décembre n’est pas la meilleure période pour ça.
Avant toute chose, je tiens à remercier grandement [Olivier Lequeux](https://www.linkedin.com/in/olivierlequeux) et son compatriote [Romain Vignes](https://www.linkedin.com/in/rvignes/) qui m’ont grandement facilité mes déplacements entre la gare et le lieu de l’évènement.
Tout se passe à l’école des mines de Saint-Etienne à Gardanne … Oui, je sais, ça m’a aussi fait tiquer « Saint-Étienne » et en fait, c’est bien un second campus de l’école des mines de la ville éponyme.

![Cafette]({attach}20231207_085718.jpg){.image .width-75}

Pour les plus observateurs d’entre vous, vous aurez remarqué sur la photo pas mal de personnes récurrentes dans ce genre d’évènement. ;)

Cette fois-ci, nul besoin de prévoir de logement, puisque ça se passe sur une seule journée. Et la liaison Lyon - Aix se fait assez rapidement, comptez 1h30 par trajet.
La journée s’ouvre avec une collation matinale des plus riches et un mot des orgas dans la même salle. Pas de keynote, on passe directement aux orateurs de notre choix. Avoir nommé les différentes salles de grands noms de l’agilité est une idée intéressante (Ron Jeffries, Kent Beck, Jeff Sutherland, Henrik Kniberg, Alistair Cockburn, Martin Fowler).
Pour ma part, j’ai fait ma groupie (avec d’autre) pour aller voir [Rébecca Carlier](https://www.linkedin.com/in/r%C3%A9becca-carlier-7590322b) avec « L’art de la rétro ». Son intro était sympa avec un rappel des événements historiques autour de ce sujet, en mettant le tout en parallèle d’autres évènements plus légers (aka Spices Girls 🤣, etc). La partie finale sur les conseils pour mener a bien cet évènement était pertinente et bienvenue, même si je ne suis pas d’accord avec l’idée qu’un animateur peut participer aussi (i.e. Scrum Master).

| | |
| - |-|
| ![Bettrave]({attach}20231207_093020.jpg){.image .fit} | ![Bettrave2]({attach}20231207_101522.jpg){.image .fit} |


Viens ensuite la raison principale de ma venue. La tenue d’un atelier « Paradice » avec mes compatriotes [Jean-Yves Klein](https://www.linkedin.com/in/jean-yves-klein) & [Yannis Martin](https://www.linkedin.com/in/yannismartin). Un inconvénient de cet atelier est qu’il faut absolument un nombre pair de participants… ce que nous n’étions pas, je me suis dévoué pour faire le complément 😁 Position qui finalement est plus confortable qu’animateur, car en plus de ne pas courir partout, on doit se brider pour ne pas divulguer les tenants et aboutissants de l’atelier. Dans l’ensemble, ça s’est bien passé. Les participants ont débatu longement … trop … et parfois au mauvais moment pour les animateurs. D’un atelier qui peut facilement durer 2 heures, voire plus. On a dû le contraindre à 45 min maximum pour avoir un peu de temps de débrief. Pour en avoir discuté plus tard dans la journée avec des cobayes, ils en étaient plutôt contents et ont bien perçu les leçons de l’atelier.

| | |
| - |-|
| ![Atelier1]({attach}20231207_104116.jpg){.image .fit} | ![Atelier2]({attach}20231207_105214.jpg){.image .fit} |
| ![Atelier3]({attach}20231207_111143.jpg){.image .fit} | ![Atelier4]({attach}20231207_111849.jpg){.image .fit} |

Après une petite pause de 15 min pour reprendre des forces, je suis allé voir « Kanban : un potentiel inexploité » avec [Olivier My](https://www.linkedin.com/in/oliviermy) & [François Boulon](https://www.linkedin.com/in/fran%C3%A7ois-boulon-127b008) et je n’ai franchement pas été déçu. De base, présenter un sujet à 2 (ou plus) n’est pas un exercice facile pour se transmettre la parole de manière fluide, mais là, ils ont très bien réussi. Toute la présentation repose sur la dualité des orateurs, chacun venant avec son expérience, ses exemples, ses conseils, et ce, sans jamais se marcher dessus. Ils ont commencé par une bonne mise en abîme pour expliquer le fonctionnement & la mise en pratique de Kanban. Cette introduction a été illustrée doublement par les orateurs. Ensuite, ils ont passé en revue chaque partie de la méthode :

- Workflow : Le réduire et simplifier autant que possible.
- WIP : Le définir et l’ajuster constamment.
- Gestion des flux : Passage en revue des différents graph d’analyse du flux. REX : entre 10 et 30 éléments suffisent pour dégager une tendance
- Transparence du processus : sources des demandes / priorisation et engagement / ligne de nage / système tiré
- Retour des utilisateurs : 3 boucles -> Top 10 business (15j) / Daily / Rétro (mensuel).

Enfin, ils ont fini par le Kanban Maturity Model qui est une très bonne boussole pour savoir si on exploite pleinement Kanban.

| | |
| - |-|
| ![Kanban1]({attach}20231207_114725.jpg){.image .fit} | ![Kanban2]({attach}20231207_115145.jpg){.image .fit} |
| ![Kanban3]({attach}20231207_122349.jpg){.image .fit} | ![Kanban4]({attach}20231207_124254.jpg){.image .fit} |

---
# Après midi

Après pas mal d’hésitation, je me laisse tenter par la présentation de [Julien Derose](https://www.linkedin.com/in/julien-deroses) et sa « Magie & Communication », mais il semble que le sujet attire, car la salle était pleine à craquer. On sentait l’orateur un peu fébrile au début, mais il a pris confiance petit à petit. Il entrecoupait sa présentation par des tours de magie. Exercice plutôt bien maitrisé puisque, même à faible distance et avec une caméra en gros plan sur ses mains, je n’ai pas vu la plupart des trucs qu’il a utilisés. Mais c’est là le plus gros défaut de sa présentation. Ces coupures nous « sortaient » de son sujet et on se focalisait sur son tour de magie. On essayait de trouver le truc de ses tours. Il est indéniable qu’il est original de lier les 2 sujets, mais je lui ai recommandé de vraiment fusionner ces 2 côtés, en essayant de mieux intégrer ses tours à son discours. Je suis tout à fait conscient que ce n’est pas chose facile, mais sa présentation y gagnerait grandement. Surtout que son tour final avec l’enveloppe est excellent 🤩, mais je ne vous en dirais pas plus 😝.

| | |
| - |-|
| ![Magie1]({attach}20231207_140116.jpg){.image .fit} | ![Magie2]({attach}20231207_143159.jpg){.image .fit} |
| ![Magie3]({attach}20231207_143839.jpg){.image .fit} | ![Magie4]({attach}20231207_143850.jpg){.image .fit} |

Je me suis ensuite dirigé vers la présetation de [Lilian Roche](https://www.linkedin.com/in/lilianroche) avec « L'évolution du rôle de Product Owner : du tacticien au stratège ». Il a logiquement commencé par une définition du rôle (garantie la vision du produit / Maintient le product backlog / Maximise la valeur produit) puis nous a donné sa vision de l’évolution logique d’un PO (Scribe < Proxy PO < Représentant business < Sponsor < Entrepreuneur). Il nous a aussi décrit ce qui, selon lui, est la bonne posture de ce rôle (décideur + expérimentateur + visionnaire + représentant des utilisateurs + collaborateur + influenceur). Autant sur le fond de la présentation, je n’ai pas grand chose a dire, on sent que le sujet est maitrisé, détaillé, expérimenté, autant sur la forme, je pense qu’il y a de la marge de progression. Il regardait trop ses slides, on sentait qu’il n’était pas à l’aise a regarder le public dans les yeux. Je trouvai aussi qu’ il se déplaçait beaucoup. Cela lui permettait t-il de réduire son stress ? Enfin, son timbre vocal n’était pas assez dynamique, le rythme un peu trop monotone. C’est vraiment dommage, car le font est riche et intéressant. Je suis peut-être un peu critique, mais c’est que j’ai vraiment trouvé cette conférence intéressante. Et je ne suis sûrement pas le seul à l’avoir trouvé intéressante puisqu’il avait des paparazzi pour lui 😄

| | |
| - |-|
| ![PO1]({attach}20231207_145857.jpg){.image .fit} | ![PO2]({attach}20231207_153239.jpg){.image .fit} |


Et … c’est tout. Oui, je sais, il reste un créneau normalement, mais je devais partir assez tôt avec Romain & Olivier pour ne pas louper mon train. Je vous laisse donc avec les tondeuses écologiques du campus 🐑

| | |
| - |-|
| ![PO1]({attach}20231207_162404.jpg){.image .fit} | ![PO2]({attach}20231207_162608.jpg){.image .fit} |




