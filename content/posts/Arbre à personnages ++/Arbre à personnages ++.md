Title: Arbre à personnages ++
Date: 2023/02/17
Modified: 2024/10/07
Tags: IceBreaker, Pip Wilson
Keyword: Icebreaker, échauffement, arbre à personnages
Category: Échauffement
Slug: Arbre à personnages ++
Author: Matthieu
Summary: Activité incontournable de tout agiliste, mais en connaissez-vous son origine ? Il est tellement utilisé que l'on se demande comment lui rendre toute sa magie. Donc, en plus de devenir incollable sur ce classique, vous repartirez avec quelques sources d'inspirations.
Cover: /images/Arbre-a-personnages.jpg
Cover_Source: https://unsplash.com/fr/photos/jouet-en-peluche-singe-brun-accroche-a-une-branche-darbre--2DEAN7f7uM
Status: published

Avec ce titre, on pourrait croire que vous allez entrer dans une autre dimension de cette activité. Je n'ai pas la prétention de ça. Il faut prendre le "++" au sens informatique du terme, soit un incrément (+1) sur la maîtrise de cette activité, ce qui est déjà bien.

## Origine

Pas évident de trouver les origines de quelque chose de tellement connu et utilisé. On est noyé sous les ressources sans pouvoir en remonter le fil. Ressources se citant souvent les unes aux autres et utilisant presque toujours la même illustration.

Comme je me doutais que cette activité n'était pas franco-française, j'ai commencé par chercher sa traduction en anglais, mais "peoples tree" ne donnait pas grand chose d'intéressant, "compagny tree" ressortait de temps en temps mais sans me donner plus de piste. Finalement une image vaut toujours mieux qu'un long discours et c'est en essayant de trouver l'auteur de l'image ci dessous que je suis tombé sur le site de [Pip Wilson](https://www.pipwilson.com) ce qui m'a permis de remettre un peu d'ordre dans tout ça.
![Arbre à personnages]({attach}arbre-a-personnages.jpg){.image .width-50}

Je note que cette personne est TRÈS active sur son blog. Ouvert en 2003 et jusqu'à 2022 il y a 16 655 billets !!! Soit une moyenne de presque 833 par an ! Soit environ 2,3 par jour ! Bon, en 2022 il n'a posté "que" 228 billets et rien en 2023 pour l'instant. Certes, ses billets sont souvent juste une ligne ou une image, mais ça reste impressionnant de longévité et d'activité.

Donc, [Pip Wilson](https://www.pipwilson.com/2012/11/a-pip-wilson-biography-november-2012.html) est un psychologue britannique et auteur de très nombreux livres dans ce domaine. Il a commencé sa carrière dans les "[Yong Offenders Unit](https://en.wikipedia.org/wiki/His_Majesty%27s_Young_Offender_Institution)" (unité carcérale pour jeune délinquant) et a enchaîné dans les [YMCA](https://fr.wikipedia.org/wiki/Young_Men%27s_Christian_Association). Sans surprise, toute sa pédagogie a été faite pour les enfants et jeunes en difficultés. Ce qui l'a amené à créer des "blob", petits personnages humanoïdes, asexués, agenrés, aculturel, ... bref, les plus socialement neutres possibles pour que tout le monde puisse s'y identifier facilement. Ces derniers sont par contre assez expressifs corporellement et émotionnellement, ils sont mis en situation dans des lieux divers et variés. L'idée de Pip Wilson est que les enfants ne savent pas ou mal exprimer leurs sentiments. Il est donc bien plus facile de les projeter sur ces "blob" et d'en parler comme une tierce personne. Il s'avérera que même pour les adultes, ça facilite beaucoup les choses.  
Il a tout de même fait [24 livres avec ses blobs](https://www.lulu.com/search?contributor=Ian+Long+Pip+Wilson&page=1&pageSize=30&adult_audience_rating=00). Quand je vous disais que le monsieur était prolifique.

![Roue des émotions]({attach}Roue des émotions.jpg){.image .width-50}

> Et l'arbre à personnages est extrait de sa pédagogie ?

Exactement. C'est celui qui a remporté le plus grand succès. Il en a même fait [un site dédié](https://www.blobtree.com) pour y promouvoir tout son matériel pédagogique, dont le fameux arbre à personnage que tout le monde connaît. Si vous voulez varier vos illustrations, sachez qu'il vend [169 mises en situations](https://www.blobtree.com/collections/singles-collection) de ses petits personnages. À la plage, montagne, piscine, sur un pont, en ville, ...

> Et ça date de quand ?

Je ne sais pas exactement. Il y a [un billet de blog](https://www.pipwilson.com/2004/11/blob-tree_110181146915869209.html) qui date du 30 novembre 2004, mais je suppose que ça doit être bien plus ancien. Et en lisant bien le billet, on trouve une reproduction d'un article du "Times Educational Supplement" de janvier 2006 qui cite Pip Wilson comme ceci :

> "This picture is over 25 years old," says Pip Wilson. "It emerged from my youth work in East London with young people unwilling or unable to read and has circulated since among teachers and other professionals often in the shape of photocopies of photocopies. Now we have greatly extended the range of Blob situations and scenarios - though all offer the same multiplicity of interpretation that made the original so useful a tool."

L'article faisait la promotion d'une nouvelle édition du livre des blobs illustré par Ian Long. Donc, on peut raisonnablement penser que l'arbre à personnage original date du tout début des années 1980.
Ce qui conforte aussi une information que j'ai vu qu'une des premières éditions d'un livre de blob date de 1985 (désolé, je n'ai plus la source). Je n'ai pas l'âge du monsieur, mais au vu de ses photos, ça me semble tout à fait crédible.

## L'atelier

Penchons-nous sur l'atelier en lui même maintenant. Je ne vais pas vous l'expliquer, mais plus relever des points d'attention suite à ce que j'ai vu ou vécu.

[Certaines sources](https://sain-et-naturel.ouest-france.fr/choisissez-un-personnage-et-nous-vous-dirons-quel-genre-de-personne-vous-etes.html) répondront pour vous suivant ce que vous avez choisi, ça ressemble tellement à de la psychologie de comptoir que l'on ne pense pas trouver ça en atelier. Mais la réalité dépasse souvent la fiction.  
Une personne ayant la langue bien pendue voudra interpréter les personnages de son équipe avant même que ceux ci ne puissent s'exprimer. Ne le laissez surtout pas faire, faites lui remarquer que son intervention est déplacée et surtout reposez clairement la question pour que la bonne personne reprenne son explication depuis le début.

Unicité des personnages. Il est possible que des personnes prennent un personnage par défaut au lieu de celui qui lui ressemble vraiment, tout ça parce qu’une autre personne l'avait déjà pris. Donc, n'oubliez pas de dire que les personnages sont multi-affectables, même si les interprétations peuvent être différentes.

Absence d'identification. Il n'est pas rare qu'il soit difficile de s'identifier à un personnage, mais ne pas s'inclure dans le dessin vous fait automatiquement exclure du groupe. Pour palier à ce problème, vous pouvez avoir en réserve d'autres personnages à choisir et placer en autonomie. Mais je vous explique tout ça dans le paragraphe des variations.

## Variations

Initialement, pour ce billet, je voulais me contenter de lister plusieurs variations de l'arbre à personnage que j'utilisais. L'original est bien, mais un peu de variété ne fait pas de mal. Mais j'ai tiré sur le fil et pondu le billet que vous venez de lire.
Sinon quelques ressources bien utiles pour varier cet exercice.

Si vous souhaitez soutenir le travail de Pip Wilson vous pouvez acheter les illustrations sur [son site directement](https://www.blobtree.com/collections/singles-collection/products/blob-tree).

|||||
| :---------------- | :------------------- | :----------------- ||
|![blob tree 1]({attach}blob_tree_1_example.jpg)|![blob tree 2]({attach}blob_tree_2_example.jpg)|![blob tree 3]({attach}blob_tree_3_example.jpeg)|![blob tree 13]({attach}blob_tree_13_example.jpg)|

Il y a tout de même des illustrations gratuites sur son site en cherchant bien. [Comme celle du football](https://www.pipwilson.com/2010/04/blob-football-file-download-jpeg-format.html) :
![Blob footbal]({attach}blob football_0001.jpg){.image .width-50}

Ensuite, quelques ressources que j'ai collectées au fil de mes pérégrinations.

|[FlexJob]( https://flexjob.fr/le-speed-boat-outil-facilitation/)| [Mathilde Riou](https://mathilderiou.com/resources/) | [Atelier collaboratif](https://atelier-collaboratif.com/telechargements/MUR-HUMEUR.jpg) |
| :---------------- | :------------------- | :----------------- |
|[![bateau personnages flexJob]({attach}Bateau personnages FlexJob.png)](https://flexjob.fr/le-speed-boat-outil-facilitation/)|[![bateau des humeurs Mathilde Riou]({attach}Bateau des humeurs.jpg)](https://www.linkedin.com/posts/mathilde-riou_intelligencecollective-icebreaker-facilitationgraphique-activity-6940586826776629248-xWz8/?originalSubdomain=gr)|[![mur des humeurs attelier collaboratif]({attach}MUR-HUMEUR.png)](https://atelier-collaboratif.com/telechargements/MUR-HUMEUR.jpg)|

D’autre que je n’ai pas utilisés comme ceux de [BeeKast](https://inspirations.beekast.com/fr/inspiration/arbre-a-personnages/). Chez educagri ils appellent ça «[l’arbre d’ostende](https://red.educagri.fr/outils/arbre-dostende/)»

Enfin, une des meilleures variations que j’ai trouvées est celle proposée par Aurelien Morvant sur son site [Ashitabzh](https://ashitabzh.podia.com/arbre-a-personnage). Là, pas de personnage pré-positionné, à vous de choisir lequel et où le placer ! Et je trouve cette idée excellente ! Au lieu de prendre un personnage par défaut qui peut ne pas correspondre tout à fait à votre humeur, là c’est vous qui le sélectionnez et placez. Merci au site [Coach Agile](https://coach-agile.com/arbre-humeur/) de nous en faire profiter facilement.
[![arbre Ashitabzh]({attach}meggs.png){.image .width-50}](https://coach-agile.com/arbre-humeur/)

PS : Bien évidemment, je reste ouvert à l'ajout ou la suppression d'une ressource sur simple demande.

Mise à jour du 07/10/2024 : Je viens de découvrir le décès de Pip 💀😢☠️. Même si je n’ai aucun lien avec lui, j’ai toute de même une pensé pour sa famille. Il nous quitte au vénérable age de 84 ans 4 mois et 20 jours … paix a lui.

![Faire part décès Pip]({attach}piplastblog.jpg){.image .width-50}
