Title: Agile Games France 2024
Date: 2024-04-03
Tags: Agile Game France, Serious Game, Valence
Keyword: jeux, Agile, Game
Slug: Agile-Games-France-2024
Author: Matthieu
Description: Retour en images de l’édition 2024 de Agile Games France
Summary: Retour en images de l’édition 2024 de Agile Games France
Status: published
Cover: /images/AGFr24.jpg

![AGFr24]({attach}AGFr24.jpg){.image .width-75}
Agile Games France est destiné à tous les agilistes en manque d’occasion pour tester de nouveaux jeux, faire des « crash tests », expérimenter, animer tout type de jeux sérieux (ou pas). Un peu comme [Agile Games Alpes](/articles/Agile-Games-Alpes-2023.html) au final 😁


Cet événement existe depuis 2012 avec une exception pour 2021, COVID oblige. Comme son petit frère, la billetterie est souvent prise d’assaut par les habitués de l’évènement qui représentent une bonne moitié des participants. Chose que j’ai remarquée cette année est que la moyenne d’âge a sensiblement baissé de fait de nouvelles têtes plus jeunes. Un peu de renouvellement ne fait jamais de mal 🤗
Cette année, il s’est déroulé les 21 & 22 mars. Le lieu choisi était [le Novotel de Valence Sud](https://all.accor.com/hotel/0455/index.fr.shtml). [Lieu élu a une courte majorité](https://www.linkedin.com/posts/boutin_agile-games-france-2024-choix-du-lieu-activity-7139649227684474880-pYbN/) (47%) face aux autres propositions qui étaient Caen (41%) et Criteuil-Magdeleine (13%).

![Historique des éditions]({attach}carteFr.jpg){.image .width-75}

# J-1

Les (nombreux) habitués de l’évènement ont profité d’un « Before » le jeudi soir. Au vu du nombre présent, cela s’est fait dans 2 lieux distincts. Pour ma part, j’ai fait partie du groupe installé à « [La Réserve](https://www.facebook.com/lareservedevalence/) » dans le centre de la ville.

| | |
| - |-|
| ![Before1]({attach}before1.jpg){.image .fit} | ![Before2]({attach}before2.jpg){.image .fit} |

L’autre partie s’étant déjà installée à l’hôtel et profitant de sa large terrasse.

Je ne vais pas vous lister tous les jeux qui y ont été joués, car ils sont trop nombreux et je n’ai participé qu’à une toute petite partie. [Gregory ALEXANDRE](https://www.linkedin.com/in/gregoryalexandre/) à fait [un autre billet](https://www.linkedin.com/pulse/retour-sur-agile-games-france-2024-gregory-alexandre-0ptdf/) sur l’évènement qui vous donnera un autre point de vue sur l’évènement et sur les jeux joués.

# J1
## Matin

Donc après le « before » plus ou moins sage, nous nous sommes retrouvés devant un bon petit déjeuner pour prendre des forces avant de passer aux choses sérieuses.

![Petit dej]({attach}petitdej.jpg){.image .width-75}

La première étape de ce marathon est l’exposition de toutes les propositions des participants. L’évènement étant « non organisé », un forum ouvert est mis en place pour que chacun puisse faire la pub de son activité, mais aussi son marché de l’inspiration.

![tableau]({attach}tableau.jpg){.image .width-75}

J’ai débuté ma matinée avec un atelier sur UnFix animé par [Maxime Bonnet](https://www.linkedin.com/in/maximebonnet/)

| | |
| - |-|
| ![UnFix]({attach}unfix1.jpg){.image .fit} | ![UnFix]({attach}unfix2.jpg){.image .fit} |

Cet atelier n’était pas vraiment une découverte pour moi, mais un bon rappel des principes fondamentaux de cette boite à outils. L’exercice d’imaginer une entreprise qui suit les puissances de 2, nous force a nous poser les bonnes questions guidées par les différentes cartes mises à notre disposition. Malheureusement, le temps nous a manqué pour aller plus en profondeur, mais donne clairement envie de creuser le sujet.

J’ai ensuite fait un « Artiste et Spécifieur … express », animé par [Moïra Dgroote](https://www.linkedin.com/in/moira2g). Tellement express que je n’ai même pas pris le temps de faire une photo 😅. Dans les grandes lignes, l’exercice consistait à ceci :

- Faire des groupes de 3 personnes
- Faire sortir 2 des 3 personnes
- Donner des instructions écrites pour la production d’un dessin (géomitrique dans notre cas)
- Cacher les productions de ce premier groupe
- Faire entrer un second membre de l’équipe
- Demander au 1er dessinateur de donner les indications au nouveau venu pour reproduire le dessin
- Faire de même avec le dernier membre de l ’équipe
- Comparer le dessin final et celui d’origine
- Faire le bilan

L’activité était vraiment express et assez simple pour en faire donc un « ice breaker ».
De ce que j’ai retenu du débrief est que lors de la retransmission de l’information, personne n’a donné de vue d’ensemble ou même simplement la thématique de l’image à reproduire. Les consignes écrites se sont transmises quasiment telles quelles au dernier de la chaine, sans vraiment chercher à simplifier ou à améliorer ces dernières.  
Pour me rattraper de ne pas avoir pris de photo,  j’ai capturé une autre activité de Moïra qu’elle a fait en début d’après midi. Mis à part le contenu des feuilles des participants, sa première activité avant la même disposition.

![Moïra]({attach}mastermind.jpg){.image .width-75}

Bien sûr, j’ai demandé en quoi consistait cette deuxième activité et elle est tout aussi intéressante. Il s’agissait d’un « MasterMind Agile ». A partir de formes de base simples (rond, carré, triangle) remplies ou non, il fallait deviner la combinaison(5) établie par l’animatrice. Mais, sinon ça serait trop facile, la correction ne se fait que par lot de suggestions. C’est à dire qu’au premier tour de jeu, les participants devaient proposer 6 combinaisons avant d’obtenir leur correction. Puis au second tour de jeu, le lot de suggestions se réduit à 3, puis enfin dans le dernier à 1 seule.  
Je trouve que, l’illustration de l’effet tunnel et de l’importance d’avoir des boucles de rétroaction les plus courtes possibles, est très puissant. Même si je n’ai pas pu y participer, je réutiliserais cet atelier.

Après mon dessin express, je suis allé assister à Dynotopia animé par [Rébecca Carlier](https://www.linkedin.com/in/r%C3%A9becca-carlier-7590322b) et [Lilan Roche](https://www.linkedin.com/in/lilianroche). Je n’ai malheureusement pas réussi à me plonger dans le jeu. Je suis déjà arrivé en retard à cause de l’activité précédente, et le groupe me semblait déjà nombreux pour y trouver ma place. Le fait de devoir s’appuyer sur un tableur pour suivre la mécanique du jeu tranchait un peu avec les activités habituellement proposées. Promis Rébecca, la prochaine fois, je m’y mettrais plus sérieusement. Un porte-clés cadeau était distribué aux participants pour les remercier de leur peine 🦖🦕

| | |
| - |-|
| ![Dynotopia]({attach}Dynotopia.jpg){.image .fit} | ![Dynotopia2]({attach}Dynotopia2.jpg){.image .fit} |
| ![Dynotopia3]({attach}Dynotopia3.jpg){.image .fit} | ![Dynotopia4]({attach}Dynotopia4.jpg){.image .fit} |

Enfin, pour finir la matinée, je suis arrivé sur le tard dans l’activité « The Bend » d’[Alexandre Boutin](https://www.linkedin.com/in/boutin). Avec un simple tissu extensible et quelques balles, on a pu ressentir l’instabilité d’un système soumis à de multiples modifications. Effet démultiplié en cas de dépendance forte entre les acteurs. Plusieurs balles étaient distribuées entre les participants ayant pris place dans ce système en tension. Ces balles devaient être transmises entre les participants en volant la place de la personne réceptionnant la balle. Une seconde itération a été faite, mais cette fois en embarcant son voisin de gauche ou de droite dans sa course. Quand une seule personne bougeait, il y avait déjà quelques turbulences, mais avec son voisin, on était clairement chahuté.

| | | 
| - |-|
| ![Bend]({attach}Bend1.jpg){.image .fit} | ![Bend]({attach}Bend2.jpg){.image .fit} |

Et voilà pour la première matinée !

## Après-midi

Après un bon repas, nous voilà repartis pour l’après midi.

![ecran_aprem]({attach}ecran_aprem.jpg){.image .width-75}

Pour ouvrir le bal, un échauffement original. Reprenant le concept du « téléphone arabe », nous avions le « mime agile » animé par [Johnny Blondel](https://www.linkedin.com/in/johnny-blondel/)

| | |
| - |-|
| ![mime_A]({attach}mime_A.jpg){.image .fit} | ![mime_A2]({attach}mime_A2.jpg){.image .fit} |

S’en est suivie ensuite une pièce de théâtre proposée par [Remi Sarraillon](https://www.linkedin.com/in/remi-sarraillon), [Mario Esposito](https://www.linkedin.com/in/mariusuxdesigner), [Sylvia Cohen](https://www.linkedin.com/in/sycohen) et … Très divertissant, avec un Rémi très convaincant en tant que manager 3.0

| | |
| - |-|
| ![theatre]({attach}theatre.jpg){.image .fit} | ![theatre2]({attach}theatre2.jpg){.image .fit} |

Juste après, c’est à mon tour d’animer un jeu !  
C’est avec grand plaisir que j’ai co-animé une session de DeadLine avec [Gregory Alexandre](https://www.linkedin.com/in/gregoryalexandre) et autant dire que j’ai été très content de sa présence. Je n’ai qu'assez peu pratiqué ce jeu en tant qu’animateur et le faire sur 2 tables à la fois est un vrai défis. Heureusement que greg était là pour pallier à mes hésitations et répondre à mes interrogations. Les participants ont, semble-t-il, apprécié le jeu, mais sa durée totale (>1h30) nous a incité à sacrifier le dernier tour de jeu pour ne pas bloquer trop longtemps les participants pour qu’ils puissent profiter d’autres jeux.  
Je pense que je maitrise maintenant assez bien les règles pour le proposer en atelier professionel. Il reste à trouver à qui 😅 le fait que ce soit un jeu et qui plus est long, n’est pas facile a vendre.

| | |
| - |-|
| ![Deadline_prépa]({attach}Deadline_prépa.jpg){.image .fit} | ![Deadline_score]({attach}Deadline_score.jpg){.image .fit} |
| ![Deadline_T1]({attach}Deadline_T1.jpg){.image .fit} | ![Deadline_T2]({attach}Deadline_T2.jpg){.image .fit} |

J’ai réussi à m’échaper de mon rôle de GO quelques minutes, pour voir ce qui se faisait en parrallèle.  
Il y avait juste à côté de nous Alexandre avec son labyrinthe Agile.

| | |
| - |-|
| ![labyrinthe_alex]({attach}labyrinthe_alex.jpg){.image .fit} | ![labyrinthe_enonce]({attach}labyrinthe_enonce.jpg){.image .fit} |

Dans un coin un peu plus isolé, [Emmanuel Hervé](https://www.linkedin.com/in/emmanuelherve) faisait une session du «TAO». _(Jeu trèèèèèès long pour sa part)_

![Jeu_TAO]({attach}Jeu_TAO.jpg){.image .width-75}

Après mon atelier, je me suis promené pour butiner à droite à gauche sans but précis. Je me suis finalement posé à la table de l’atelier d’[Alexis Monville](https://www.linkedin.com/in/alexismonville). C’était plus un cercle de réflexion sur la conception d’un nouveau jeu qui lui trotait dans la tête … hâte de voir ce qu’il en a retiré et ce qu’il nous prépare.

![Jeu_alex]({attach}Jeu_alex.jpg){.image .width-75}

J’ai réussi à me poser à l’activité proposée par [Aurore Chailloux](https://www.linkedin.com/in/aurore-chailloux) autour du jeu «Opale». Comme certains doivent s’en douter, ce jeu est très largement inspiré du livre « Reinventing Organisation » de Frédéric Laloux. Ce n’est pas vraiment une explication des concepts, mais plus une mise en situation de différentes problématiques avec des exemples de pistes pour les résoudre. Le tout sous fond de course entre les équipes.  
Jeu très intéressant pour remettre les pieds sur terre à certains managers se pensant bien pensant et bien veillant.

| | |
| - |-|
| ![Opal1]({attach}Opal1.jpg){.image .fit} | ![Opal2]({attach}Opal2.jpg){.image .fit} |

La fin de la journée s’approchant, j’attends le repas du soir en discutant avec les participants. C’est bien de faire des jeux à la chaine, mais parfois il faut se poser et prendre le temps de profiter des autres. Prendre des nouvelles des personnes que l’on ne croisent que dans ce genre d’évènement.

![repas_soir]({attach}repas_soir.jpg){.image .width-75}

Le reste de la soirée est assez varié. Certains continuent avec des jeux, comme (encore eux) [Rébecca Carlier](https://www.linkedin.com/in/r%C3%A9becca-carlier-7590322b) et [Lilan Roche](https://www.linkedin.com/in/lilianroche) et leur jeu «Kanban EV». Je n’y ai pas participé, mais il me semblait que c’était encore du lourd … expliquer Kanban via _(chose original)_ une usine de voiture 😆

| | |
| - |-|
| ![Kanban_EV]({attach}Kanban_EV.jpg){.image .fit} | ![Kanban_EV2]({attach}Kanban_EV2.jpg){.image .fit} |

Sinon, nous avions un «Blind Test», une veillé _mais sans feu de bois_, une discothèque et bien sûr, pleins de petits groupes de discussions.

| | | |
|-|-|-|
| ![Blind_test]({attach}Blind_test.jpg){.image .fit} | ![Veille]({attach}Veille.jpg){.image .fit} | ![Disco]({attach}Disco.jpg){.image .fit} |

La soirée s’est terminée tranquillement au bar de l’hôtel. Pleins de sympathiques discussions, quelques cocktails _illégitimes_, un jeu du «Mao» _(mais je n’en dirais pas plus sur ce dernier)_. C’est finalement le vigile de l’hôtel qui a sonné la fin de la partie vers 2h du matin.

# J2

Le lendemain matin, nous nous mettons dans les meilleures conditions possibles après cette soirée avec un bon petit dej.

![petitdej]({attach}petitdej.jpg){.image .width-75}

Durant la matinée, nous avons pû trouver [Christophe Deniau](https://www.linkedin.com/in/christophe-deniaud/) avec son Agile Impédiment Game _(Je n’y ai pas participé)_.

![Agile_Impediment_Game]({attach}Agile_Impediment_Game.jpg){.image .width-75}

[Sabina Lammert](https://www.linkedin.com/in/sabina-lammert-14abb512b/) m’a fait découvrir «Les Mondes 3D» qui va bien au delà d’un simple jeu de _priorisation_.Elle y mêle aussi les regroupements, les dépendances, les intérêts des différents acteurs, et bien plus. Mais le temps nous manquait.

| | |
| - |-|
| ![Monde3D1]({attach}Monde3D1.jpg){.image .fit} | ![Monde3D3]({attach}Monde3D3.jpg){.image .fit} |
| ![Monde3D2]({attach}Monde3D2.jpg){.image .fit} | ![Monde3D4]({attach}Monde3D4.jpg){.image .fit} |

Dans un autre coin [Patrice Boisieau](https://www.linkedin.com/in/patriceboisieau) animait un « Quadrant d’Ofman » dont il nous a fait le plaisir de diffuser [les principes](https://drive.google.com/file/d/1yzZfwISuihrIKdX1jxlhChTaH6Km0TAS/view) avec [une aide](https://drive.google.com/file/d/1NlDa2hFTYvb77NqwmYxF5U8mKOdzXRYV/view?usp=sharing) et bien sûr [le quadrant à imprimer](https://drive.google.com/file/d/1Njk9HKKDDVDYAEoV6JjT1KuvCy8KURna/view?usp=sharing).

![Ofman]({attach}Ofman.jpg){.image .width-75}

Dans un salon privé nous avions aussi une « Fresque Agile », sur le même modèle que les fresques du climat.

![Fresque_agilite]({attach}Fresque_agilite.jpg){.image .width-75}

 [Rébecca Carlier](https://www.linkedin.com/in/r%C3%A9becca-carlier-7590322b) et [Lilan Roche](https://www.linkedin.com/in/lilianroche) _(toujours eux)_ animaient leur autre création qu’est « Le mystère de la pyramide de Toutankhascrum ». Pour avoir joué à leur idée de départ à [Agile Grenoble 2023](/Agile-Grenoble-2023), le produit final qu’ils nous présentaient ici est bien différent. Même si le principe de jeu est similaire, l’habillage a très fortement évolué. La partie scénaristique a clairement été revue de fond en comble et très étoffée… pour notre plus grand bonheur. Donc, si vous avez l’occasion de les croiser, n’hésitez pas à vous laisser tenter par une petite partie 😉

| | |
| - |-|
| ![ToutenkaScrum1]({attach}ToutenkaScrum1.jpg){.image .fit} | ![ToutenkaScrum2]({attach}ToutenkaScrum2.jpg){.image .fit} |

[Moïra Dgroote](https://www.linkedin.com/in/moira2g) _(il y a quelques «serial-animateur»)_ était parti à l’aventure avec des canards.

| | |
| - |-|
| ![Aventure_canard]({attach}Aventure_canard.jpg){.image .fit} | ![Aventure_canard2]({attach}Aventure_canard2.jpg){.image .fit} |

Un jeu qui est un récurrent de l’évènement avec un « Space Team », en 2 salles, 2 ambiances. D’un côté le jeu de plateau, de l’autre son adaptation pour mobile.

| | |
| - |-|
| ![SpaceTeam]({attach}SpaceTeam.jpg){.image .fit} | ![SpaceTeam2]({attach}SpaceTeam2.jpg){.image .fit} |

Mais tout le monde n’était pas occupé. Certains discutaient … d’autres se répartissaient des mignonettes qui devaient disparaitre 🍾

| | |
| - |-|
| ![terasse]({attach}terasse.jpg){.image .fit} | ![Mignonnette]({attach}Mignonnette.jpg){.image .fit} |


Enfin, un petit jeu collaboratif en ligne par [Olivier Azeau](https://www.linkedin.com/in/azeau/). Au départ, un POC sans ambition qui a finalement pris de l’ampleur et est assez intéressant pour la coordination d’équipe. Il reste quelques bugs, mais me semble très prometteur.

![Jeu_olivier]({attach}Jeu_olivier.jpg){.image .width-75}

Et c’est malheureusement TOUT pour moi. Pour raison personnelle, j’ai dû partir juste après le repas. Je ne le regrette pas, car je sais que le meilleur était déjà passé. D’expérience je sais que la dernière après midi est plus tranquille avec surtout beaucoup de discussions. Alors certes, ces discussions sont souvent très intéressantes et enrichissantes et c’est dommage de ne pas y participer, mais on ne peut pas toujours faire ce que l’on veut.

![Photo de groupe]({attach}groupeAGFr24.jpg){.image .fit}

Merci à tous les participants pour cet événement hors du temps et plein de super personnes que l’on apprécie de retrouver à chaque édition ou lors d’autres évènements agiles. 🤩
