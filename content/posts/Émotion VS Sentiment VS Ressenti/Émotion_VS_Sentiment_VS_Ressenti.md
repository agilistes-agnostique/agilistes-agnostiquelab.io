Title: Émotions vs Sentiment vs Ressenti
Date: 2024-10-10
Tags: vocabulaire, émotion, sentiment, ressenti
Category: Parlons peu, mais parlons bien
Slug: Émotion-vs-Sentiment-vs-Ressenti
Author: Matthieu
Description: Tabou dans le monde professionnel, parler de ce que l’on ressent n’est pas évident. Employer les bons termes est déjà un premier pas pour comprendre ce qui nous arrive.
Summary: Tabou dans le monde professionnel, parler de ce que l’on ressent n’est pas évident. Employer les bons termes est déjà un premier pas pour comprendre ce qui nous arrive.
Status: published
Cover: /images/émotion-vs-sentiment-vs-ressenti.jpg 
Cover_Source: https://unsplash.com/photos/brown-eggs-on-white-tray-uh6msP8Md_E

# Introduction
Pour cet autre billet de [la série](https://anargile-leanbertaire.info/articles/Parlons-peu-mais-parlons-bien.html), penchons-nous sur les termes « émotion », « sentiment » et « ressenti ». La distinction entre ces 3 termes peut parfois prêter à confusion, mais une fois que l’on a les bons critères de différenciation, il devient très facile de les distinguer.

## Étymologie

* **Émotion** : Dérivé de émouvoir formé d'après l’ancien français et moyen français motion « mouvement », emprunté au latin motio « action de mouvoir, mouvement, trouble, frisson (de fièvre) ». ([source](https://fr.wiktionary.org/wiki/émotion))
* **Sentiment** : Dérivé du latin « sentire », avec le suffixe -ment, « percevoir par les sens », puis « être d’un certain avis, penser »  ([source](https://fr.wiktionary.org/wiki/sentiment))
* **Ressenti** : Participe passé du verbe ressentir ([source](https://fr.wiktionary.org/wiki/ressenti)), Verbe dérivé de sentir, avec le préfixe re-

Comme on pouvait s’en douter « Sentiment » et « Ressenti » ont la même origine, ce qui ne les empêche pas de se distinguer par la suite.

## Définitions

Les définitions de ces termes sont très nombreuses et peuvent être prises sous plusieurs angles. Je me suis concentré sur l’aspect affectif et intellectuel. 

### Émotion
> Réaction affective brusque et momentanée, agréable ou pénible, souvent accompagnée de manifestations physiques. [Source](https://www.dictionnaire-academie.fr/article/A9E1090)  

> Conduite réactive, réflexe, involontaire vécue simultanément au niveau du corps d'une manière plus ou moins violente et affectivement sur le mode du plaisir ou de la douleur.[Source](https://www.cnrtl.fr/definition/%C3%A9motion)

**Exemples** :  peur, colère, surprise, admiration, déception, béatitude, etc.

### Sentiment
> Connaissance, compréhension que l’on a de certaines choses sans le secours du raisonnement, de l’expérience ; appréhension immédiate et subjective d’une réalité. [Source](https://www.dictionnaire-academie.fr/article/A9S1251)  

> État affectif complexe, assez stable et durable, composé d'éléments intellectuels, émotifs ou moraux, et qui concerne soit le « moi » (orgueil, jalousie...) soit autrui (amour, envie, haine...).[Source](https://www.cnrtl.fr/definition/sentiment)

**Exemples** : amour, haine, gratitude, jalousie, nostalgie, etc.

### Ressenti
> (Néologisme) Ensemble des choses que l’on ressent, et qui forme l’opinion que l’on a des choses.[Source](https://fr.wiktionary.org/wiki/ressenti)  

> Éprouver dans son âme ou dans son esprit l'effet d'une cause extérieure.[Source](https://www.cnrtl.fr/definition/ressenti)

**Exemples** : bien-être, mal-être, injustice, satisfaction, etc.

### Fiche pratique

On peut donc résumer la différence entre ces 3 termes (en grossissant le trait) sous forme d’un tableau :

| Termes    | Intensité | Durée    | Visibilité            | Origine                 |
|-----------|-----------|----------|-----------------------|-------------------------|
| Émotion   | forte     | courte   | généralement physique | un évènement spécifique |
| Sentiment | modéré    | longue   | pas obligatoire       | résultat d’une réflexion et d’une interprétation des émotions |
| Ressenti  | faible    | variable | souvent caché         | perception personnelle d’une situation, d’une émotion ou d’un sentiment |

Encore une fois, ce tableau n’est pas la vérité absolue et bien évidemment des contre-exemples peuvent se trouver à la pelle. Mais il vous permet de vous faire une première idée de ce que vous vivez.

## Éclairage

Sur la base de ce tableau et en réfléchissant un instant, on peut comprendre comment se construit chacune de ces nuances.  
L’émotion est la base, la brique élémentaire. Elle fait appel aux réflexes primaires de notre psyché.  
Ensuite, avec l’accumulation de ces émotions, on construit les sentiments. Parfois en opposition à la logique rationnelle de notre cerveau.  
Enfin, le ressenti est le sommet de l’iceberg de toutes ces sensations. Elle est unique et personnelle, car elle fait appel à notre conditionnement sociologique (culture, religions, convictions, éducation, etc.). On la confond souvent avec l’intuition et ses raisons profondes nous sont souvent difficiles à saisir et pourtant, on lui fait confiance.

## Bénéfices

Sensibiliser ses équipes aux subtilités du langage permet une meilleure communication et évite les quiproquos. Savoir décomposer toutes les strates et les constructions, permet d’avoir un regard plus objectif sur ce qui se passe en nous et nous permet une introspection plus efficace.  
Dans la gestion des relations professionnelles, ça peut nous aider pour déconstruire un mauvais (ou bon) a priori qui ne serait finalement pas justifié. Une réunion s’est mal passé et vous repartez avec un certain ressenti envers vos interlocuteurs ? Posez-vous la question de pourquoi ? Est-ce fondé sur un sentiment ou émotion particulière ? En mettant tout cet affect de côté, la réaction était-elle justifiée ?  
Les publicitaires usent (et abusent) de techniques vous mettant dans de bonnes dispositions pour vous faire céder à leur argumentaire. Ne vous laissez plus avoir par la suggestion d’une ou plusieurs émotions, le premier outil de défense est la connaissance de tous ces mécanismes.


## Ouverture

J’aurais pu élargir le scope de ce billet avec des termes proches comme « Humeur », « Sensation » ou « Perception ». Mais je trouve que ces trois forme un triptyque cohérent et je ne voulais pas plus charger les explications. Mais libre à vous d’essayer de compléter le tableau 😉 
