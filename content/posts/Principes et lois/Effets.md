Title: Quelques principes, lois et effets utiles 3/3
Date: 2023-05-28
Tags: principes, lois
Keyword: principes, lois, culture
Category: management
Slug: Principes-lois-effets-3
Author: Matthieu
Description: Domino, Golem, Zeigarnik, … ça ne vous évoque rien ? Je vous explique tout par ici.
Summary: Avec un peu d’expérience sur un sujet il est facile de dégager de grand principe de fonctionnement. C’est un peu les dictons du monde moderne. Ces principes ou lois s’exerce aussi dans le monde de l’entreprise. Je vais tenter de vous en lister quelques un des plus intéressant, sans rentrer dans le détail (chacun pouvant faire l’objet l’un long billet), mais assez pour que vous puissiez les reconnaitre et les comprendre.
Status: draft
Cover: /images/principes-et-lois.avif

# Effets

* [Effet Domino](https://fr.wikipedia.org/wiki/Effet_domino) : Un des plus connu et plus simple. Il s’agit d’une réaction en chaine d’évènements de même ampleur, physiquement ou thématiquement proche. Ces changements peuvent être voulus, maitrisés, délimités, … ou pas.

* [Effet Papillon](https://fr.wikipedia.org/wiki/Effet_papillon) : On pourrait le considérer comme un corolaire de l’effet Domino. La différence majeure est l’introduction du ratio entre la suite d’évènements. L’idée est qu’en changeant très légèrement les conditions initiales d’un système, le résultat peut-être d'une tout autre mesure. C’est Edward Lorenz qui est retenu comme le père de cette expression, l’ayant utilisé comme titre d’une conférence scientifique en 1972, sous la forme suivante :

> « Le battement d’ailes d’un papillon au Brésil peut-il provoquer une tornade au Texas ? »

<u>Outils utiles :</u> Faites des petits pas ! Moins vous avez d'encours, moins grand sont les risques de désastres. Donc le découpage, la priorisation 


* [Effet Dunning Kruger](https://fr.wikipedia.org/wiki/Effet_Dunning-Kruger) : Aussi connu sous le nom d’effet de « sur-confiance », qui n’est autre qu’un biais cognitif de surestimation de la maitrise d’un sujet. Des citations me viennent à l’esprit pour illustrer cet effet.

> « L’ignorant affirme, le savant doute, le sage réfléchit. » 
*Aristote*

> « Ceux qui savent ne parlent pas, ceux qui parlent ne savent pas. Le sage enseigne par ses actes, non par ses paroles. »
*Lao Tseu*

> « Certaines personnes écoutent la moitié, comprennent un quart, mais explique le double. » 
*Inconnu*

Une jolie image permet de bien comprendre aussi :
![Effet Dunning-Kruger](https://upload.wikimedia.org/wikipedia/commons/7/75/2019-06-19_effet_dunning_kruger.png){.image .width-75}

Bon il s’avère que la courbe est complètement biaisé et non représentative des résultats des expériences. Même la page [Wikipédia](https://fr.wikipedia.org/wiki/Dunning%E2%80%93Kruger_effect) induit en erreur sur l’interprétation des résulats. J’ai trouvé un point de vu éclairant sur cet effet [sur le site d’Aurélien Le Masson](https://biaiscognitifs.substack.com/p/biais-4-leffet-dunning-kruger).

<u>Outils utiles :</u> [Tourner 7 fois sa langue dans sa bouche](https://fr.wiktionary.org/wiki/tourner_sa_langue_sept_fois_dans_sa_bouche) :-p

* [Effets Pygmalion](https://fr.wikipedia.org/wiki/Effet_Pygmalion) : Aussi connu sous le nom d’effet Rosenthal et Jacobson. Biais cognitif qui fait en sorte que nous augmentons nos performances en fonction de l'image positive que nous renvoie les gens nous entourant. Effet largement démontré sur des humains, mais aussi sur des rats !!!!

* [Effets Golem](https://fr.wikipedia.org/wiki/Effet_Golem) : Jumeaux maléfique de l'effet Pygmalion. Si l'image qui nous est renvoyé est négatives alors nos performances s'amoindrissent.

* [Effets Hawthorne](https://fr.wikipedia.org/wiki/Effet_Hawthorne) : L'attention porté 

* [Effet Zeigarnik](https://fr.wikipedia.org/wiki/Effet_Zeigarnik) : On le doit à la psychologue russe [Bljuma Voulfovna Zeigarnik](https://fr.wikipedia.org/wiki/Bljuma_Zeigarnik) (Блюма Вульфовна Зейгарник) qui a permis d'établir ce biais cognitif en 1927 : 
> « Je me rappelle plus facilement les tâches et les problèmes que j’ai laissés en suspens ou qui sont en cours, que les problèmes que j’ai résolus. »

<u>Outils utiles :</u> Célébrez-vos succès/échecs/apprentissage !!! Si vous utilisez Scrum, les revues sont là pour ça, mais rien de vous empêche d'instaurer des plénières / démo / etc. sur un autre rythme. Mais ça peut aussi prendre la forme d'une lettre d'information diffuser a toute l'équipe/entreprise. Bref, suivez ce qui est fait et faites-le savoir.
