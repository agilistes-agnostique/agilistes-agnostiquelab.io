Title: Quelques principes et lois utiles
Date: 2023-05-28
Modified: 2023-06-19
Tags: principes, lois
Keyword: principes, lois, culture
Category: management
Slug: Principes-et-lois
Author: Matthieu
Description: Pareto, Parkinson, falaise de verre, … ça ne vous évoque rien ? Je vous explique tout par ici.
Summary: Avec un peu d’expérience sur un sujet il est facile de dégager de grand principe de fonctionnement. C’est un peu les dictons du monde moderne. Ces principes ou lois s’exerce aussi dans le monde de l’entreprise. Je vais tenter de vous en lister quelques un des plus intéressant, sans rentrer dans le détail (chacun pouvant faire l’objet l’un long billet), mais assez pour que vous puissiez les reconnaitre et les comprendre.
Status: draft
Cover: /images/principes-et-lois.avif

Page regroupant tous les principes, effets, lois et autres axiomes intéressant à connaitre pour la gestion de projets. Les explications ne sont absolument pas complètes mais devrait vous servir de pointeurs pour approfondir le sujet. Les « outils utiles » sont à prendre comme des pistes à explorer, mais il existe évidemment pleins d’autres alternatives.

# Avant-propos

Précision de vocabulaire pour comprendre de quoi on parle :
* [Loi](https://fr.wiktionary.org/wiki/loi) : Règle, obligations écrites, prescrites ou tacites, auxquelles les personnes se doivent de se conformer. (Sciences) Postulat ou énoncé vrai sous certaines conditions. (Physique) Ce qui règle l’ordre du monde physique. 

* [Principe](https://fr.wiktionary.org/wiki/principe) : Première règle d’un art ou d’une discipline. (Sciences) Loi que certaines observations ont d’abord rendue vraisemblable et à laquelle on a donné ensuite la plus grande généralité. (Courant) Règle de conduite d’une personne ou d’un groupe.

En science « dure » la distinction est assez simple puisqu’une loi ne souffre pas d’exception *(à moins que les cas particuliers ne soient intégrés à la loi)*.
Dans notre cas la différence est plus subtile. D’un côté avec les *lois* on a une règle de fonctionnement venant *d’en haut*, dicté de manière arbitraire et parfois suite à une intuition, alors que le *principe* vient *d’en bas* dans le sens ou c’est par l’accumulation d’observations ou d’expérimentations qu’on le façonne. Mais au final on se retrouve dans la même situation avec une directive qui nous indique un fonctionnement. La frontière étant assez fine, la répartition entre les 2 est assez aléatoire. J’ai pris le parti de suivre ce qui est le plus usité, même si j’ai constaté que le terme « Loi » était souvent préféré sans raison particulière.

* [Effet](https://fr.wiktionary.org/wiki/effet) : Ce qui est produit par quelque cause. Là on est plus sur la mise en évidence de levier de comportement. Parfois activable sur commande, parfois implicitement ou automatiquement.

Un autre mot qui découle de l’utilisation de ces termes est « Corollaire ».

* [Corollaire](https://fr.wiktionary.org/wiki/corollaire) : Du latin « corollarium » qui veux dire « petite couronne ». C’est donc une extension directe de la loi / principe / effet susnommé.

# Principes
* [Principe Pareto](https://fr.wikipedia.org/wiki/Principe_de_Pareto) ou « la règle du 80/20 » : Principe assez simple qui dit que « 80 % des effets sont le produit de 20 % des causes ». Si on applique ça au monde du travail, vous obtiendrez 80% du résultat attendu avec 20% de l’effort. Son corollaire étant que les 20% restant vous prendront 80% de votre effort, donc clairement à remettre en question pour s’assure que le ration bénéfice / effort en vaut la chandelle. (« As-t-on vraiment besoin de cette fonctionnalité ? »)

<u>Outils utiles :</u> Des outils de priorisation classiques peuvent aider à se concentrer sur les 20% les plus utiles. Par exemple la [matrice d’Eisennhower](https://fr.wikipedia.org/wiki/Matrice_d%27Eisenhower). Des outils se concentrant sur la valeur métiers serait aussi intéressant, tel un [MoSCoW](https://fr.wikipedia.org/wiki/M%C3%A9thode_MoSCoW) ou le plus polémique [WSJF](https://wikiagile.fr/index.php?title=WSJF_(SAFe))

* [Principe de Peter](https://fr.wikipedia.org/wiki/Principe_de_Peter) : Vous avez surement déjà rencontré quelqu’un que vous pensez ne pas avoir les compétences suffisantes pour son poste. C’est l’illustration parfaite de ce principe qui dit que « dans une hiérarchie, tout employé a tendance à s’élever à son niveau d’incompétence », avec pour corollaire que « avec le temps, tout poste sera occupé par un employé incapable d’en assumer la responsabilité ». 
Pour être un peu plus concret, un bon dev/ouvrier/commercial/… ne fait pas forcément un bon manager/responsable de service/directeur d’agence/… Ce sont souvent des métiers différents et si l’envie de la personne concernée n’est pas là il ne faut pas l’y obliger. Malheureusement ce genre de promotion est souvent l’unique moyen d’améliorer sa situation (financière, influence, reconnaissance, …) au sein de l’entreprise. Des solutions existent. La plus facile est de *promouvoir* un *incompétent* a un poste d’apparence plus prestigieux mais en réalité moins impactant pour l’entreprise. Solution que je trouve *cache misère* et qui fait porter un poids mort à l’entreprise (un peu mieux expliqué [ici]( https://eventuallycoding.com/2013/11/27/principe-de-peter-et-corollaire-darchimede)). Heureusement des idées plus pertinentes sont déjà en place chez certains, comme des parcours spécialisés qui englobe l’ensemble d’une carrière, sans changer de domaine ([voir ce billet](https://eventuallycoding.com/2021/06/24/senior-avec-6-ans-dexperience-et-apres))
Si vous voulez approfondir ce principe, [le livre original](https://www.babelio.com/livres/Peter-Le-principe-de-Peter/1271105) est toujours édité.
	* [Couplage avec la loi normale](https://fr.wikipedia.org/wiki/Principe_de_Peter#La_d%C3%A9foliation_hi%C3%A9rarchique) : (La loi normale étant expliqué plus bas) Heureusement tous les postes ne sont pas occupés par des « incompétents » et statistiquement on retrouve la répartition normale suivante :
		* 10 % d’employés sont super-compétents
		* 20 % d’employés sont compétents
		* 40 % d’employés sont modérément compétents
		* 20 % d’employés sont incompétents
		* 10 % d’employés sont super-incompétents

	* [Extension Dilibert](https://fr.wikipedia.org/wiki/Principe_de_Dilbert) : Vous connaissez peut-être (le comic strip Dilibert)[http://www.dilbert.com/] qui illustre toute les dérives possibles de la mauvaise gestion d’entreprise via son personnage principal Dilibert. Le principe s’énonce ainsi : « Les gens les moins compétents sont systématiquement affectés aux postes où ils risquent de causer le moins de dégâts : ceux de managers. » 

<u>Outils utiles :</u> Un moyen très simple est de ne pas avoir de hiérarchie ^^. Cependant au-delà d’une certaine taille d’organisation ça devient indispensable. À ce moment-là on peut s’inspirer de l’holacratie avec l’[élection sans candidat](https://instantz.org/election-sans-candidat/). Un accompagnement et formation sont 2 leviers évidents pour contrer ce principe.

* [Principe du plafond de verre](https://fr.wikipedia.org/wiki/Plafond_de_verre) : Principe de plus en plus médiatisé et souvent lié au critère de parité, mais son application totalement générique. Il désigne le fait que les postes les plus *prestigieux* soient réservés à une caste bien particulière. Le critère de sélection pouvant être très divers, le plus visible étant le sexe, mais on peut aussi trouver la couleur de peau, la nationalité, le cursus scolaire/type de diplôme, la religion, etc. Il est impossible de faire un listing exhaustif, donc je vous laisse imaginer la suite. Bien sûr le *critère* de sélection peut être multiplet et en mixer plusieurs avec une pondération propre.
	* [Extension falaise de verre](https://fr.wikipedia.org/wiki/Falaise_de_verre) : Principe moins connus mais tout aussi pernicieux. C’est au moment où les rats quittent le navire que l’on constate ce principe. Ces fameux postes inaccessibles le deviennent miraculeusement lors de période de crise grave. Les postes très exposés médiatiquement tel que la politique en donne [tout un florilège]( https://fr.wikipedia.org/wiki/Falaise_de_verre#Exemples_politiques) (ex : Marissa Mayer@Brexit 2016). Mais les grandes entreprises ne sont pas épargnées non plus (ex : Marissa Mayer@Yahoo 20212) ou les musées (ex : Laurence des Cars@Louvres 2021). Ce principe a été établis suite à [l’étude]( https://onlinelibrary.wiley.com/doi/abs/10.1002/9781118785317.weom110287) de [Clara Kulich]( https://www.unige.ch/fapse/psychosociale/collaborateurs/kulich) qui trouvait étrange [la statistique du Times]( https://www.psychologie.uni-frankfurt.de/58476021/Generic_58476021.pdf) de sous performance des entreprises dirigées par des femmes. Merci à elle.

<u>Outils utiles :</u> Sujet délicat sans réponse magique. Dans le domaine de l’IT la parité est souvent quasiment impossible à avoir, c’est donc aux managers d’avoir un point d’attention voire une métrique spécifique sur le sujet, sans pour autant tomber dans la *discrimination positive* exagérée qui pourrait être mal perçu par les 2 parties.

# Lois
* [Loi de Murphy](https://fr.wikipedia.org/wiki/Loi_de_Murphy) ou Loi de l’emmerdement maximal (LEM) : C’est ma petite préférée celle-là. « Si cela peut mal se passer, cela arrivera » On doit cette formulation à [George Nichols](https://fr.wikipedia.org/wiki/George_Nichols), celle du [professeur Murphy](https://fr.wikipedia.org/wiki/Edward_A._Murphy_Jr.) étant « Si ce gars a la moindre possibilité de faire une erreur, il la fera » Tout cela à cause d’un assistant qui monta à l’envers TOUS les capteurs de l’expérience en cours. Il y avait au moins une certaine constance dans son action.
Il y a plein de corolaire et d’extension à cette loi, mais si je devais en retenir 2 particulièrement ça serais celles-là :
	* Corolaire de Finagle « Tout ce qui peut aller mal le fera au pire moment. »
	* Loi des séries : « Les emmerdes c’est comme les cons, ça vole toujours en escadrille. »  Jacques Chirac
Je vous fais grâce de vous les expliquer, elles sont assez parlantes d’elle-même.

<u>Outils utiles :</u> Les catastrophes sont souvent un enchainement d’erreurs individuel. Le mieux pour le contrer est de ne pas faire les tâches importantes seul. Pour cela le *pair*[ce que vous voulez] est un bon moyen. Dans le livre [Renversez la vapeur !](https://www.chez-mon-libraire.fr/livre/9782892259926-renversez-la-vapeur-l-histoire-vraie-d-un-capitaine-qui-a-su-transformer-des-executants-en-leaders-l-david-marquet/) (traduction de « Turn the ship around ») en plus de faire les actions en pair il recommande de vocaliser tout ce qui va être fait en amont de leur réalisation. Cela permettrait de se rendre compte soi-même ou son partenaire d’une errer potentiel.

* [Loi de puissance](https://fr.wikipedia.org/wiki/Loi_de_puissance) : Très utilisé en sciences « dur », elle a cependant une application concrète en sociologie. Elle permette de décrire tous les phénomènes qui présentent une invariance d’échelle. Par exemple les sites de type wikis suivent la règle des 90-9-1 : 90 % de la population utilisatrice ne contribue pas ; 9% sont des contributeurs occasionnels et 1% des visiteurs du site contribue régulièrement. Ces proportions ne changeant pas que vous vous appeliez « Wikipédia » ou « LeWikiDuTressageDeRoseau ». Ou plus près de notre domaine qu’est la gestion de projet Agile, que vous gériez une équipe de 9 personnes ou 900, la répartition de vos problématiques ne changeront qu’à la marge.

<u>Outils utiles :</u> Seul la connaissance vous aidera sur le sujet. Pour ça formez-vous ! Mettez en place des métriques de vos utilisateurs.

* [Loi de Pareto](https://fr.wikipedia.org/wiki/Loi_de_Pareto) : Quoi ? Encore lui ? Eh bien oui. Le principe ayant une application assez générique, la loi elle est plus rigoureuse et est utilisé en sciences physique et sociales. C’est un type particulier de la loi de puissance. Cet outil met en évidence la loi des 80/20. Autrement dit, agir sur 20 % de causes permet de résoudre 80 % du problème. Le Pareto est utile pour identifier sur quelle cause agir en priorité pour améliorer de façon significative la situation. 

<u>Outils utiles :</u> Le bien nommé [diagramme de Pareto](https://fr.wikipedia.org/wiki/Diagramme_de_Pareto) vous permettra d’identifier les causes premières et donc agir de manière efficiente sur le problème.

* [Loi normale](https://fr.wikipedia.org/wiki/Loi_normale) (ou courbe de Gauss) : Issue du domaine des probabilités et statistique elle permet de modéliser des phénomènes natures issus de plusieurs évènements aléatoires. Prenez l’espérance mathématique (le résultat le plus probable) (noté : μ) d’un phénomène croisé avec son écart type (σ) et vous obtenez la répartition des résultats. En image ça donne ça :
![Distribution normal](https://upload.wikimedia.org/wikipedia/commons/thumb/7/74/Normal_Distribution_PDF.svg/720px-Normal_Distribution_PDF.svg.png){.image}
La hauteur des courbes représentant le nombre d’occurrence du résultat obtenu. 
Dans un processus de transformation (agile) d’une organisation vous pourriez en déduire le nombre de personne contre, neutre ou favorable avec un petit échantillon du personnel de l’entreprise de base, puis une extrapolation à l’ensemble de l’entreprise.

<u>Outils utiles :</u> Vu que c’est un outil en lui-même je n’ai rien a suggéré si ce n’est un point d’attention sur vos métriques. Choisissez-les avec soin et assurez-vous que vos échantillons soient vraiment représentatifs.

* [Loi de Parkinson](https://fr.wikipedia.org/wiki/Loi_de_Parkinson) : Énoncé en 1955 par Cyril Northcote Parkinson dans un article publié dans la revue *The Economist*, elle a été formulé ainsi « Le travail s’étend de manière à remplir le temps disponible pour son achèvement. […] ». Quel que soit le temps que vous (vous) accordez pour la réalisation d’une tâche, l’intégralité de ce temps sera consommé. C’est l’une des raisons pour lequel Scrum et d’autre méthodes Agiles se fixe des bulle temporel (timebox) et un rythme particulier. En plus d’éviter les effets tunnels, ça évite surtout de se perdre dans des détails (Pareto) de moindre importance. <br/><br/><u>Outils utiles :</u> Levez le nez du guidon ! Pour ça des ~~Sprints~~ cycles sont intéressants pour faire un point régulièrement (démo / rétro). Une autre manière est aussi de découper plus finement vos travaux, le chiffrage n’en sera que plus précis et juste (essayez le [carapaccio elephant](https://www.occitech.fr/blog/2014/05/decoupez-vos-stories-en-carpaccio/)). Rétrospectivement un [graphique de contrôle](https://fr.wikipedia.org/wiki/Carte_de_contr%C3%B4le) vous permettra d’identifier les anomalies pour en identifier les causes.

	* [Corolaire futilité de Parkinson](https://fr.wikipedia.org/wiki/Loi_de_futilit%C3%A9_de_Parkinson) : L’idée est que les organisations donnent une importance bien plus important sur des détails secondaires qu’au réel problème de fond. Typiquement dans une réunion pour la construction d’un abri à vélo, les discutions autour de sa couleur occupera bien plus de temps que son emplacement ou sa capacité. Cela peut s’expliquer par le fait que s’exprimer sur des caractéristiques essentielles implique une prise de responsabilité proportionnel et donc une mise en cause directe si un mauvais choix a été fait. Alors que la couleur n’implique qu’une responsabilité minime et au pire avoir « mauvais goût ».

	<u>Outils utiles :</u> Ne laissez pas de place au hasard ! Priorisez vos objectifs, préparer votre ordre du jour, invitez uniquement les personnes pertinentes, …

* [Loi de Hofstadter](https://fr.wikipedia.org/wiki/Loi_de_Hofstadter) : Elle vient en complément de la loi de Parkinson et s’énonce ainsi « Il faut toujours plus de temps que prévu, même en tenant compte de la loi de Hofstadter. ». Je vous laisse en déduire les implications dans la planification et estimation des projets. Jetez un œil au *Facteur Pi* un peu plus bas si vous n’avez pas encore perdu la foi dans ce genre d’exercice.

<u>Outils utiles :</u> Essayer de maitriser cette loi va rapidement vous faire basculer du coté *gestion de projet classique*, avec des outils tel que [PERT](https://fr.wikipedia.org/wiki/PERT) ou des [diagrammes temps/temps](https://www.ecommercemag.fr/Thematique/methodologie-1247/fiche-outils-10182/diagramme-temps-temps-308151.htm). Rappelez-vous que l’agilité joue sur le périmètre plus que le planning.

* [Loi de Douglas]() : C’est la version « physique » de la loi de Parkinson, qui peut être résumé par l’adage « Plus on a de la place, plus on s’étale ». Que l’on ait un petit bureau d’écolier ou un grand bureau d’angle de 3m, on aura toujours tendance à en occuper l’intégralité de la surface disponible. *L’origine de cette loi est assez brumeuse et je n’ai pas trouvé pourquoi elle porte ce nom*

<u>Outils utiles :</u> Est-il vraiment utile de combattre cette loi ? Un grand & utile management visuel est surement plus utile que de se contraindre dans un openspace ou imposer du flexoffice. Au mieux transformer votre salle café en [Obeya(https://fr.wikipedia.org/wiki/Obeya)] pour joindre l’utile à l’agréable :)

* [Loi de Carlson](https://www.helloworkplace.fr/loi-carlson/) : On doit cette loi à [Sune Carlson](https://en.wikipedia.org/wiki/Sune_Carlson) qui dans son livre « Executive Behaviour » publié en 1951 qui dit : « un travail réalisé en continu prend moins de temps et d’énergie que lorsqu’il est réalisé en plusieurs fois ». Cela exprime une évidence sur le fait que plus l’on est interrompu et moins nous sommes efficaces.

<u>Outils utiles :</u> La [technique du pomodoro](https://fr.wikipedia.org/wiki/Technique_Pomodoro) est souvent cité pour combattre cette loi. Je vous conseille aussi le « Jeu des prénoms » ([1](https://docs.google.com/presentation/d/18VsHRMqxQshqrFPVp_oCD8w6WDzJSMZaizMLeYvdHWg/edit#slide=id.g518d6b0fe_0_0))([2](https://oyomy.fr/2022/09/le-jeu-des-prenoms/)) pour expérimenter très concrètement cette loi avec tous ses inconvénients.

* [Loi de Laborit](https://www.helloworkplace.fr/loi-laborit/) : Ou « loi du moindre effort », nous la devons à un Français, [Henri Laborit](https://fr.wikipedia.org/wiki/Henri_Laborit). C’est aussi un biais cognitif qui nous pousse à faire en premier ce qui est rapide et facile, au détriment des tâches longue et désagréable même si elles sont plus urgentes.

<u>Outils utiles :</u> La [Matrice d’Eisenhower](https://fr.wikipedia.org/wiki/Matrice_d%27Eisenhower) est là encore un bon outil pour se motiver et ordonner correctement son effort.

* [Loi des rendements décroissants](https://fr.wikipedia.org/wiki/Loi_des_rendements_d%C3%A9croissants) : Concept assez ancien, datant de 1768 et trouve son origine dans les travaux de Turgot, puis Von Thünen, et enfin David Ricardo. Elle a été initialement décrite ainsi : « Les productions ne peuvent être exactement proportionnelles aux avances; elles ne le sont même pas, placées dans le même terrain, et l’on ne peut jamais supposer que des avances doubles donnent un produit double. ». Plus clairement elle dit que ce n'est pas en mettant deux fois plus de ressources (*avances*) que l'on obtiendra deux fois plus de résultat (*produit*), le contexte ne pouvant être totalement identique (*même terrain*). Il faut replacer cette loi dans son contexte de l'époque (XVIIIᵉ siècle), où la production agricole est au centre de l'économie. Mais finalement cette loi s'applique à presque tout, même à la productivité intellectuelle comme nous la met en exergue la loi d'Illich ci-dessous.

	* [loi d’Illich](https://www.observatoire-ocm.com/management/loi-illich/) : On la doit à Ivan Illich, un prêtre catholique et philosophe autrichien du XXᵉ siècle, très critique envers la société industrielle de son époque. C’est une application humaine de la loi des rendements décroissants, qui dit que « Après un certain nombre d’heures, la productivité du temps passé diminue d’abord et devient ensuite négative. ». <br/><br/><u>Outils utiles :</u> Faites des pauses ! C'est bien souvent ce qui est conseillé et le plus efficace. Vous pouvez aussi détourner la [technique du pomodoro](https://fr.wikipedia.org/wiki/Technique_Pomodoro) pour mettre des gardes fous sur le temps passé sur une tâche.

	* [Loi de Brooks](https://fr.wikipedia.org/wiki/Loi_de_Brooks) : Cette loi s’applique aussi plus physiquement sur l’organisation des équipes. Frederick Brooks l'énonce ainsi « Ajouter des personnes à un projet en retard accroît son retard », l'analogie plus parlante est	« avec 9 femmes on ne fait pas un bébé en 1 mois ». Au-delà d’un certain nombre les interactions dans l’équipe deviennent très nombreuses, les décisions plus longues à prendre et l’effort de synchronisation chronophage. La performance individuelle des membres baisse pour gérer la complexité du groupe.

	<u>Outils utiles :</u> La « [2 pizza team rule](https://www.techtarget.com/whatis/definition/two-pizza-rule) » est l’une des clés du succès d’Amazon. Les pizzas américaines n’ayant rien à voir avec celle outre atlantique, 6-8 personnes est plébiscité.

* [Loi de Segal](https://en.wikipedia.org/wiki/Segal%27s_law) [Fr](https://www.observatoire-ocm.com/management/loi-de-segal/) : S'énonce avec cette maxime « Un homme avec une montre sait quelle heure il est. Un homme avec deux montres n'en est jamais sûr. ». Mais peux aussi se rencontrer sous la [citation](https://citations.ouest-france.fr/citation-noel-mamere/trop-informations-tue-information-14258.html) « Trop d'information, tue l'information » qui préfigure l'infobésité actuelle. Vous l'aurez compris que pour prendre une décision il ne faut pas démultiplier les sources d'information sous peine de paralysie décisionnel.

<u>Outils utiles :</u> L'approche 40-70 de [Colin Powell](https://blog.princeyoulou.com/la-regle-des-40-70-de-colin-powell/) nous dit qu'il faut entre 40 et 70% des infos pour prendre la décision la plus sensé. En dessous vous êtes sous informé et vous tromperez trop souvent, au-dessus vous aurez trop de doutes pour être serein en plus d'une perte de temps. Votre « instinct » devant vous guider. Sinon Herbert Simon avec son [Satisficing Model / Satisficing ](https://en.wikipedia.org/wiki/Satisficing) peut vous aider. Elle se résume en « le mieux est l'ennemie du bien », ne cherchez pas la perfection, mais la décision qui vous satisfera. Sinon affiner votre demande/besoin vous aidera peut-être à y voir plus clair pour que la décision s'impose d'elle-même.

* [Loi de Fraisse](https://www.hellowork.com/fr-fr/medias/loi-fraisse-activite-gestion-temps.html): Nous viendrait du psychologue Français, [Paul Fraisse](https://fr.wikipedia.org/wiki/Paul_Fraisse). Son observation est toute simple et nous l'expérimentons tous les jours « La durée d'une activité nous parait toujours inversement proportionnel au plaisir que l'on y prend ».

<u>Outils utiles :</u> Faite une liste de vos tâches à faire pour la journée, classez suivant votre motivation à les faire et commencez par le bas de la pile. Ce n'est peut-être pas agréable mais vous motivera à réaliser rapidement les tâches rébarbatives au profit des plus plaisante. Une espèce de carotte au bout d'un bâton …

* [Loi de Taylor](http://pragma-tic.blogspot.com/2013/08/lois-generales-du-temps-la-loi-de-taylor.html) : Il s'agit bien de Frederick Winslow Taylor a qui on doit l'Organisation Scientifique du Travail (OST) et donc le Taylorisme dont Henry Ford s'est fortement inspiré pour ses usines. Elle s'énonce ainsi « L’ordre dans lequel nous effectuons une série de tâches influe directement sur le temps d’accomplissement unitaire de chacune d’entre elle, mais aussi sur le temps global de leur ensemble. »

<u>Outils utiles :</u> Un petit [rétro planning](https://asana.com/fr/resources/what-is-retroplanning) vous aidera à y voir plus claire. Sinon de manière plus ludique un [retour vers le futur / remember the futur](https://getyellow.medium.com/yellow-toolbox-6-i-remember-the-future-263ed5888c1b) vous donnera une esquisse de l'ordonnancement à adopter.

* [Loi de Swoboda-Fliess-Teltscher](https://nospensees.fr/la-loi-de-swoboda-fliess-teltscher-et-les-cycles-biologiques/) : Même si cette « Loi » a été démenti depuis (par [Terence Hines](https://web.archive.org/web/20090212181753/http://ammonsscientific.com/link.php?N=10326) en 1998) je préfère la cité puisqu’on la rencontre encore assez souvent. Succinctement, ces 3 professeurs pensait avoir identifié des cycles dans les performance intellectuel, émotionnel et physique. Profitant d'un [effet Barnum](https://fr.wikipedia.org/wiki/Effet_Barnum) cette pseudoscience s'est très largement démocratisé dans les années 70 à 90.

<u>Outils utiles :</u> User et abuser de la [zététique](https://fr.wikipedia.org/wiki/Z%C3%A9t%C3%A9tique) et d'esprit critique permet de nous prémunir de ce genre de dérive.

* [La loi du mouvement de Newton](https://fr.wikipedia.org/wiki/Lois_du_mouvement_de_Newton) : Pourquoi parler de Newton et d’une loi physique ici ? (ce n’est la gravité) Et bien elle s’applique aussi au comportement humain. La loi s’énonce ainsi : 
> « Tout corps persévère dans l’état de repos ou de mouvement uniforme en ligne droite dans lequel il se trouve, à moins que quelque force n’agisse sur lui, et ne le contraigne à changer d’état. »

Pour l’appliquer dans notre domaine il suffit de la changer que légèrement pour obtenir ceci :

> « Tout <del>corps</del> *comportement* persévère dans l’état de repos ou de mouvement <del>uniforme en ligne droite</del> dans lequel il se trouve, à moins que quelque force n’agisse sur lui, et ne le contraigne à changer d’état. »

Et en effet si on ne fait rien pour combattre une mauvaise habitude elle perdurera. De même pour changer la culture d’une équipe/entreprise il faut lui ancrer une habitude, qui ne peut être fait qu’en impulsant un mouvement. En tant qu’agent du changement appliquer une force inférieure à l’inertie du système est voué à l’échec, mais cet effort doit être dosé en fonction de l’acceptabilité du changement attendu.

<u>Outils utiles :</u> Je ne peux vous conseillez un outil puisque tout dépend de ce que voulez faire. Le mieux restant de vous attacher les services d'un coach Agile pour vous mettre sur la bonne voie.

* [Loi de Kotter](https://www.linkedin.com/pulse/loi-de-kotter-les-meilleurs-changements-commencent-boisvert/?originalSubdomain=fr) : On la doit à [John Kotter](https://fr.wikipedia.org/wiki/John_Kotter) sommité de la gestion du changement. Contrairement à toutes les lois précédentes celle-ci est un manuel en 8 étapes pour provoquer un changement profond et durable au sein des entreprises.

1. Identifiez les urgences 
2. Créez de puissantes équipes de travail
3. Développez une vision et une stratégie 
4. Communiquez la vision
5. Abattez les obstacles
6. Cherchez des gains à court terme
7. Consolidez les acquis
8. Ancrez le changement dans la culture d’entreprise

La première étape revêt un statut particulier car influe directement sur la réussite du projet, pour cela les meilleurs changements à mettre en place en premier commencent par des résultats immédiats (*Quick win*).

<u>Outils utiles :</u> [Ses livres](https://www.amazon.fr/Livres-John-Kotter/s?rh=n%3A301061%2Cp_27%3AJohn+Kotter) :)

[Loi de Brooks]()

<u>Outils utiles :</u> 

[Loi de Perls]()

<u>Outils utiles :</u> 

[Loi de l’alternance]()

<u>Outils utiles :</u> 

[Loi de Metcalfe]() : Metcalfe est un ingénieur, pionnier d’internet. La traduction de sa loi est « l’utilité d’un réseau est proportionnelle au carré du nombre de ses utilisateurs ».

# Effets

* [Effet Domino](https://fr.wikipedia.org/wiki/Effet_domino) : Un des plus connu et plus simple. Il s’agit d’une réaction en chaine d’évènements de même ampleur, physiquement ou thématiquement proche. Ces changements peuvent être voulus, maitrisés, délimités, … ou pas.

* [Effet Papillon](https://fr.wikipedia.org/wiki/Effet_papillon) : On pourrait le considérer comme un corolaire de l’effet Domino. La différence majeure est l’introduction du ratio entre la suite d’évènements. L’idée est qu’en changeant très légèrement les conditions initiales d’un système, le résultat peut-être d'une tout autre mesure. C’est Edward Lorenz qui est retenu comme le père de cette expression, l’ayant utilisé comme titre d’une conférence scientifique en 1972, sous la forme suivante :

> « Le battement d’ailes d’un papillon au Brésil peut-il provoquer une tornade au Texas ? »

<u>Outils utiles :</u> Faites des petits pas ! Moins vous avez d'encours, moins grand sont les risques de désastres. Donc le découpage, la priorisation 


* [Effet Dunning Kruger](https://fr.wikipedia.org/wiki/Effet_Dunning-Kruger) : Aussi connu sous le nom d’effet de « sur-confiance », qui n’est autre qu’un biais cognitif de surestimation de la maitrise d’un sujet. Des citations me viennent à l’esprit pour illustrer cet effet.

> « L’ignorant affirme, le savant doute, le sage réfléchit. » 
*Aristote*

> « Ceux qui savent ne parlent pas, ceux qui parlent ne savent pas. Le sage enseigne par ses actes, non par ses paroles. »
*Lao Tseu*

> « Certaines personnes écoutent la moitié, comprennent un quart, mais explique le double. » 
*Inconnu*

Une jolie image permet de bien comprendre aussi :
![Effet Dunning-Kruger](https://upload.wikimedia.org/wikipedia/commons/7/75/2019-06-19_effet_dunning_kruger.png){.image .width-75}

Bon il s’avère que la courbe est complètement biaisé et non représentative des résultats des expériences. Même la page [Wikipédia](https://fr.wikipedia.org/wiki/Dunning%E2%80%93Kruger_effect) induit en erreur sur l’interprétation des résulats. J’ai trouvé un point de vu éclairant sur cet effet [sur le site d’Aurélien Le Masson](https://biaiscognitifs.substack.com/p/biais-4-leffet-dunning-kruger).

<u>Outils utiles :</u> [Tourner 7 fois sa langue dans sa bouche](https://fr.wiktionary.org/wiki/tourner_sa_langue_sept_fois_dans_sa_bouche) :-p

* [Effets Pygmalion](https://fr.wikipedia.org/wiki/Effet_Pygmalion) : Aussi connu sous le nom d’effet Rosenthal et Jacobson. Biais cognitif qui fait en sorte que nous augmentons nos performances en fonction de l'image positive que nous renvoie les gens nous entourant. Effet largement démontré sur des humains, mais aussi sur des rats !!!!

* [Effets Golem](https://fr.wikipedia.org/wiki/Effet_Golem) : Jumeaux maléfique de l'effet Pygmalion. Si l'image qui nous est renvoyé est négatives alors nos performances s'amoindrissent.

* [Effets Hawthorne](https://fr.wikipedia.org/wiki/Effet_Hawthorne) : L'attention porté 

* [Effet Zeigarnik](https://fr.wikipedia.org/wiki/Effet_Zeigarnik) : On le doit à la psychologue russe [Bljuma Voulfovna Zeigarnik](https://fr.wikipedia.org/wiki/Bljuma_Zeigarnik) (Блюма Вульфовна Зейгарник) qui a permis d'établir ce biais cognitif en 1927 : 
> « Je me rappelle plus facilement les tâches et les problèmes que j’ai laissés en suspens ou qui sont en cours, que les problèmes que j’ai résolus. »

<u>Outils utiles :</u> Célébrez-vos succès/échecs/apprentissage !!! Si vous utilisez Scrum, les revues sont là pour ça, mais rien de vous empêche d'instaurer des plénières / démo / etc. sur un autre rythme. Mais ça peut aussi prendre la forme d'une lettre d'information diffuser a toute l'équipe/entreprise. Bref, tracez ce qui est fait et faites-le savoir.

# Autres
Ces autres cas ne sont pas énoncés sous forme de principe, lois ou effet mais restent tout de même intéressant à connaitre.

* [Facteur Pi](https://wikiagile.fr/index.php?title=La_constante_PI_dans_l%27estimation_logicielle) : On parle bien du chiffre 3,14… Alistair Cockburn lui-même en a fait un [article sur son blog](http://alistair.cockburn.us/The+magic+of+pi+for+project+managers) ([Fr](https://www.les-traducteurs-agiles.org/2017/12/06/la-magie-de-pi.html). Ce facteur essaye de deviner le coût réel d’un projet suivent l’estimation des dev. Pour ça il suffit de prendre le chiffre donné par ces derniers et le multiplier par π, π², ou √π (lisez l’article précédent pour choisir). En général il en résulte un projet à succès qui a fini dans les temps voir en avance !

<u>Outils utiles :</u> Rien de particulier à faire vu que c'est un outil utile en lui-même.


* [Rasoir d’Ockham](https://fr.wikipedia.org/wiki/Rasoir_d%27Ockham) : Aussi désigné par principe de simplicité, d’économie ou de parcimonie. Le rasoir étant plus rependu je l’ai préféré au classement dans les « Principes ». Au fait pourquoi « rasoir » ? Rien à voir avec l'outil permettant la tonsure du moine Ockham qui l’a formulé en 1319, mais doit être compris comme le principe philosophique d’élimination (« raser »).
> Pluralitas non est ponenda sine necessitate
> (les multiples ne doivent pas être utilisés sans nécessité)

Une reformulation plus moderne serait « les hypothèses suffisantes les plus simples doivent être préférées ».
Intuitivement des dictons viennent aussi à l’esprit pour l’illustrer, comme « L’explication la plus simple est généralement la bonne » ou « Pourquoi chercher compliqué quand plus simple suffit ? »

<u>Outils utiles :</u> Découpez ! Faites des boucles de rétroaction courtes ! Plus vous ferez des plans sur la comète et plus il y a des chances que vous soyez dans l'erreur. Donc autant vérifier le plus vite possible vos hypothèses.

* [Rasoir de Hanlon](https://fr.wikipedia.org/wiki/Rasoir_de_Hanlon) : C’est presque un corolaire au rasoir précédent et s’énonce ainsi :
> « Ne jamais attribuer à la malveillance ce que la bêtise suffit à expliquer. »

Une reformulation savoureuse l’illustre tout aussi bien.
> « Toujours préférer l’hypothèse de la connerie à celle du complot. La connerie est courante. Le complot exige un esprit rare »

*Michel Rocard*
N’en déplaise aux amateurs aux théories du complot, il est très difficile d’en échafauder et [encore plus de les conserver]( https://www.slate.fr/story/113255/complots-rester-secrets)([En]( https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0147905)).

<u>Outils utiles :</u> 

* [Nombre de Dunbar](https://fr.wikipedia.org/wiki/Nombre_de_Dunbar) : 150 ! C’est le nombre totalement arbitraire et avec une *petite* variance de 50%, qu’a sorti le chercheur Robin Dunbar et faisant établissant une corrélation entre la taille du néocortex cortex et le nombre d’individus dans des groupes de primates … extrapolé aux humains. Dans le genre *estimation au doigt mouillé* on n’est pas mal. Bref, ça serait le nombre de relation de *confiance* qu’un individu pourrait entretenir.
Heureusement d’autre études un poil plus sérieuse ([Twitter](https://www.numerama.com/business/18932-la-taille-du-cerveau-determine-le-nombre-d-amis-sur-twitter.html) / [Facebook](https://www.abc.net.au/news/science/2016-01-20/150-is-the-limit-of-real-facebook-friends/7101588)) l’on confirmé … [ou pas](https://royalsocietypublishing.org/doi/10.1098/rsbl.2021.0158).

<u>Outils utiles :</u> 

* [Hypothèse de la vitre brisée](https://fr.wikipedia.org/wiki/Hypoth%C3%A8se_de_la_vitre_bris%C3%A9e) : Plus rependu en science social, je pense qu’elle a toute sa place dans notre domaine. Cette analogie explique qu’intuitivement les individus sont moins enclins au respect s’ils constatent une dégradation du sujet en question. Inversement, les incivilités diminueront en corrélation au niveau de soin apporté. 
Pourquoi s’embêter à refaire toute une partie de code si un petit *if* dans un coin permet de faire ce que je veux ? Un ticket mal écrits de plus n’est pas grave au vu du tas déjà présent … non ?
Vous aurez compris les exemples. Il est donc important d’être rigoureux, de ne tolérer que le strict minimum d’écart, mais surtout d’être réactif face à ces incivilités.

<u>Outils utiles :</u> Politique zéro bug et priorité à la prod sont souvent le sommet de l'iceberg de tout un tas de pratique de gestion de la dette technique qui permettra de combattre de manière efficace cette hypothèse. Coté management faite de même, ne laissez des conflits couver, traitez au plus tôt pour éviter une contagion des problèmes.

* [Management des 3%](https://hugues.le-gendre.com/almanach/09/20-manager-pour-les-3-pourcents/) : Notre métier étant très humain, je trouve cet effet intéressant malgré le fait que l’on parle de management. Il tente de palier au fait de vouloir trouver des règles qui puisse régir toutes les personnes et processus. Il part du principe que dans toute organisation il y aura forcément un certain nombre de personnes qui tenteront de contourner ou mettre à leur profit une règle ou une absence de règle. Chercher à bloquer ces *3%* de *déviants* consumera bien plus que *3%* d’énergie, mais surtout génèrera une grande frustration de la part des 97% restant se sentant inutilement encadrer par une montagne de procédure et règles. 

<u>Outils utiles :</u> Pour cela il faut garder un lien fort avec toutes les personnes de l'entreprise. Facile et rapide au début, bien moins évident et bien plus long quand l'entreprise grandit. Cependant plusieurs outils peuvent être mis en place. 1/ La machine à café :) c'est là où les réunions les plus directes et productives ont lieu, donc autant y faire un tour régulièrement et y rester plusieurs heures pour discuter avec un maximum de monde. 2/ [Forum ouvert](https://fr.wikipedia.org/wiki/M%C3%A9thodologie_Forum_Ouvert). Surement un des meilleurs moyens pour récolter de très bonne suggestion. 3/ [«Ask me Anything»](https://fr.wikipedia.org/wiki/Ask_me_anything). Son coté informel et accessible en fait un moyen plébiscité des grandes structures. Ne pas hésiter à mettre un place un canal anonyme pour libérer au maximum la parole.

* [Théorie de l’étiquetage](https://fr.wikipedia.org/wiki/Th%C3%A9orie_de_l%27%C3%A9tiquetage) : Cette théorie est à relier aux effets Pygmalion et Golem dans le sens où elle fortement teinté d'auto-réalisation et démontre encore une fois l'importance des mots. Là où les effets précédents se limitent au positif ou négatif, la théorie essaye de généraliser. Décrire, catégoriser, classifier une personne ou un groupe l'incitera à renforcer son comportement avec les caractéristiques de l'étiquetage qu'on lui aura attribué.

<u>Outils utiles :</u> Cassez les silos … ou pas. Il peut être intéressant de combattre cette théorie pour ne pas limiter des personnes ou groupes, mais si l'on souhaite une haute expertise dans un domaine l'inverse peut s'avérer utile. Dans le premier cas, regarder du côté de [FAST](https://www.fastagile.io/fast-guide) serait intéressant. Dans le même temps mettre en place des communautés, inciter à faire des présentations interne ou externe des expertises en place, pousse l'autre face de cette théorie.


* [Théorie X & Y](https://fr.wikipedia.org/wiki/Th%C3%A9orie_X_et_th%C3%A9orie_Y) : Ces théories jumelles ont été développées dans les années 1960 par Douglas McGregor. D'un côté la « X » suppose que l'humain moyen n'aime pas le travail et doit être contrait où menacer pour qu'il s'y attèle. La « Y » affirme le contraire. La première pré-dispose à un cercle vicieux et induit un management autoritaire, alors que la seconde incite à un cercle vertueux avec un management participatif.

<u>Outils utiles :</u> Il existe tout une panoplie d'outils dans la branche des [entreprises libérées](https://fr.wikipedia.org/wiki/Entreprise_lib%C3%A9r%C3%A9e), où la [fonderie Favi](https://www.francetvinfo.fr/economie/emploi/carriere/vie-professionnelle/management/lentreprise-liberee-le-bien-etre-au-travail-selon-jean-francois-zobrist_4123751.html) nous donne une belle leçon de ce côté. Je vous suggère aussi de lire [« Reinventing Organisation » de Frédérique Laloux](https://www.chez-mon-libraire.fr/livre/9782354562519-reinventing-organizations-la-version-resumee-et-illustree-du-livre-phenomene-qui-invite-a-repenser-le-management-etienne-appert-frederic-laloux/).

* [Méthode Coué](https://fr.wikipedia.org/wiki/M%C3%A9thode_Cou%C3%A9) :  C'est méthode fondée sur l'autosuggestion et l'autohypnose, du psychologue et pharmacien français Émile Coué de la Châtaigneraie au début du XXe siècle. Elle est basée sur l'autosuggestion et l'autohypnose. Dans les grandes lignes elle suggère qu'il est possible d'influencer favorablement notre être inconscient par la suggestion, et de cette façon d'améliorer notre état tant physique que moral. Même si cette méthode est très utilisée dans le développement personnel, rien n'empêche de l'appliquer à toute une équipe. 

<u>Outils utiles :</u> Faites en sorte que toute l'équipe voie le verre à moitié plein, transformer vos échecs en apprentissage, inspirez là en lui suggèrent des lendemain (encore) meilleurs. Sans tombé dans l'excès bien sûr, et en bonus l'effet Pygmalion pourra jouer en votre faveur.
