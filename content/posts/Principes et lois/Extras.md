Title: Quelques principes et lois utiles … Extras !
Date: 2023-05-28
Modified: 2023-06-19
Tags: principes, lois
Keyword: principes, lois, culture
Category: management
Slug: Principes-lois-effets-4
Author: Matthieu
Description: Pareto, Parkinson, falaise de verre, … ça ne vous évoque rien ? Je vous explique tout par ici.
Summary: Avec un peu d’expérience sur un sujet il est facile de dégager de grand principe de fonctionnement. C’est un peu les dictons du monde moderne. Ces principes ou lois s’exerce aussi dans le monde de l’entreprise. Je vais tenter de vous en lister quelques un des plus intéressant, sans rentrer dans le détail (chacun pouvant faire l’objet l’un long billet), mais assez pour que vous puissiez les reconnaitre et les comprendre.
Status: draft
Cover: /images/principes-et-lois.avif

Les 2 premiers billets étaient déjà bien chargés mais il m’en restait encore dans ma besace. Du coup je n’ai pu résister à l’envie de vous livrer quelques lignes de plus :)

# Extras !
Ces autres cas ne sont pas énoncés sous forme de principe, lois ou effet mais restent tout de même intéressant à connaitre.

* [Facteur Pi](https://wikiagile.fr/index.php?title=La_constante_PI_dans_l%27estimation_logicielle) : On parle bien du chiffre 3,14… Alistair Cockburn lui-même en a fait un [article sur son blog](http://alistair.cockburn.us/The+magic+of+pi+for+project+managers) ([Fr](https://www.les-traducteurs-agiles.org/2017/12/06/la-magie-de-pi.html). Ce facteur essaye de deviner le coût réel d’un projet suivent l’estimation des dev. Pour ça il suffit de prendre le chiffre donné par ces derniers et le multiplier par π, π², ou √π (lisez l’article précédent pour choisir). En général il en résulte un projet à succès qui a fini dans les temps voir en avance ! 

<u>Outils utiles :</u> Rien de particulier à faire vu que c'est un outil utile en lui-même.


* [Rasoir d’Ockham](https://fr.wikipedia.org/wiki/Rasoir_d%27Ockham) : Aussi désigné par principe de simplicité, d’économie ou de parcimonie. Le rasoir étant plus rependu je l’ai préféré au classement dans les « Principes ». Au fait pourquoi « rasoir » ? Rien à voir avec l'outil permettant la tonsure du moine Ockham qui l’a formulé en 1319, mais doit être compris comme le principe philosophique d’élimination (« raser »).
> Pluralitas non est ponenda sine necessitate
> (les multiples ne doivent pas être utilisés sans nécessité)

Une reformulation plus moderne serait « les hypothèses suffisantes les plus simples doivent être préférées ».
Intuitivement des dictons viennent aussi à l’esprit pour l’illustrer, comme « L’explication la plus simple est généralement la bonne » ou « Pourquoi chercher compliqué quand plus simple suffit ? »

<u>Outils utiles :</u> Découpez ! Faites des boucles de rétroaction courtes ! Plus vous ferez des plans sur la comète et plus il y a des chances que vous soyez dans l'erreur. Donc autant vérifier le plus vite possible vos hypothèses.

* [Rasoir de Hanlon](https://fr.wikipedia.org/wiki/Rasoir_de_Hanlon) : C’est presque un corolaire au rasoir précédent et s’énonce ainsi :
> « Ne jamais attribuer à la malveillance ce que la bêtise suffit à expliquer. »

Une reformulation savoureuse l’illustre tout aussi bien.
> « Toujours préférer l’hypothèse de la connerie à celle du complot. La connerie est courante. Le complot exige un esprit rare »

*Michel Rocard*
N’en déplaisent aux amateurs aux théories du complot, il est très difficile d’en échafauder et [encore plus de les conserver]( https://www.slate.fr/story/113255/complots-rester-secrets)([En]( https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0147905)).

<u>Outils utiles : La transparence ! Avec une pincé de management visuel. Rien de plus. Si vous ne laissez aucune prise au doute et à la méfiance, vous ne devriez pas pouvoir être pris en défaut.</u> 

* [Nombre de Dunbar](https://fr.wikipedia.org/wiki/Nombre_de_Dunbar) : 150 ! C’est le nombre totalement arbitraire et avec une *petite* variance de 50%, qu’a sorti le chercheur Robin Dunbar en établissant une corrélation entre la taille du néocortex et le nombre d’individus dans des groupes de primates … extrapolé aux humains. Dans le genre *estimation au doigt mouillé* on n’est pas mal. Bref, ça serait le nombre de relation de *confiance* qu’un individu pourrait entretenir.
Heureusement d’autre études un poil plus sérieuse ([Twitter](https://www.numerama.com/business/18932-la-taille-du-cerveau-determine-le-nombre-d-amis-sur-twitter.html) / [Facebook](https://www.abc.net.au/news/science/2016-01-20/150-is-the-limit-of-real-facebook-friends/7101588)) l’on confirmé … [ou pas](https://royalsocietypublishing.org/doi/10.1098/rsbl.2021.0158).

<u>Outils utiles : Dé-scalez !  Garder vos groupes/équipes les plus petits possibles, quitte à ne pas aller aussi vite que vous voudriez sur le moment. ~Anihilez~ Réduisez aux maximum vos dépendances, vous aurez moins d’interaction à gérer.</u> 

* [Hypothèse de la vitre brisée](https://fr.wikipedia.org/wiki/Hypoth%C3%A8se_de_la_vitre_bris%C3%A9e) : Plus rependu en science social, je pense qu’elle a toute sa place dans notre domaine. Cette analogie explique qu’intuitivement les individus sont moins enclins au respect s’ils constatent une dégradation du sujet en question. Inversement, les incivilités diminueront en corrélation au niveau de soin apporté. 
Pourquoi s’embêter à refaire toute une partie de code si un petit *if* dans un coin permet de faire ce que je veux ? Un ticket mal écrits de plus n’est pas grave au vu du tas déjà présent … non ?
Vous aurez compris les exemples. Il est donc important d’être rigoureux, de ne tolérer que le strict minimum d’écart, mais surtout d’être réactif face à ces incivilités.

<u>Outils utiles :</u> Politique zéro bug et priorité à la prod sont souvent le sommet de l'iceberg de tout un tas de pratiques de gestion de la dette technique, qui permettra de combattre de manière efficace cette hypothèse. Coté management faite de même, ne laissez des conflits couver, traitez au plus tôt pour éviter une contagion des problèmes.

* [Management des 3%](https://hugues.le-gendre.com/almanach/09/20-manager-pour-les-3-pourcents/) : Notre métier étant très humain, je trouve cet effet intéressant malgré le fait que l’on parle de management. Il tente de palier au fait de vouloir trouver des règles qui puisse régir toutes les personnes et processus. Il part du principe que dans toute organisation il y aura forcément un certain nombre de personnes qui tenteront de contourner ou mettre à leur profit une règle ou une absence de règle. Chercher à bloquer ces *3%* de *déviants* consumera bien plus que *3%* d’énergie, mais surtout génèrera une grande frustration de la part des 97% restant se sentant inutilement encadrer par une montagne de procédure et règles. 

<u>Outils utiles :</u> Pour cela il faut garder un lien fort avec toutes les personnes de l'entreprise. Facile et rapide au début, bien moins évident et bien plus long quand l'entreprise grandit. Cependant plusieurs outils peuvent être mis en place. 1/ La machine à café :) c'est là où les réunions les plus directes et productives ont lieu, donc autant y faire un tour régulièrement et y rester plusieurs heures pour discuter avec un maximum de monde. 2/ [Forum ouvert](https://fr.wikipedia.org/wiki/M%C3%A9thodologie_Forum_Ouvert). Surement un des meilleurs moyens pour récolter de très bonne suggestion. 3/ [«Ask me Anything»](https://fr.wikipedia.org/wiki/Ask_me_anything). Son coté informel et accessible en fait un moyen plébiscité des grandes structures. Ne pas hésiter à mettre un place un canal anonyme pour libérer au maximum la parole.

* [Théorie de l’étiquetage](https://fr.wikipedia.org/wiki/Th%C3%A9orie_de_l%27%C3%A9tiquetage) : Cette théorie est à relier aux effets Pygmalion et Golem dans le sens où elle fortement teinté d'auto-réalisation et démontre encore une fois l'importance des mots. Là où les effets précédents se limitent au positif ou négatif, la théorie essaye de généraliser. Décrire, catégoriser, classifier une personne ou un groupe l'incitera à renforcer son comportement avec les caractéristiques de l'étiquetage qu'on lui aura attribué.

<u>Outils utiles :</u> Cassez les silos … ou pas. Il peut être intéressant de combattre cette théorie pour ne pas limiter des personnes ou groupes, mais si l'on souhaite une haute expertise dans un domaine l'inverse peut s'avérer utile. Dans le premier cas, regarder du côté de [FAST](https://www.fastagile.io/fast-guide) serait intéressant. Dans le même temps mettre en place des communautés, inciter à faire des présentations interne ou externe des expertises en place, pousse l'autre face de cette théorie.


* [Théorie X & Y](https://fr.wikipedia.org/wiki/Th%C3%A9orie_X_et_th%C3%A9orie_Y) : Ces théories jumelles ont été développées dans les années 1960 par Douglas McGregor. D'un côté la « X » suppose que l'humain moyen n'aime pas le travail et doit être contrait où menacer pour qu'il s'y attèle. La « Y » affirme le contraire. La première pré-dispose à un cercle vicieux et induit un management autoritaire, alors que la seconde incite à un cercle vertueux avec un management participatif.

<u>Outils utiles :</u> Il existe tout une panoplie d'outils dans la branche des [entreprises libérées](https://fr.wikipedia.org/wiki/Entreprise_lib%C3%A9r%C3%A9e), où la [fonderie Favi](https://www.francetvinfo.fr/economie/emploi/carriere/vie-professionnelle/management/lentreprise-liberee-le-bien-etre-au-travail-selon-jean-francois-zobrist_4123751.html) nous donne une belle leçon de ce côté. Je vous suggère aussi de lire [« Reinventing Organisation » de Frédérique Laloux](https://www.chez-mon-libraire.fr/livre/9782354562519-reinventing-organizations-la-version-resumee-et-illustree-du-livre-phenomene-qui-invite-a-repenser-le-management-etienne-appert-frederic-laloux/).

* [Méthode Coué](https://fr.wikipedia.org/wiki/M%C3%A9thode_Cou%C3%A9) :  C'est méthode fondée sur l'autosuggestion et l'autohypnose, du psychologue et pharmacien français Émile Coué de la Châtaigneraie au début du XXe siècle. Elle est basée sur l'autosuggestion et l'autohypnose. Dans les grandes lignes elle suggère qu'il est possible d'influencer favorablement notre être inconscient par la suggestion, et de cette façon d'améliorer notre état tant physique que moral. Même si cette méthode est très utilisée dans le développement personnel, rien n'empêche de l'appliquer à toute une équipe. 

<u>Outils utiles :</u> Faites en sorte que toute l'équipe voie le verre à moitié plein, transformer vos échecs en apprentissage, inspirez là en lui suggèrent des lendemain (encore) meilleurs. Sans tombé dans l'excès bien sûr, et en bonus l'effet Pygmalion pourra jouer en votre faveur.
