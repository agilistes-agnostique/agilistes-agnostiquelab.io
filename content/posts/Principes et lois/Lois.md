Title: Quelques principes, lois et effets utiles 2/3
Date: 2023-05-28
Modified: 2023-06-19
Tags: principes, lois
Keyword: principes, lois, culture
Category: management
Slug: Principes-lois-effets-2
Author: Matthieu
Description: Pareto, Parkinson, falaise de verre, … ça ne vous évoque rien ? Je vous explique tout par ici.
Summary: Avec un peu d’expérience sur un sujet il est facile de dégager de grand principe de fonctionnement. C’est un peu les dictons du monde moderne. Ces principes ou lois s’exerce aussi dans le monde de l’entreprise. Je vais tenter de vous en lister quelques un des plus intéressant, sans rentrer dans le détail (chacun pouvant faire l’objet l’un long billet), mais assez pour que vous puissiez les reconnaitre et les comprendre.
Status: draft
Cover: /images/principes-et-lois.avif

# Lois
* [Loi de Murphy](https://fr.wikipedia.org/wiki/Loi_de_Murphy) ou Loi de l’emmerdement maximal (LEM) : C’est ma petite préférée celle-là. « Si cela peut mal se passer, cela arrivera » On doit cette formulation à [George Nichols](https://fr.wikipedia.org/wiki/George_Nichols), celle du [professeur Murphy](https://fr.wikipedia.org/wiki/Edward_A._Murphy_Jr.) étant « Si ce gars a la moindre possibilité de faire une erreur, il la fera » Tout cela à cause d’un assistant qui monta à l’envers TOUS les capteurs de l’expérience en cours. Il y avait au moins une certaine constance dans son action.
Il y a plein de corolaire et d’extension à cette loi, mais si je devais en retenir 2 particulièrement ça serais celles-là :
	* Corolaire de Finagle « Tout ce qui peut aller mal le fera au pire moment. »
	* Loi des séries : « Les emmerdes c’est comme les cons, ça vole toujours en escadrille. »  Jacques Chirac
Je vous fais grâce de vous les expliquer, elles sont assez parlantes d’elle-même.

<u>Outils utiles :</u> Les catastrophes sont souvent un enchainement d’erreurs individuel. Le mieux pour le contrer est de ne pas faire les tâches importantes seul. Pour cela le *pair*[ce que vous voulez] est un bon moyen. Dans le livre [Renversez la vapeur !](https://www.chez-mon-libraire.fr/livre/9782892259926-renversez-la-vapeur-l-histoire-vraie-d-un-capitaine-qui-a-su-transformer-des-executants-en-leaders-l-david-marquet/) (traduction de « Turn the ship around ») en plus de faire les actions en pair il recommande de vocaliser tout ce qui va être fait en amont de leur réalisation. Cela permettrait de se rendre compte soi-même ou son partenaire d’une errer potentiel.

* [Loi de puissance](https://fr.wikipedia.org/wiki/Loi_de_puissance) : Très utilisé en sciences « dur », elle a cependant une application concrète en sociologie. Elle permette de décrire tous les phénomènes qui présentent une invariance d’échelle. Par exemple les sites de type wikis suivent la règle des 90-9-1 : 90 % de la population utilisatrice ne contribue pas ; 9% sont des contributeurs occasionnels et 1% des visiteurs du site contribue régulièrement. Ces proportions ne changeant pas que vous vous appeliez « Wikipédia » ou « LeWikiDuTressageDeRoseau ». Ou plus près de notre domaine qu’est la gestion de projet Agile, que vous gériez une équipe de 9 personnes ou 900, la répartition de vos problématiques ne changeront qu’à la marge.

<u>Outils utiles :</u> Seul la connaissance vous aidera sur le sujet. Pour ça formez-vous ! Mettez en place des métriques de vos utilisateurs.

* [Loi de Pareto](https://fr.wikipedia.org/wiki/Loi_de_Pareto) : Quoi ? Encore lui ? Eh bien oui. Le principe ayant une application assez générique, la loi elle est plus rigoureuse et est utilisé en sciences physique et sociales. C’est un type particulier de la loi de puissance. Cet outil met en évidence la loi des 80/20. Autrement dit, agir sur 20 % de causes permet de résoudre 80 % du problème. Le Pareto est utile pour identifier sur quelle cause agir en priorité pour améliorer de façon significative la situation. 

<u>Outils utiles :</u> Le bien nommé [diagramme de Pareto](https://fr.wikipedia.org/wiki/Diagramme_de_Pareto) vous permettra d’identifier les causes premières et donc agir de manière efficiente sur le problème.

* [Loi normale](https://fr.wikipedia.org/wiki/Loi_normale) (ou courbe de Gauss) : Issue du domaine des probabilités et statistique elle permet de modéliser des phénomènes natures issus de plusieurs évènements aléatoires. Prenez l’espérance mathématique (le résultat le plus probable) (noté : μ) d’un phénomène croisé avec son écart type (σ) et vous obtenez la répartition des résultats. En image ça donne ça :
![Distribution normal](https://upload.wikimedia.org/wikipedia/commons/thumb/7/74/Normal_Distribution_PDF.svg/720px-Normal_Distribution_PDF.svg.png){.image}
La hauteur des courbes représentant le nombre d’occurrence du résultat obtenu. 
Dans un processus de transformation (agile) d’une organisation vous pourriez en déduire le nombre de personne contre, neutre ou favorable avec un petit échantillon du personnel de l’entreprise de base, puis une extrapolation à l’ensemble de l’entreprise.

<u>Outils utiles :</u> Vu que c’est un outil en lui-même je n’ai rien a suggéré si ce n’est un point d’attention sur vos métriques. Choisissez-les avec soin et assurez-vous que vos échantillons soient vraiment représentatifs.

* [Loi de Parkinson](https://fr.wikipedia.org/wiki/Loi_de_Parkinson) : Énoncé en 1955 par Cyril Northcote Parkinson dans un article publié dans la revue *The Economist*, elle a été formulé ainsi « Le travail s’étend de manière à remplir le temps disponible pour son achèvement. […] ». Quel que soit le temps que vous (vous) accordez pour la réalisation d’une tâche, l’intégralité de ce temps sera consommé. C’est l’une des raisons pour lequel Scrum et d’autre méthodes Agiles se fixe des bulle temporel (timebox) et un rythme particulier. En plus d’éviter les effets tunnels, ça évite surtout de se perdre dans des détails (Pareto) de moindre importance. <br/><br/><u>Outils utiles :</u> Levez le nez du guidon ! Pour ça des ~~Sprints~~ cycles sont intéressants pour faire un point régulièrement (démo / rétro). Une autre manière est aussi de découper plus finement vos travaux, le chiffrage n’en sera que plus précis et juste (essayez le [carapaccio elephant](https://www.occitech.fr/blog/2014/05/decoupez-vos-stories-en-carpaccio/)). Rétrospectivement un [graphique de contrôle](https://fr.wikipedia.org/wiki/Carte_de_contr%C3%B4le) vous permettra d’identifier les anomalies pour en identifier les causes.

	* [Corolaire futilité de Parkinson](https://fr.wikipedia.org/wiki/Loi_de_futilit%C3%A9_de_Parkinson) : L’idée est que les organisations donnent une importance bien plus important sur des détails secondaires qu’au réel problème de fond. Typiquement dans une réunion pour la construction d’un abri à vélo, les discutions autour de sa couleur occupera bien plus de temps que son emplacement ou sa capacité. Cela peut s’expliquer par le fait que s’exprimer sur des caractéristiques essentielles implique une prise de responsabilité proportionnel et donc une mise en cause directe si un mauvais choix a été fait. Alors que la couleur n’implique qu’une responsabilité minime et au pire avoir « mauvais goût ».

	<u>Outils utiles :</u> Ne laissez pas de place au hasard ! Priorisez vos objectifs, préparer votre ordre du jour, invitez uniquement les personnes pertinentes, …

* [Loi de Hofstadter](https://fr.wikipedia.org/wiki/Loi_de_Hofstadter) : Elle vient en complément de la loi de Parkinson et s’énonce ainsi « Il faut toujours plus de temps que prévu, même en tenant compte de la loi de Hofstadter. ». Je vous laisse en déduire les implications dans la planification et estimation des projets. Jetez un œil au *Facteur Pi* un peu plus bas si vous n’avez pas encore perdu la foi dans ce genre d’exercice.

<u>Outils utiles :</u> Essayer de maitriser cette loi va rapidement vous faire basculer du coté *gestion de projet classique*, avec des outils tel que [PERT](https://fr.wikipedia.org/wiki/PERT) ou des [diagrammes temps/temps](https://www.ecommercemag.fr/Thematique/methodologie-1247/fiche-outils-10182/diagramme-temps-temps-308151.htm). Rappelez-vous que l’agilité joue sur le périmètre plus que le planning.

* [Loi de Douglas]() : C’est la version « physique » de la loi de Parkinson, qui peut être résumé par l’adage « Plus on a de la place, plus on s’étale ». Que l’on ait un petit bureau d’écolier ou un grand bureau d’angle de 3m, on aura toujours tendance à en occuper l’intégralité de la surface disponible. *L’origine de cette loi est assez brumeuse et je n’ai pas trouvé pourquoi elle porte ce nom*

<u>Outils utiles :</u> Est-il vraiment utile de combattre cette loi ? Un grand & utile management visuel est surement plus utile que de se contraindre dans un openspace ou imposer du flexoffice. Au mieux transformer votre salle café en [Obeya(https://fr.wikipedia.org/wiki/Obeya)] pour joindre l’utile à l’agréable :)

* [Loi de Carlson](https://www.helloworkplace.fr/loi-carlson/) : On doit cette loi à [Sune Carlson](https://en.wikipedia.org/wiki/Sune_Carlson) qui dans son livre « Executive Behaviour » publié en 1951 qui dit : « un travail réalisé en continu prend moins de temps et d’énergie que lorsqu’il est réalisé en plusieurs fois ». Cela exprime une évidence sur le fait que plus l’on est interrompu et moins nous sommes efficaces.

<u>Outils utiles :</u> La [technique du pomodoro](https://fr.wikipedia.org/wiki/Technique_Pomodoro) est souvent cité pour combattre cette loi. Je vous conseille aussi le « Jeu des prénoms » ([1](https://docs.google.com/presentation/d/18VsHRMqxQshqrFPVp_oCD8w6WDzJSMZaizMLeYvdHWg/edit#slide=id.g518d6b0fe_0_0))([2](https://oyomy.fr/2022/09/le-jeu-des-prenoms/)) pour expérimenter très concrètement cette loi avec tous ses inconvénients.

* [Loi de Laborit](https://www.helloworkplace.fr/loi-laborit/) : Ou « loi du moindre effort », nous la devons à un Français, [Henri Laborit](https://fr.wikipedia.org/wiki/Henri_Laborit). C’est aussi un biais cognitif qui nous pousse à faire en premier ce qui est rapide et facile, au détriment des tâches longue et désagréable même si elles sont plus urgentes.

<u>Outils utiles :</u> La [Matrice d’Eisenhower](https://fr.wikipedia.org/wiki/Matrice_d%27Eisenhower) est là encore un bon outil pour se motiver et ordonner correctement son effort.

* [Loi des rendements décroissants](https://fr.wikipedia.org/wiki/Loi_des_rendements_d%C3%A9croissants) : Concept assez ancien, datant de 1768 et trouve son origine dans les travaux de Turgot, puis Von Thünen, et enfin David Ricardo. Elle a été initialement décrite ainsi : « Les productions ne peuvent être exactement proportionnelles aux avances; elles ne le sont même pas, placées dans le même terrain, et l’on ne peut jamais supposer que des avances doubles donnent un produit double. ». Plus clairement elle dit que ce n'est pas en mettant deux fois plus de ressources (*avances*) que l'on obtiendra deux fois plus de résultat (*produit*), le contexte ne pouvant être totalement identique (*même terrain*). Il faut replacer cette loi dans son contexte de l'époque (XVIIIᵉ siècle), où la production agricole est au centre de l'économie. Mais finalement cette loi s'applique à presque tout, même à la productivité intellectuelle comme nous la met en exergue la loi d'Illich ci-dessous.

	* [loi d’Illich](https://www.observatoire-ocm.com/management/loi-illich/) : On la doit à Ivan Illich, un prêtre catholique et philosophe autrichien du XXᵉ siècle, très critique envers la société industrielle de son époque. C’est une application humaine de la loi des rendements décroissants, qui dit que « Après un certain nombre d’heures, la productivité du temps passé diminue d’abord et devient ensuite négative. ». <br/><br/><u>Outils utiles :</u> Faites des pauses ! C'est bien souvent ce qui est conseillé et le plus efficace. Vous pouvez aussi détourner la [technique du pomodoro](https://fr.wikipedia.org/wiki/Technique_Pomodoro) pour mettre des gardes fous sur le temps passé sur une tâche.

	* [Loi de Brooks](https://fr.wikipedia.org/wiki/Loi_de_Brooks) : Cette loi s’applique aussi plus physiquement sur l’organisation des équipes. Frederick Brooks l'énonce ainsi « Ajouter des personnes à un projet en retard accroît son retard », l'analogie plus parlante est	« avec 9 femmes on ne fait pas un bébé en 1 mois ». Au-delà d’un certain nombre les interactions dans l’équipe deviennent très nombreuses, les décisions plus longues à prendre et l’effort de synchronisation chronophage. La performance individuelle des membres baisse pour gérer la complexité du groupe.

	<u>Outils utiles :</u> La « [2 pizza team rule](https://www.techtarget.com/whatis/definition/two-pizza-rule) » est l’une des clés du succès d’Amazon. Les pizzas américaines n’ayant rien à voir avec celle outre atlantique, 6-8 personnes est plébiscité.

* [Loi de Segal](https://en.wikipedia.org/wiki/Segal%27s_law) [Fr](https://www.observatoire-ocm.com/management/loi-de-segal/) : S'énonce avec cette maxime « Un homme avec une montre sait quelle heure il est. Un homme avec deux montres n'en est jamais sûr. ». Mais peux aussi se rencontrer sous la [citation](https://citations.ouest-france.fr/citation-noel-mamere/trop-informations-tue-information-14258.html) « Trop d'information, tue l'information » qui préfigure l'infobésité actuelle. Vous l'aurez compris que pour prendre une décision il ne faut pas démultiplier les sources d'information sous peine de paralysie décisionnel.

<u>Outils utiles :</u> L'approche 40-70 de [Colin Powell](https://blog.princeyoulou.com/la-regle-des-40-70-de-colin-powell/) nous dit qu'il faut entre 40 et 70% des infos pour prendre la décision la plus sensé. En dessous vous êtes sous informé et vous tromperez trop souvent, au-dessus vous aurez trop de doutes pour être serein en plus d'une perte de temps. Votre « instinct » devant vous guider. Sinon Herbert Simon avec son [Satisficing Model / Satisficing ](https://en.wikipedia.org/wiki/Satisficing) peut vous aider. Elle se résume en « le mieux est l'ennemie du bien », ne cherchez pas la perfection, mais la décision qui vous satisfera. Sinon affiner votre demande/besoin vous aidera peut-être à y voir plus clair pour que la décision s'impose d'elle-même.

* [Loi de Fraisse](https://www.hellowork.com/fr-fr/medias/loi-fraisse-activite-gestion-temps.html): Nous viendrait du psychologue Français, [Paul Fraisse](https://fr.wikipedia.org/wiki/Paul_Fraisse). Son observation est toute simple et nous l'expérimentons tous les jours « La durée d'une activité nous parait toujours inversement proportionnel au plaisir que l'on y prend ».

<u>Outils utiles :</u> Faite une liste de vos tâches à faire pour la journée, classez suivant votre motivation à les faire et commencez par le bas de la pile. Ce n'est peut-être pas agréable mais vous motivera à réaliser rapidement les tâches rébarbatives au profit des plus plaisante. Une espèce de carotte au bout d'un bâton …

* [Loi de Taylor](http://pragma-tic.blogspot.com/2013/08/lois-generales-du-temps-la-loi-de-taylor.html) : Il s'agit bien de Frederick Winslow Taylor a qui on doit l'Organisation Scientifique du Travail (OST) et donc le Taylorisme dont Henry Ford s'est fortement inspiré pour ses usines. Elle s'énonce ainsi « L’ordre dans lequel nous effectuons une série de tâches influe directement sur le temps d’accomplissement unitaire de chacune d’entre elle, mais aussi sur le temps global de leur ensemble. »

<u>Outils utiles :</u> Un petit [rétro planning](https://asana.com/fr/resources/what-is-retroplanning) vous aidera à y voir plus claire. Sinon de manière plus ludique un [retour vers le futur / remember the futur](https://getyellow.medium.com/yellow-toolbox-6-i-remember-the-future-263ed5888c1b) vous donnera une esquisse de l'ordonnancement à adopter.

* [Loi de Swoboda-Fliess-Teltscher](https://nospensees.fr/la-loi-de-swoboda-fliess-teltscher-et-les-cycles-biologiques/) : Même si cette « Loi » a été démenti depuis (par [Terence Hines](https://web.archive.org/web/20090212181753/http://ammonsscientific.com/link.php?N=10326) en 1998) je préfère la cité puisqu’on la rencontre encore assez souvent. Succinctement, ces 3 professeurs pensait avoir identifié des cycles dans les performance intellectuel, émotionnel et physique. Profitant d'un [effet Barnum](https://fr.wikipedia.org/wiki/Effet_Barnum) cette pseudoscience s'est très largement démocratisé dans les années 70 à 90.

<u>Outils utiles :</u> User et abuser de la [zététique](https://fr.wikipedia.org/wiki/Z%C3%A9t%C3%A9tique) et d'esprit critique permet de nous prémunir de ce genre de dérive.

* [La loi du mouvement de Newton](https://fr.wikipedia.org/wiki/Lois_du_mouvement_de_Newton) : Pourquoi parler de Newton et d’une loi physique ici ? (ce n’est la gravité) Et bien elle s’applique aussi au comportement humain. La loi s’énonce ainsi : 
> « Tout corps persévère dans l’état de repos ou de mouvement uniforme en ligne droite dans lequel il se trouve, à moins que quelque force n’agisse sur lui, et ne le contraigne à changer d’état. »

Pour l’appliquer dans notre domaine il suffit de la changer que légèrement pour obtenir ceci :

> « Tout <del>corps</del> *comportement* persévère dans l’état de repos ou de mouvement <del>uniforme en ligne droite</del> dans lequel il se trouve, à moins que quelque force n’agisse sur lui, et ne le contraigne à changer d’état. »

Et en effet si on ne fait rien pour combattre une mauvaise habitude elle perdurera. De même pour changer la culture d’une équipe/entreprise il faut lui ancrer une habitude, qui ne peut être fait qu’en impulsant un mouvement. En tant qu’agent du changement appliquer une force inférieure à l’inertie du système est voué à l’échec, mais cet effort doit être dosé en fonction de l’acceptabilité du changement attendu.

<u>Outils utiles :</u> Je ne peux vous conseillez un outil puisque tout dépend de ce que voulez faire. Le mieux restant de vous attacher les services d'un coach Agile pour vous mettre sur la bonne voie.

* [Loi de Kotter](https://www.linkedin.com/pulse/loi-de-kotter-les-meilleurs-changements-commencent-boisvert/?originalSubdomain=fr) : On la doit à [John Kotter](https://fr.wikipedia.org/wiki/John_Kotter) sommité de la gestion du changement. Contrairement à toutes les lois précédentes celle-ci est un manuel en 8 étapes pour provoquer un changement profond et durable au sein des entreprises.

1. Identifiez les urgences 
2. Créez de puissantes équipes de travail
3. Développez une vision et une stratégie 
4. Communiquez la vision
5. Abattez les obstacles
6. Cherchez des gains à court terme
7. Consolidez les acquis
8. Ancrez le changement dans la culture d’entreprise

La première étape revêt un statut particulier car influe directement sur la réussite du projet, pour cela les meilleurs changements à mettre en place en premier commencent par des résultats immédiats (*Quick win*).

<u>Outils utiles :</u> [Ses livres](https://www.amazon.fr/Livres-John-Kotter/s?rh=n%3A301061%2Cp_27%3AJohn+Kotter) :)

[Loi de Brooks]()

<u>Outils utiles :</u> 

[Loi de Perls]()

<u>Outils utiles :</u> 

[Loi de l’alternance]()

<u>Outils utiles :</u> 

[Loi de Metcalfe]() : Metcalfe est un ingénieur, pionnier d’internet. La traduction de sa loi est « l’utilité d’un réseau est proportionnelle au carré du nombre de ses utilisateurs ».
