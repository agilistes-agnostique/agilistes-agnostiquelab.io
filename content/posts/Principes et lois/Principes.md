Title: Quelques principes, lois et effets utiles 1/3
Date: 2023-05-28
Modified: 2023-06-19
Tags: principes, lois
Keyword: principes, lois, culture
Category: management
Slug: Principes-lois-effets-1
Author: Matthieu
Description: Pareto, Parkinson, falaise de verre, … ça ne vous évoque rien ? Je vous explique tout par ici.
Summary: Avec un peu d’expérience sur un sujet il est facile de dégager de grand principe de fonctionnement. C’est un peu les dictons du monde moderne. Ces principes ou lois s’exerce aussi dans le monde de l’entreprise. Je vais tenter de vous en lister quelques un des plus intéressant, sans rentrer dans le détail (chacun pouvant faire l’objet l’un long billet), mais assez pour que vous puissiez les reconnaitre et les comprendre.
Status: draft
Cover: /images/principes-et-lois.avif

Page regroupant tous les principes, effets, lois et autres axiomes intéressant à connaitre pour la gestion de projets. Les explications ne sont absolument pas complètes mais devrait vous servir de pointeurs pour approfondir le sujet. Les « outils utiles » sont à prendre comme des pistes à explorer, mais il existe évidemment pleins d’autres alternatives.

# Avant-propos

Précision de vocabulaire pour comprendre de quoi on parle :
* [Loi](https://fr.wiktionary.org/wiki/loi) : Règle, obligations écrites, prescrites ou tacites, auxquelles les personnes se doivent de se conformer. (Sciences) Postulat ou énoncé vrai sous certaines conditions. (Physique) Ce qui règle l’ordre du monde physique. 

* [Principe](https://fr.wiktionary.org/wiki/principe) : Première règle d’un art ou d’une discipline. (Sciences) Loi que certaines observations ont d’abord rendue vraisemblable et à laquelle on a donné ensuite la plus grande généralité. (Courant) Règle de conduite d’une personne ou d’un groupe.

En science « dure » la distinction est assez simple puisqu’une loi ne souffre pas d’exception *(à moins que les cas particuliers ne soient intégrés à la loi)*.
Dans notre cas la différence est plus subtile. D’un côté avec les *lois* on a une règle de fonctionnement venant *d’en haut*, dicté de manière arbitraire et parfois suite à une intuition, alors que le *principe* vient *d’en bas* dans le sens ou c’est par l’accumulation d’observations ou d’expérimentations qu’on le façonne. Mais au final on se retrouve dans la même situation avec une directive qui nous indique un fonctionnement. La frontière étant assez fine, la répartition entre les 2 est assez aléatoire. J’ai pris le parti de suivre ce qui est le plus usité, même si j’ai constaté que le terme « Loi » était souvent préféré sans raison particulière.

* [Effet](https://fr.wiktionary.org/wiki/effet) : Ce qui est produit par quelque cause. Là on est plus sur la mise en évidence de levier de comportement. Parfois activable sur commande, parfois implicitement ou automatiquement.

Un autre mot qui découle de l’utilisation de ces termes est « Corollaire ».

* [Corollaire](https://fr.wiktionary.org/wiki/corollaire) : Du latin « corollarium » qui veux dire « petite couronne ». C’est donc une extension directe de la loi / principe / effet susnommé.

# Principes
Commencons par les principes,


* [Principe de Pareto](https://fr.wikipedia.org/wiki/Principe_de_Pareto) ou « la règle du 80/20 » : Principe assez simple qui dit que « 80 % des effets sont le produit de 20 % des causes ». Si on applique ça au monde du travail, vous obtiendrez 80% du résultat attendu avec 20% de l’effort. Son corollaire étant que les 20% restant vous prendront 80% de votre effort, donc clairement à remettre en question pour s’assure que le ration bénéfice / effort en vaut la chandelle. (« As-t-on vraiment besoin de cette fonctionnalité ? »)

<u>Outils utiles :</u> Des outils de priorisation classiques peuvent aider à se concentrer sur les 20% les plus utiles. Par exemple la [matrice d’Eisennhower](https://fr.wikipedia.org/wiki/Matrice_d%27Eisenhower). Des outils se concentrant sur la valeur métiers serait aussi intéressant, tel un [MoSCoW](https://fr.wikipedia.org/wiki/M%C3%A9thode_MoSCoW) ou le plus polémique [WSJF](https://wikiagile.fr/index.php?title=WSJF_(SAFe))

* [Principe de Peter](https://fr.wikipedia.org/wiki/Principe_de_Peter) : Vous avez surement déjà rencontré quelqu’un que vous pensez ne pas avoir les compétences suffisantes pour son poste. C’est l’illustration parfaite de ce principe qui dit que « dans une hiérarchie, tout employé a tendance à s’élever à son niveau d’incompétence », avec pour corollaire que « avec le temps, tout poste sera occupé par un employé incapable d’en assumer la responsabilité ». 
Pour être un peu plus concret, un bon dev/ouvrier/commercial/… ne fait pas forcément un bon manager/responsable de service/directeur d’agence/… Ce sont souvent des métiers différents et si l’envie de la personne concernée n’est pas là il ne faut pas l’y obliger. Malheureusement ce genre de promotion est souvent l’unique moyen d’améliorer sa situation (financière, influence, reconnaissance, …) au sein de l’entreprise. Des solutions existent. La plus facile est de *promouvoir* un *incompétent* a un poste d’apparence plus prestigieux mais en réalité moins impactant pour l’entreprise. Solution que je trouve *cache misère* et qui fait porter un poids mort à l’entreprise (un peu mieux expliqué [ici]( https://eventuallycoding.com/2013/11/27/principe-de-peter-et-corollaire-darchimede)). Heureusement des idées plus pertinentes sont déjà en place chez certains, comme des parcours spécialisés qui englobe l’ensemble d’une carrière, sans changer de domaine ([voir ce billet](https://eventuallycoding.com/2021/06/24/senior-avec-6-ans-dexperience-et-apres))
Si vous voulez approfondir ce principe, [le livre original](https://www.babelio.com/livres/Peter-Le-principe-de-Peter/1271105) est toujours édité.
	* [Couplage avec la loi normale](https://fr.wikipedia.org/wiki/Principe_de_Peter#La_d%C3%A9foliation_hi%C3%A9rarchique) : (La loi normale étant expliqué plus bas) Heureusement tous les postes ne sont pas occupés par des « incompétents » et statistiquement on retrouve la répartition normale suivante :
		* 10 % d’employés sont super-compétents
		* 20 % d’employés sont compétents
		* 40 % d’employés sont modérément compétents
		* 20 % d’employés sont incompétents
		* 10 % d’employés sont super-incompétents

	* [Extension Dilibert](https://fr.wikipedia.org/wiki/Principe_de_Dilbert) : Vous connaissez peut-être (le comic strip Dilibert)[http://www.dilbert.com/] qui illustre toute les dérives possibles de la mauvaise gestion d’entreprise via son personnage principal Dilibert. Le principe s’énonce ainsi : « Les gens les moins compétents sont systématiquement affectés aux postes où ils risquent de causer le moins de dégâts : ceux de managers. » 

<u>Outils utiles :</u> Un moyen très simple est de ne pas avoir de hiérarchie ^^. Cependant au-delà d’une certaine taille d’organisation ça devient indispensable. À ce moment-là on peut s’inspirer de l’holacratie avec l’[élection sans candidat](https://instantz.org/election-sans-candidat/). Un accompagnement et formation sont 2 leviers évidents pour contrer ce principe.

* [Principe du plafond de verre](https://fr.wikipedia.org/wiki/Plafond_de_verre) : Principe de plus en plus médiatisé et souvent lié au critère de parité, mais son application totalement générique. Il désigne le fait que les postes les plus *prestigieux* soient réservés à une caste bien particulière. Le critère de sélection pouvant être très divers, le plus visible étant le sexe, mais on peut aussi trouver la couleur de peau, la nationalité, le cursus scolaire/type de diplôme, la religion, etc. Il est impossible de faire un listing exhaustif, donc je vous laisse imaginer la suite. Bien sûr le *critère* de sélection peut être multiplet et en mixer plusieurs avec une pondération propre.
	* [Extension falaise de verre](https://fr.wikipedia.org/wiki/Falaise_de_verre) : Principe moins connus mais tout aussi pernicieux. C’est au moment où les rats quittent le navire que l’on constate ce principe. Ces fameux postes inaccessibles le deviennent miraculeusement lors de période de crise grave. Les postes très exposés médiatiquement tel que la politique en donne [tout un florilège]( https://fr.wikipedia.org/wiki/Falaise_de_verre#Exemples_politiques) (ex : Marissa Mayer@Brexit 2016). Mais les grandes entreprises ne sont pas épargnées non plus (ex : Marissa Mayer@Yahoo 20212) ou les musées (ex : Laurence des Cars@Louvres 2021). Ce principe a été établis suite à [l’étude]( https://onlinelibrary.wiley.com/doi/abs/10.1002/9781118785317.weom110287) de [Clara Kulich]( https://www.unige.ch/fapse/psychosociale/collaborateurs/kulich) qui trouvait étrange [la statistique du Times]( https://www.psychologie.uni-frankfurt.de/58476021/Generic_58476021.pdf) de sous performance des entreprises dirigées par des femmes. Merci à elle.

<u>Outils utiles :</u> Sujet délicat sans réponse magique. Dans le domaine de l’IT la parité est souvent quasiment impossible à avoir, c’est donc aux managers d’avoir un point d’attention voire une métrique spécifique sur le sujet, sans pour autant tomber dans la *discrimination positive* exagérée qui pourrait être mal perçu par les 2 parties.
