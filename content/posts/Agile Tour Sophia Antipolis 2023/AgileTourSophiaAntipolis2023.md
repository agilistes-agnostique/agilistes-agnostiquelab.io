Title: Agile Tour Sophia Antipolis 2023
Date: 2023-09-25
Tags: Agile Tour Sophia Antipolis, Conférences
Keyword: Agile, Sophia Antipolis
Slug: Agile-Tour-Sophia-Antipolis-2023
Author: Matthieu
Description: Petit retour en images de l’édition 2023 de Agile Tour Sophia Antipolis
Summary: Petit retour en images de l’édition 2023 de Agile Tour Sophia Antipolis
Status: published
Cover: /images/ATSA-2023.jpg
Cover_Source:https://www.telecom-valley.fr/wp-content/uploads/2023/07/ATSA-2023_header-4.jpg


Vendredi dernier, je suis allé à la [12ème édition](https://www.telecom-valley.fr/agile-tour-sophia-2023/) d’[Agile tour Sophia Antipolis](https://www.telecom-valley.fr/actions/conferences/agile-tour-sophia-antipolis/) pour y faire la présentation « Accelerate, FAST & UnFix sont dans un bateau », avec mon compatriote Yannis. Le thème de cette édition étant « Prenons de la hauteur », on aurait dû changer « bateau » par « montgolfière », ce qui a été fait un peu à la dernière minute sur la première diapo de notre présentation. 😅

Après un bout du chemin en train puis en voiture depuis Avignon, nous sommes finalement arrivés à l’heure pour partager le repas des orateurs avec quelques organisateurs et autres orateurs. Moment de partage traditionnel et indispensable des conférences Agile 😋. Petit bémol, le budget de l’orga semble des plusréduitst et chacun devait payer sa part … mais ce n’est pas ce qui nous a empêché de profiter de ce très bon moment de convivialité.

![Diner des orateurs]({attach}diner.jpg){.image .width-75}

La raison de ce budget réduit est la volonté de garder l’évènement gratuit ! Chose rare et appréciable, même si du coup il faut faire quelques sacrifices. Le lendemain, les orateurs ont tout de même une collation de prévu sous la forme d’un panier repas, les participants eux sont soit invités à amener leur repas, soit aller le chercher dans les environs ou enfin passer commande auprès de l’orga pour avoir le panier repas.


Le lendemain, le grand jour est arrivé. Après une belle montée d’escalier, nous arrivons à l’entrée des bâtiments. En tant qu’orateur, on nous distribue de jolies goodies faites maison dans le FabLab de l’université.

![Goodies orateurs]({attach}goodies.jpg){.image .width-25}

Après l’ouverture de la journée faite par les organisateurs, la keynote est assurée par Viviane Morelle, championne du monde de montgolfière 🤩. 

| | |
| - |-|
| ![Ouverture]({attach}ouverture.jpg){.image .fit} | ![Keynote début]({attach}keynote_début.jpg){.image .fit} |

Ensuite, nous pitchons notre session qui se passe le matin à 11h55 . Nous pouvons donc profiter du premier créneau en tant qu’auditeur. Je choisis la session de Felix Tapin & Pascal Tran, « La vision vue de haut ». Sans tout dévoiler de leur présentation, ils nous ont fait faire une « jolie prairie » et un « Pitch elevator ».

| | |
| - |-|
| ![Vision en haut]({attach}Vision en haut.jpg){.image .fit} | ![dessins]({attach}dessins.jpg){.image .fit} |

C’est non sans une pointe de fierté que notre groupe a eu l’unanimité du jury concernant notre pitch du « Sofa Beer » !!! 🍺 Forcément dès que l’on parle d’alcool, en général, c’est rassembleur.

À notre tour de faire notre présentation. Ça se passe globalement bien, les gens ont l’air intéressés, car il y a pas mal de questions. Il faudra juste revoir le timing car nous avons allègrement dépassé le temps qui nous était normalement imparti. Mais heureusement, c’était la pause de midi juste après. En parallèle, Erwan Bourgeois présentait « Créer sa communauté de pratiques Agile avec le Community Canvas » auquel j’aurais bien voulu assister, mais on ne peu pas tout faire.

| | | 
| - |-|
| ![Extérieur 1]({attach}extérieur1.jpg){.image .fit} | ![Extérieur 2]({attach}extérieur2.jpg){.image .fit} |
| ![Panneau et stand]({attach}panneau_et_stand.jpg){.image .fit} | ![Panneau]({attach}Panneau.jpg){.image .fit} |

Pour la première session de l’après-midi nous retrouvons Christelle Bergé & Jérémie Bohbot qui nous font un retour d’expérience d’atelier de conception UnFix et je dois avouer que ça faisait longtemps que je n’ai pas eu le sentiment d’apprentissage durant une session. Vraiment très intéressant et instructif.    
J’enchaîne avec un autre retour d’expérience cette fois sur SAFe. Intéressant lui aussi du fait du pragmatisme de l’orateur sur cette méthode décriée.

| | |
| - |-|
| ![Rex UnFix]({attach}Rex_Unfix.jpg){.image .fit} | ![Rex SAFe]({attach}Rex_SAFe.jpg){.image .fit} |


Malheureusement, je n’ai pas pu voir la keynote de fin. Je devais prendre un train à Antibes et je souhaitais éviter à mon chauffeur le détour et les embouteillages de la ville. Je saute donc dans une autre voiture avant la session. Mais c’est sans regret que je discute avec le plus élégant des coachs agile que je connais en la personne de Léonard Prunier. On ne voit normalement jamais de costume 3 pièces sur-mesure à ce genre d’évènement normalement. Il en profite pour me refiler son petit recueil de rétro « Pimp My Rétro ».
Je repars donc en train depuis Antibes et chose que je n’avais pas fait attention avant, le train va prendre son temps pour remonter jusqu’à Lyon … 4h30 exactement 😱 !!! Mais je ne serais pas déçu du voyage, car le train passe littéralement à  quelques mètres de la côte et nous offre de jolis paysages 😲

| | | 
| - |-|
| ![Train 2]({attach}Train2.jpg){.image .fit} | ![Train 3]({attach}Train3.jpg){.image .fit} |
| ![Train 4]({attach}Train4.jpg){.image .fit} | ![Train 3]({attach}Train5.jpg){.image .fit} |
