Title: Agile Tour Bordeaux 2023
Date: 2023-10-01
Tags: Agile Tour Bordeaux, Conférences
Keyword: Agile Tour,  Bordeaux
Slug: Agile-Tour-Bordeaux-2023
Author: Matthieu
Description: Retour en images de l’édition 2023 de Agile Tour Bordeaux
Summary: Retour en images de l’édition 2023 de Agile Tour Bordeaux
Status: published
Cover: /images/logoATBX.jpg

Lyon - Bordeaux, y’a pas à dire ça fait une trote … mais le déplacement en valait la peine. 🤩

Le 26 et 27 octobre dernier s’est tenu la [24ème édition d’Agile Tour Bordeaux](https://agiletourbordeaux.fr/) pour y (re)faire la présentation de « Accelerate, FAST & UnFix sont dans un bateau » que j’avais déjà assuré à [Sophia Antipolis](/articles/Agile-Tour-Sophia-Antipolis-2023.html) avec Yannis. Mais cette fois, nous serons 3 sur scène avec l’aide de Jean-Yves.
Le thème de cette édition était « qualité et résilience : complices de votre amélioration continue ».

Pour y aller je n’avais gère le choix, train ou voiture. Le premier me tentait vraiment, mais les horaires et fréquences n’étaient pas très engageants, la deuxième solution me promettait 6 heures de routes. Après quelques discussions, j’ai finalement fait co-voiturage avec JY, ce qui nous a permis de nous relayer tout discutant, ce qui fait passer bien plus vite le temps et les kilomètres. La pluie nous a quand même obligés à lever le pied par moment, ce temps, très instable, s’est amélioré tout doucement au fil des 2 jours, pour finalement avoir quleques belles éclaircis sur le chemin du retour.

Arrivés sur place vers 21h, nous nous dépêchons de garer la voiture pour aller au repas des speakers, on verra plus tard pour l’hôtel.
Pour vous mettre tout de suite dans l’ambiance, il y a une petite chose que j’ai particulièrement appréciée sur cet évènement est le regroupement de tous les lieux, ce qui permet de tout faire à pied.

| | | 
| - |-|
| ![Diner orateurs]({attach}ATBx2023-premier-repas.jpg){.image .fit} | ![Plan]({attach}plan.jpg){.image .fit} |

« Le coq » était le restaurant du premier soir qui est littéralement à 10 mètres de l’hôtel. 3iS qui accueille l’évènement est à moins de 500 m de l’hôtel, donc en 5 min (surtout sous la pluie) on pouvait faire la jonction. «Be Wine» nous a accueilli le soir du premier jour avant d’aller se restaurer au « Fellini ». Le fait de pouvoir poser la voiture dans le parking de l’hôtel et ne plus s’en préoccuper jusqu’à la fin était vraiment agréable.

Si vous voulez savoir ce que vous avez loupé, le programme est [par là](https://agiletourbordeaux.fr/programme.html).

![Ouverture]({attach}ouvertureATBX23.jpg){.image .width-75} 

Après la présentation d’ouverture très enrichissante, je me suis dirigé vers l’amphi 105 pour écouter de courtes présentations, environ 20 minutes.
La première était « N’oublie pas ta bulle » présenté par Alice Marteau, Geoffrey Graveaud, Nathalie Fontaine et Anissa Benmehidi. Retour très intéressant sur la mise en place d’une communauté de coachs interentreprises pour discuter de leur propre problématique.
<del>Étant au premier rang leur disposition ne m’a pas permis de les prendre en photo tous ensemble malheureusement.</del>
<ins>MAJ : Alice m’a fourni une jolie photo du groupe au complet.</ins>

| | | 
| - |-|
| ![Groupe bulle]({attach}groupe_bulle.jpg){.image .fit} | ![prez bulle]({attach}prez_bulle.jpg){.image .fit} |

| | | | |
| - |-| - | - |
| ![bulle 1]({attach}bulle1.jpg){.image .fit} | ![bulle 2]({attach}bulle2.jpg){.image .fit} | ![bulle 3]({attach}bulle3.jpg){.image .fit} | ![bulle 4]({attach}bulle4.jpg){.image .fit} |

La seconde présentation était « PO/Designer, duo-gagnant pour la conception de votre produit ! » de Noémie M. Rivière qui nous a expliquée la complémentarité des rôles de PO et Designer surtout lors de la phase d’initialisation du produit. J’y ai découvert le rôle de « Product Designer » qui vient en plus de l’UI et l’UX. Vraiment très intéressant.

![PO Designer]({attach}PO-designer.jpg){.image .width-75}


Ensuite vient notre tour !

| | | 
| - |-|
| ![Affiche]({attach}affiche.jpg){.image .fit} | ![Présentateurs]({attach}stars2.jpg){.image .fit} |
| ![En action]({attach}star prez2.jpg){.image .fit} | ![En action]({attach}star prez.jpg){.image .fit} |

Comme pour Sophia nous avons dépassé notre temps imparti… mais de manière plus raisonnable cette fois. Les questions qui fusaient tout au long de la présentation nous laissaient penser que le public était intéressé et curieux des sujets présentés.

![Public]({attach}public.jpg){.image .width-75}

Pause déjeuner.

Sur l’après-midi, notre ami Thomas Moreau nous a invités à co-animer son atelier sur UnFix. Yannis et JY l’ont fait, mais j’ai (égoïstement) préféré profiter des orateurs. J’ai donc opté pour « Je te promet le chaos » de Anne-Laure Gaillard et «  La matrice de compétences Agile l’outil absolu pour la résilience de vos projets et de vos équipes » de Mathieu Allouche.
La première nous a fait partager son expérience du «chaos testing» et le second nous explique ce qu’est et comment utiliser une matrice de compétence.

![Je te promet le chaos]({attach}promet-chaos.jpg){.image .width-75}

Et la première journée se termine enfin. Je n’ai pas décrit tous les moments d’échanges et de partages que l’on peut avoir durant ces événements, mais ils sont très nombreux et enrichissants. La suite de la soirée s’est d’abord déroulée au bar dont je ne publierai qu’une seule et unique photo 😅

![Be Wine]({attach}beWine.jpg){.image .width-75}

Après une nuit un peu courte, on est reparti pour la seconde journée avec pour démarrer « (Et si on apprenait à) Apprendre et partager autrement » d'Aurélie Vache. Je lui tire mon chapeau d’avoir réussi a assurer sa présentation aussi bien, avec les problèmes d’élocution qui les sont siens. Une belle leçon de confiance en soi qui est finalement l’un des thèmes de sa présentation.

![Aurélie Vache]({attach}aurelie-vache.jpg){.image .width-75}

Mention spéciale aux intros des orateurs de cette journée où l’on a vu défiler des M&M’s, une péruque, un jogging des années 80, un guitariste et autres joyeusetés …

| | | 
| - |-|
| ![Perruque jogging]({attach}pitch-perruque-jogging.jpg){.image .fit} | ![M&M’s]({attach}pitch-MMs.jpg){.image .fit} |

Pour ma part, j’ai opté pour «  Loi de Conway : Lorsque les bonnes pratiques ne suffisent pas » de Julien Topçu qui m’a vraiment convaincu. Il est si facile de se décharger du bazar professionnel ambiant en invoquant cette loi, qu’il est agréable de voir sa démystification, puis différents outils et une stratégie pour la combattre au mieux. Vraiment très instructif.

 ![BAPO]({attach}BAPO.jpg){.image .width-75}
 
Apparemment, en parrallèle, la conférence de Fabrice Bloch et Juliette Lorentz a aussi fait pas mal d’émules avec leur « Soyez vilains, sabotez votre transfo - Arkham Management School », où certains éléments perturbateurs voulaient appliquer en direct les méthodes de sabotage.

Dernière présentation de la matinée avec «  Waterfall,Agilité : de l'utilité de se confronter et de travailler ensemble » de Anthony Praud, Alain Delachaux et Christian Simion, M&M’s jaune, rouge et le petit nouveau bleu. C’est là où j’ai compris que Fabrice et Juliette voulaient rendre la pareille à ces trubillons. Disons que leur exposé a été légèrement découpé façon carpaccio, par des interventions inopinées. Ça n’enlevait rien a la qualité des propos tenus et animait plutôt joyeusement la séance … mais pour ceux qui n’étaient pas au fait de ces interruptions, ça ne devait pas être facile à suivre. Si je me mets à la place d’une personne extérieure à notre joyeux groupe, on avait l’impression d’un joyeux bordel qui saborde une présentation qui s’annonçait intéressante.

![Perturbateurs]({attach}perturbations.jpg){.image .width-75}

La matinée se termine et on profite de notre second repas pour discuter avec encore plus de personnes que la veille … avant de faire nos adieux. Eh oui, on a encore de la route devant nous et les dernières 48 h ont été pas mal chargées. Jean-Yves et moi avons donc fait l’impasse sur l’après-midi et son forum ouvert.

Un dernier mot pour la fin. Si vous êtes déçu de ne pas être venu et bien vous avez de la chance ! Le lieu d’acceuil de l’évènement est un institut de l’image et du son, ce qui veut dire que l’enregistrement des sessions était prévu ! Sauf pour les ateliers bien évidemment. Donc, soyez à l’affût des annonces de l’orga pour voir ou revoir les orateurs et leur diatribe.

![Régie]({attach}regie.jpg){.image .width-75}

Participez à l’organisation de cet évènement vous tente, mais vous ne savez pas en quoi cela consiste ? [Anne-Laure Gaillard](https://www.linkedin.com/in/annelauregaillard/) nous la présente pour l’édition 2022 avec ce [très sympathique article](https://www.linkedin.com/pulse/agile-tour-bordeaux-du-point-de-vue-orga-anne-laure-gaillard-/) .

Encore un grand merci à tous ces bénévoles qui font vivre ce genre d’évènement.

![Régie]({attach}orgasATBX23.jpg){.image .width-75}
