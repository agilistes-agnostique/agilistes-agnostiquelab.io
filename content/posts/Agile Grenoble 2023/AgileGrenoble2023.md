Title: Agile Grenoble 2023
Date: 2023-12-29
Tags: Agile  Grenoble, Conférences
Keyword: Agile ,  Grenoble
Slug: Agile-Grenoble-2023
Author: Matthieu
Description: Retour en images de l’édition 2023 d’Agile Grenoble. Avis tout a fait subjectif qui vous fait vivre de l’intérieur cet évènement.
Summary: Retour en images de l’édition 2023 d’Agile Grenoble. Avis tout a fait subjectif qui vous fait vivre de l’intérieur cet évènement.
Status: published
Cover: /images/logoAG2023.jpg


Je continue dans les retours d’expérience des conférences Agile en France. Cette fois-ci, on s’attaque à un TRÈS gros morceau, puisqu’il s’agit d’Agile Grenoble. Considéré par certains comme la plus importante et pointue des conférences Agile de France. En terme de fréquentation, j’ai appris que Grenoble n’était plus sur la première marche, mais la quantité ne fait pas la qualité. Alors plogeons tout de suite dans le vif du sujet avec mon expérience de cette année. « Choisir, c’est renoncer », et avec 87 sessions, le choix est difficile. En assistant à toutes les séances, au mieux, il est possible d’en voir 7 par jour (keynote inclus), soit à peine 16% de ce qui est proposé 😱. 

# Jour 1
## Matin

| | |
| - |-|
| ![Accueil]({attach}20231122_081839.jpg){.image .fit} | ![Présentation]({attach}20231123_083908.jpg){.image .fit} |

On commence par la Keynote d’ouverture assuré par le pionnier Claude Aubry. Pour ceux qui ne le connaitrait pas c’est l’un des précurseurs de l’agilité en France, l’auteur du livre « [Scrum](https://www.chez-mon-libraire.fr/livre/9782100828296-scrum-un-outil-convivial-pour-une-agilite-radicale-6e-edition-claude-aubry/) » qui en est à sa 6ème édition, mais aussi «[L’art de devenir une équipe Agile](https://www.chez-mon-libraire.fr/livre/9782100790289-l-art-de-devenir-une-equipe-agile-claude-aubry-etienne-appert/)» plus didactique et illustré, bloggeur « [Scrum, Agilité & rock'n roll](https://claudeaubry.fr/) » depuis 2006. Son intitulé était « L’agilité de la décroissance ». J’ai vraiment été intrigué par un tel titre et malheureusement, je n’ai pas eu une bonne surprise. Il nous a fait un très joli plaidoyé sur la nécessité de la décroissance, avec une promo assez prononcée (consciente ou non) du livre « Ralentir ou périr », tout en essayant de faire des parallèles avec le manifeste Agile ou des pratiques Agile un poil capilotracté à mon avis. Ça m’a furieusement rappelé une discussion sur un sondage pour inclure des notions morales/écologiques au guide Scrum.
En mettant de côté le fond du sujet, que l’on soit d'accord ou non, je n’ai pas été le seul à me demander si un discours sur ce thème était pertinent dans ce genre de conférence.

![Claude Aubry]({attach}20231122_085307.jpg){.image .width-75}

Ensuite, j’ai été rapidement rattrapé par un côté frustrant de Grenoble … les conférences se remplissent vite dans les salles et on est parfois face par une porte close. Ce fut le cas pour la conférence de [Kervin Kueny](https://www.linkedin.com/in/kervin-kueny-b57929114) et « Les questions pour préparer demain », ce qui m’obligea à revenir dans l’amphitéatre pour aller voir [Simon  Jaillais](https://www.linkedin.com/in/simon-jaillais-44888353/) et « Le besoin de mon client au cœur de tout ». Et franchement, je ne regrette absolument pas ce choix. Il nous a fait un tour sur les différences de pratique entre le Lean Startup, le Design Thinking et l’Agilité. Qui, finalement, ont toutes le même objectif, satisfaire l’utilisateur. Il nous a rappelé l’importance de ne pas parler uniquement des problèmes avec les utilisateurs, mais AUSSI de réfléchir aux solutions avec eux. Un petit tour sur la pyramide de la valeur et la matrice des compromis étendue. Je suis repartis avec deux recommandations, le livre « Jobs to be done » et la vidéo sur les feedbacks  « [le pull de la grand mère](https://www.youtube.com/watch?app=desktop&v=GhNwcQzvvz4) ». Conférence très intéressante pour moi, qui veux creuser le côté produit de l’Agilité.

| | |
| - |-|
| ![TradOff sliders]({attach}20231122_102455.jpg){.image .fit} | ![Fall in love with your customer]({attach}20231122_103817.jpg){.image .fit} |

Ensuite, j’ai continué dans ma lancée des conférences produit et je suis allé voir  [Vincent Feugier](https://www.linkedin.com/in/vincent-feugier-634a98123), [Frédéric Drogo](https://www.linkedin.com/in/fredericdrogo) et Fabien Joseph qui nous présentaient « PO : Évitez la feuille blanche ! ». Je trouvai leur début de présentation un peu longue, entre le CV de chacun et de toute leur méthodologie pour la construction de leur résultat. Le vif du sujet se faisait attendre. Le résultat et support de présentation était un [magnifique logigramme]({attach}PO, évitez la feuille blanche - OpenGroupe.pdf) à suivre pour un PO débutant. Il passait en revue tous les points clés et les réponses/outils adaptés. Logigramme tellement étendu qu’ils ont affiché un QR code pour que chacun puissent le visualiser sur son téléphone.

| | | 
| - |-|
| ![Fiche appréciation]({attach}20231122_105123.jpg){.image .fit} | ![Logigramme]({attach}20231122_111227.jpg){.image .fit} |

La pause méridienne permet de discuter plus calmement avec les nombreuses connaissances que l’on peut croiser à cet évènement. Ce qui est bien, c’est que certains se déplacent de loin pour y venir, ce qui, avec les Agile Game France, en fait un de mes évènements favoris.

## Après midi

La keynote de l’après-midi a été assurée par [Catherine Pamphile](https://www.linkedin.com/in/balanceisinmotion) sur « Un attribut prédictif de succès des transformations : la sécurité psychologique ». Point de vue intéressant sur l’exploitation de cet indicateur comme divination de la réussite d’une transformation.

![Catherine Pamphile]({attach}20231122_142423.jpg){.image .width-75}

Ne voulant pas me faire avoir une seconde fois sur le nombre de places limité des salles, je me suis dirigé assez rapidement vers la conférence de [Mario Espositio](https://www.linkedin.com/in/mariusuxdesigner) et [Rémi Seraillon](https://www.linkedin.com/in/remi-sarraillon) sur « L'extinction des dinosaures : Entre comportement toxique et manque de sens, que faire ? ». Bon je me suis fait accroché dans le hall, ce qui m’a value de m’assoire par terre pour y assister. Au-delà de la bonne tranche de rigolade, les conseils et les recommandations distillés dans leur présentation étaient tout à fait pertinants et très bien illustrés.

| | |
| - |-|
| ![Dino]({attach}20231122_143503.jpg){.image .fit} | ![Dino2]({attach}20231122_144235.jpg){.image .fit} |

Avec une banane jusqu’aux oreilles, je suis allé voir un sujet un peu plus sérieux avec [Maxime Bonnet](https://www.linkedin.com/in/maximebonnet) et « L’agilité à l’échelle simplement avec Scrum@Scale ». Sujet que je connais un peu pour avoir fait des présentations express de ce cadre, mais je voulais aller un peu plus loin sans forcément lire toute la documentation. J’ai eu droit en bonus à un REX d’implémentation au sein de Groupama, client que je connais bien. Le tout sur fond d’images appétissantes, parce que voilà, c’est le choix de l’orateur 😋

| | | 
| - |-|
| ![Pourquoi]({attach}20231122_155854.jpg){.image .fit} | ![Guide]({attach}20231122_160520.jpg){.image .fit} |

Ensuite, j’ai joué la carte de l’originalité avec [Eric Bortzmeyer](https://www.linkedin.com/in/innovation-bortzmeyer) et [Halldor Kvale-Skattebo](https://www.linkedin.com/in/skattebo) qui avait comme sujet « Approche de l'agilité en Norvège ». Ils nous ont bien posé le cadre avec les particularités de ce pays (6 millions d’habitants), leurs environnements de travail (ont collaboré avec Dave Snoden & Cynefin), mais finalement, je suis resté sur ma faim. Je m’attendais à plus de points de comparaison entre les 2 cultures et pratiques de chaque pays. Au final, c’est au moment des questions que cela a été le plus intéressant, car appuyait justement sur ces différences. Je salue tout de même leur présence et leur présentation ; on n'a pas la chance tous les jours d’avoir des orateurs étrangers venir d’aussi loin.

![Eric & Hallor]({attach}20231122_170216.jpg){.image .width-75}

## Soir

Voilà, fin de la première journée … des conférences. Mais la soirée ne fait que commencer ! Cette année, il a été prévu une soirée jeux ! Nous profitions du traiteur pour nous sustenter le soir, donc pas de perte de temps entre la bière servie, comme d’habitude, par Zenika et ce moment de détente.

![Jeux]({attach}20231122_221952.jpg){.image .width-75}

---

# Jour 2
## Matin

Cette seconde journée s’ouvre avec une plenière tout à fait dans l’ère du temps, puisque l’on y parle d'IA. [Ari Kouts](https://www.linkedin.com/in/arikouts) nous fait une belle démonstration des capacités actuelles et nous a fait réfléchir sur les enjeux sociétaux de cette technologie. Le ton était très optimiste sur l’envol de la techno, mais vient en opposition avec le discours de Claude Aubry de la première journée. Contraste fort, mais très intéressant.

![Keynote IA]({attach}20231123_090151.jpg){.image .width-75}

Je fais un rapide tour dans l’espace commun pour revenir au premier rang de l’amphithéâtre. Je ne voudrais surtout pas louper la conférence qui va s’y tenir. Il s’agit de la dernière de l’estimé [Alexandre Boutin](https://www.linkedin.com/in/boutin) ! Je pourrais dire « j’y étais ! ». Il nous a fait un pertinent rappel sur l’historique de l’Agilité pour mieux se poser la question de son implémentation actuelle et de son avenir. Il a aussi remis au centre l’intérêt & les avantages de l’Agilité.
Tout le premier rang était complice et lui avait préparé des surprises. L’une d’entre elles était des moustaches factices pour tous. Mais lui aussi nous avait réservé une surprise en offrant à toutes les personnes présentes une tablette de chocolat ! (il y en a une photo en bas d’article)

| | | 
| - |-|
| ![Alex]({attach}20231123_100312.jpg){.image .fit} | ![Selfi]({attach}20231123_103937.jpg){.image .fit} |

![Merci]({attach}20231123_104132.jpg){.image .width-75}

Pour clore cette matinée, je suis (encore) allé voir un ami, en la personne de [Elyzer Ducros](https://www.linkedin.com/in/ducroselyezer-agilecoach) avec son « Le produit dont vous êtes fier ». Il aborde les sujets environnementaux et sociétaux de notre métier, mais aussi les biais qui nous freinent ou nous détournent de notre objectif. Tout ça dans le but d’expliquer son acronyme F.I.E.R. qui l’aide à se poser les bonnes questions sur la pertinance de son rôle. Donc FIER, signifie :

- <u>**F**</u>ondamentale
- <u>**I**</u>nclusif
- <u>**E**</u>fficient
- <u>**R**</u>esponsable

Je vous laisse lui demander en détail ce qui se cache derrière ces notions.

![FIER]({attach}20231123_110748.jpg){.image .width-75}


## Après midi

La journée reprend avec une très touchante keynote de [Benjamin Cabanne](https://www.linkedin.com/in/bcabanne), qui, avec son accent toulousain, s’est ouvert a nous et nous a partager son expérience. C’était très personnel, humain, empatique.

![Benjamin]({attach}20231123_132952.jpg){.image .width-75}

Ensuite, et c’est surement mon oratrice coup de cœur de cette conférence, [Caroline Le Lay](https://www.linkedin.com/in/caroline-le-lay-leme) nous a parlé de « Et si demain, contractualiser l'agilité dans une relation gagnant/gagnant, devenait possible ! ». Malgré son stress et ses inquiétudes, elle s’en est très bien tirée. Elle nous a parlé de son expérience sur la contractualisation Agile de projet, ce qui fait que ça fonctionne maintenant, mais aussi les pièges qu’elle a expérimentés et qu’il faut éviter. Son parallèle avec l’achat d’un bouquet de fleurs personnalisé chez un fleuriste est assez bien trouvé. Les 3 clés du succès qu'elle nous a confiées sont :

- Confiance entre les parties (client / fournisseur)
- Maitrise du contexte
- Maitrise du budget

| | | 
| - |-|
| ![Caro]({attach}20231123_143616.jpg){.image .fit} | ![Caro2]({attach}20231123_143759.jpg){.image .fit} |

J’attends de voir ce qu’elle va nous présenter durant la prochaine édition du PIAF 😅

J’ai enchainé avec une présentation plus technique (ce qui se fait malheureusement rare) de [Fontaine Emeric](https://www.linkedin.com/in/fontaine-emeric-6b38095) & [Imen Bouhamed](https://www.linkedin.com/in/imen-bouhamed-7b820126) et leur « Trunck based, TDD, devops avec des ops, ... Découvrez un Mob de fonctionnement hors norme ».

![XP]({attach}20231123_160022.jpg){.image .width-75}

Et pour finir les conférences, je me suis laissé tenter par la présentation de [Pierrick Boyer](https://www.linkedin.com/in/pierrick-boyer) avec son « De l'innovation pour rebooster ses équipes », où il réaffirme l’importance de savoir lever la tête du guidon et de laisser du temps et des ressources pour l’innovation. Il nous explique comment la mise en place en sous main de cette pratique a redynamisé son équipe sans AUCUNE perte de productivité et même tout l’inverse.

| | |
| - |-|
| ![Pierrick]({attach}20231123_170328.jpg){.image .fit} | ![Pierrick2]({attach}20231123_170344.jpg){.image .fit} |

---

# Jour 3

Et oui, beaucoup ne sont présents que les 2 premiers jours, mais la troisième journée est pour moi l’une des plus intéressantes. C’est la journée du forum ouvert, animé par [Sarah](https://www.linkedin.com/in/sarah-dantz/) & [Léa](https://www.linkedin.com/in/leachevalier1) de la [SCOP Sémawé](https://www.linkedin.com/company/semawe/). Pas de programme défini à l’avance, juste des sujets apportés par les participants eux-mêmes. Et en bonus (encore) des tablettes de chocolats d’Alex, je le soupçonne d’en avoir prévu un peu trop pour sa conf 😋

| | | 
| - |-|
| ![Prez]({attach}20231124_092745.jpg){.image .fit} | ![File]({attach}20231124_093011.jpg){.image .fit} |
| ![Tableau]({attach}20231124_150342.jpg){.image .fit} | ![Choix]({attach}20231124_100500.jpg){.image .fit} |
| ![Energizeur]({attach}20231124_135525.jpg){.image .fit} | ![théatre]({attach}20231124_112341.jpg){.image .fit} |

La bonne surprise de cette journée était la participation d’Alex en tant qu’animateur d’un atelier ! Ne devait-il pas prendre sa retraite en tant que participant ? Hé bien, oui, mais seulement des keynotes et des présentations. Pour les ateliers et les jeux, il se laisse une porte ouverte, a notre grande satisfaction 🤩

![Alex]({attach}20231124_140917.jpg){.image .width-75}

# Conclusion

Vous me direz : « Mais tu n’as rien présenté toi ? ». Hé bien non. Ce n’est pas faute d’avoir soumis des propositions. Mais il faut croire que j’ai encore un peu de mal à les rendre attrayantes pour me faire sélectionner. Mais ce n’est que partie remise. Finalement, la position de spectateur est aussi très confortable. Pas de stress de préparation, pas obligé de faire une croix sur un ou plusieurs créneaux. On profite finalement plus de l’évènement ainsi.

Je suis très satisfait de cette édition. J’ai toujours une crainte de ne pas trouver de quoi m’alimenter en nouvelles idées ou en matière, mais avec un tel foisonnement de sujets et de personnes que cette crainte n’est pas fondée. En plus d’y apprendre plein de choses, je retrouve aussi plein d’amis et de connaissances avec qui les échanges sont souvent très instructifs et agréables. Je ne peux que vous inciter à venir dans cette grande messe de l’Agilité si vous vous intéressez au sujet de près ou de loin.

Encore un grand merci à tous les organisateurs, les orateurs et les participants. C’est grâce à tout le monde que Agile Grenoble est ce qu’il est aujourd’hui et que l’on en redemande chaque année.
