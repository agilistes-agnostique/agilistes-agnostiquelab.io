Title: Exemple pour la rédaction d’un article
Date: 2099-01-01
Tags: Markdown
Status: draft

## Entête (Métadata)

```
Title: Titre qui sera affiché. Note : Pas la peine de le remettre en début d’article
Date: 2023-01-01
Modified: 2023-01-02
Tags: liste a séparer par des virgules
Keyword: liste de mots à séperer par une virgule
Category: liste a séparer par des virgules
Slug: nom de la page qui sera appelé. Permet d’avoir un identifiant unique d’article
Author: Votre nom
Authors: Quand vous êtes plus d’un a avoir écris
Description: Texte qui sert pour les snippets des sites externe (LinkedIn, Slack, …). Faire court !!! <110 caractères
Summary: Texte qui sera repris en tant que résumé, pour l’index et le flux RSS
Lang: Optionnel
Translation: Non encore mis en place
Status: #Optionnel draft/hidden/published 
Cover: Image qui sera mis en couverture sur la page d’accueil. Non obligatoire. 
    Ex : https://source.unsplash.com/XWC5q9_Xp0o/600x400
    Ex2 : https://images.unsplash.com/photo-1578852612716-854e527abf2e?fm=jpg&fit=crop&crop=middle&h=400&w=600&q=80
Cover_Source: URL de l’image de couverture
```
Pour l'API unsplash voir ici : https://unsplash.com/documentation#dynamically-resizable-images

Voir la doc de référence [ici](https://docs.getpelican.com/en/latest/content.html?highlight=content)
Pour le template : https://html5up.net/editorial

## Icône & smiley
Pour avoir des icones dans les articles :
https://www.lecoindunet.com/liste-tous-emoticones-emoji-copier-coller-580
https://emojipedia.org/fr
http://unicode.org/emoji/charts-12.0/full-emoji-list.html
https://apps.timwhitlock.info/emoji/tables/unicode


### Sourires et signes d'affection
😀😃😄😁😆😅🤣😂🙂😉😊😇🥰😍🤩😘😗☺️😚😙🥲😏
### Langues, mains et accessoires
😋😛😜🤪😝🤗🤭🫢🫣🤫🤔🫡🤤🤠🥳🥸😎🤓🧐
### Neutre et sceptique
🙃🫠🤐🤨😐😑😶🫥😶‍🌫️😒🙄😬😮‍💨🤥🫨🙂‍↔️🙂‍↕️
### Endormi et malade
😌😔😪😴😷🤒🤕🤢🤮🤧🥵🥶🥴😵😵‍💫🤯🥱
### Inquiet et négatif
😕🫤😟🙁☹️😮😯😲😳🥺🥹😦😧😨😰😥😢😭😱😖😣😞😓😩😫😤😡😠🤬👿
### Costume, créature et animal
😈👿💀☠️💩🤡👹👺👻👽👾🤖😺😸😹😻😼😽🙀😿😾🙈🙉🙊
### Cœurs et émotions
💌💘💝💖💗💓💞💕💟❣️💔❤️‍🔥❤️‍🩹❤️🩷🧡💛💚💙🩵💜🤎🖤🩶🤍💋💯💢💥💦💨🕳️💬👁️‍🗨️🗨️🗯️💭💤
### Fruits
🍇🍈🍉🍊🍋🍋‍🟩🍌🍍🥭🍎🍏🍐🍑🍒🍓🫐🥝🍅🫒🥥
### Légumes
🥑🍆🥔🥕🌽🌶️🫑🥒🥬🥦🧄🧅🥜🫘🌰🫚🫛🍄‍🟫
### Plats préparés
🍞🥐🥖🫓🥨🥯🥞🧇🧀🍖🍗🥩🥓🍔🍟🍕🌭🥪🌮🌯🫔🥙🧆🥚🍳🥘🍲🫕🥣🥗🍿🧈🧂🥫🍝
### Plats asiatiques
🍱🍘🍙🍚🍛🍜🍠🍢🍣🍤🍥🥮🍡🥟🥠🥡
### Sucreries et desserts
🍦🍧🍨🍩🍪🎂🍰🧁🥧🍫🍬🍭🍮🍯
### Boissons et vaisselle/couverts
🍼🥛☕🫖🍵🍶🍾🍷🍸🍹🍺🍻🥂🥃🫗🥤🧋🧃🧉🥢🍽️🍴🥄🔪🫙🏺
### Évènements et célébrations
🎃🎄🎆🎇🧨✨🎈🎉🎊🎋🎍🎎🎏🎐🎑🧧🎁🎟️🎫🏮🪔
### Sports et récompenses
🎖️🏆🏅🥇🥈🥉⚽⚾🥎🏀🏐🏈🏉🎾🥏🎳🏏🏑🏒🥍🏓🏸🥊🥋🥅⛳⛸️🎣🤿🎽🎿🛷🥌🎯
### Jeux et culture
🪀🪁🔫🎱🔮🪄🎮🕹️🎰🎲🧩🪅🪩🪆♠️♥️♦️♣️♟️🃏🀄🎴🎭🖼️🎨
### Cartes et géographie
🌍🌎🌏🌐🗺️🗾🧭🏔️⛰️🌋🗻🏕️🏖️🏜️🏝️🏞️

# Titre & sous-titre

```
# h1 Titre
## h2 Titre
### h3 Titre
#### h4 Titre
##### h5 Titre
###### h6 Titre
```

## Barre horizontal

Enchainer 3 fois un tiret bas, tiret ou étoile

```
___

---

***
```

---

---

---

Terminer une ligne avec 2 espace ou plus permet de forcer un retour à la ligne lors de la génération

## Emphase

```
**Text en gras**

__Text en gras__

*Text en italic*

_Text en italic_

~~Text barré~~
```

**Text en gras**

__Text en gras__

*Text en italic*

*Text en italic*

~~Text barré~~

## Citations

```
> Blockquotes can also be nested...
>> ...by using additional greater-than signs right next to each other...
> > > ...or with spaces between arrows.
```

> Blockquotes can also be nested...
>
> > ...by using additional greater-than signs right next to each other...
> >
> > > ...or with spaces between arrows.

## Listes

### Désordonnée

```
+Create a list by starting a line with `+`, `-`, or `*`
+ Sub-lists are made by indenting 2 spaces:
  - Marker character change forces new list start:
    * Ac tristique libero volutpat at
    + Facilisis in pretium nisl aliquet
    - Nulla volutpat aliquam velit
+ Very easy!
```

* Create a list by starting a line with `+`, `-`, or `*`
* Sub-lists are made by indenting 2 spaces: 
  * Marker character change forces new list start: 
    * Ac tristique libero volutpat at


    * Facilisis in pretium nisl aliquet


    * Nulla volutpat aliquam velit
* Very easy!

### Ordonnée

```
1. Lorem ipsum dolor sit amet
2. Consectetur adipiscing elit
3. Integer molestie lorem at massa
```

1. Lorem ipsum dolor sit amet
2. Consectetur adipiscing elit
3. Integer molestie lorem at massa

## Code

En ligne avec des simple cotes \`code\`. Exemple de `code` en ligne

Avec une identation de 4 espaces ou 1 tabulation ça fonction pour les blocs de code (ou texte brut)

```
// Some comments
Test avec 4 espaces
  2 espaces supplémentaires
Test tabulation
	double tabulation
```

Bloc de text brut sans formatage particulier peut se faire avec \``` en début et fin de bloc

```
Exemple de textSample text here...
```

Coloration syntaxique, il suffit de mettre le nom du language juste après les \``` initiaux.

```javascript
var foo = function (bar) {
  return bar++;
};

console.log(foo(5));
```

## Tableau

```
| Aligné à gauche  | Centré          | Aligné à droite | Pas aligné |
| :--------------- |:---------------:| ---------------:|------------|
| Aligné à gauche  | ce texte        | Aligné à droite | Pas aligné |
| Aligné à gauche  | est             | Aligné à droite | Pas aligné |
| Aligné à gauche  | centré          | Aligné à droite | Pas aligné |
```

| Aligné à gauche | Centré | Aligné à droite | Pas aligné |
|-----------------|--------|-----------------|------------|
| Aligné à gauche | ce texte | Aligné à droite | Pas aligné |
| Aligné à gauche | est | Aligné à droite | Pas aligné |
| Aligné à gauche | centré | Aligné à droite | Pas aligné |

Comme on peu le voir, par défaut c’est aligné a gauche et les colones sont équitablement répartis sur la largeur. Elles s’adaptent tout de même au contenu.

## Links

[Text du lien](https://test.com)  
[Lien avec une tooltip](https://test.com "Un petit text")

```
[Text du lien](https://test.com)  
[Lien avec une tooltip](https://test.com "Un petit text")
```

## Images

On peux lier des images externe

![Minion](https://octodex.github.com/images/minion.png "MinionGitlab"){#22 .image .left}

```
![Minion](https://octodex.github.com/images/minion.png "MinionGitlab") {#22 .image .left}
```

Le texte entre crochet (`[Minion]`) sera mis comme texte de remplacement(`alt`) dans la balise `img`. Le texte entre guillemet (`"MinionGitLab"`) sera en tooltip de l’image

---

Mais aussi lier des images interne.

![Alt text](/images/avatar.png){#5 .image .right}

```
![Alt text](/images/avatar.png){#5 .image .right}
```

Le `{#5 .image .right}` permet d’ajouter une valeur à l’attribut `id` de l’image (pratique pour la navigation au clavier) et des class CSS quand préfixé par un «.». Donc a vous de voir avec votre CSS.

---

Enfin on peux mettre une référence renseignée plus tard. Un peu comme une note de pied de page.

```
![Alt text][id]
[id]: https://octodex.github.com/images/dojocat.jpg  "The Dojocat"
```

\![Alt text][id]{#id .image .fit}
[id]: https://octodex.github.com/images/dojocat.jpg  "The Dojocat"

### [Note de pied de page](https://pandoc.org/MANUAL.html#footnotes)

```
Footnote 1 link[^first].  
Footnote .2 link[^toto].  
Footnote 3 link[^trois].  
Multiple référence de la note[^toto].  
Référence en ligne directement ^[Plus facile a écrire et ne pas perdre le fil de son idée.]

[^first]: Les notes profites _du_ **formatage**  
    et peuvent s’étaler sur plusieurs lignes

[^toto]: Footnote text.
[^trois]: Footnote text.
```

Footnote 1 link[^first].  
Footnote .2 link[^toto].  
Footnote 3 link[^trois].  
Multiple référence de la note[^toto].  
Référence en ligne directement ^[Plus facile a écrire et ne pas perdre le fil de son idée.]

[^first]: Les notes profites *du* **formatage**  
et peuvent s’étaler sur plusieurs lignes

[^toto]: Footnote text.
[^trois]: Footnote text.


