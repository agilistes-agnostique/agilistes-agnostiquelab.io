Title: Complexe vs Compliqué
Date: 2023/01/30
Update: 2023/02/17
Category: "Parlons peu, mais parlons bien"
Slug: Complexe-vs-Compliqué
Cover: /images/Complexe-vs-Compliqué.jpg
Cover_Source: https://unsplash.com/fr/photos/decor-rond-marron-et-noir-MiSPnHknw4w
Tags: vocabulaire, cynefin
Status: published 
Description: Point de vocabulaire. Les termes « complexe » et « compliqué » sont considérés comme des synonymes alors qu’il y a des différences fondamentales entre les 2.
Summary: Point de vocabulaire. Les termes « complexe » et « compliqué » sont considérés comme des synonymes alors qu’il y a des différences fondamentales entre les 2.

Pour ce premier vrai billet de [la série](https://anargile-leanbertaire.info/articles/Parlons-peu-mais-parlons-bien.html), penchons-nous sur les termes "complexe" et "compliqué". Dans le monde de la gestion de projet, on utilise plus souvent le terme « complexe », alors que dans la vie courante, c’est « compliqué » qui est privilégié. Mais finalement, peu de personnes arrivent à vous donner une différence claire entre les deux, au mieux vous donner un exemple moyennement convainquant, au pire vous dire que ce sont des synonymes. Si l'on se base sur les statistiques d'un moteur de recherche bien connu, c'est le terme « [complexe](https://trends.google.fr/trends/explore?geo=FR&q=complexe,compliqu%C3%A9) » qui est le plus recherché.

Commençons par leur étymologie.

## Étymologie
* Complexe : Du latin complexus (« embrassement », « étreinte »)
* Compliqué : Du latin complicare (« plier », « rouler »)

Donc, même si les mots partagent les mêmes 5 premières lettres, leur origine n’est clairement pas commune.
Voyons leurs définitions maintenant.

## Définitions

Autant prendre la référence de la langue qu’est le [dictionnaire de l’académie Française](https://www.dictionnaire-academie.fr).

### Complexe

> Qui n’est pas simple, qui est composé d’éléments divers et entremêlés ; compliqué, difficile à comprendre.
[Source](https://www.dictionnaire-academie.fr/article/A9C3257)

Ça commence bien, la définition de « complexe » inclut « compliqué », comme quoi les mots sont proches l’un de l’autre. De plus, il est amusant de voir que sa définition commence par son antonyme « simple ». On voit qu’il y a une notion de nombre d’éléments élevés et de leurs interactions, mais on ne qualifie pas ces interactions.

### Compliqué

> Composé de parties plus ou moins nombreuses qui ont entre elles des rapports difficiles à analyser, à saisir ; dont la réalisation ou l’usage est délicat ou difficile.
[Source](https://www.dictionnaire-academie.fr/article/A9C3268)
Bien déjà, ça ne boucle pas, on ne retrouve pas le terme "complexe" dans la définition. On retrouve une notion de nombre d’éléments mais qui peut être faible. On retrouve aussi les interactions, mais là c’est bien qualifié, elles sont « difficiles à analyser ».

## Éclairage

Complexe désigne donc un système avec de multiples éléments qui interagissent entre eux. Qu’importe si ces interactions soient simples ou compliquées, c’est leur nombre qui en fait un système complexe. Je ne me suis pas attardé sur la fin de la définition de ce mot, mais elle est presque la partie la plus intéressante « difficile à comprendre », ça veut dire que les tenants et aboutissants du système demandent un effort cognitif significatif pour l’appréhender de manière globale, si cela est possible. La difficulté est souvent dans l’appréhension complète du système, on se retrouve avec des « inconnus inconnus ». Et c’est bien là la différence avec un système compliqué, une interaction non prise en compte ou inconnue et le résultat en bout de chaîne devient imprévisible.

Compliqué désigne un système avec peu ou beaucoup d’éléments qui ont des interactions non triviales. Mais l’ensemble des relations et leurs difficultés est listé, répertorié, nous avons donc des « inconnus connus ». La fin de la définition est aussi très intéressante, une nouvelle notion y fait son apparition, celle de la réalisation ou usage, on est donc sur quelque chose de plus concret, palpable.

### Exemples

Quoi de mieux que des exemples pour bien comprendre.
> Un bon croquis vaut mieux qu’un long discours. (Napoléon Bonaparte)
[source](https://www.dicocitations.com/citations/citation-9557.php){.image}

C’est pour « complexe » que j’ai eu le plus de mal à trouver un bon exemple.

![Nuée d’oiseaux]({attach}nuée_oiseaux.avif){.image}

Ces nuées d’oiseaux, qui ont quelque chose d'hypnotique, sont des systèmes purement complexes. Ils ne sont guidés que par quelques règles assez simples que l’on peut décrire ainsi :

* Séparation : Éviter l’entassement des voisins (répulsion à courte portée)
* Alignement : Se diriger vers le cap moyen des voisins
* Cohésion : Se diriger vers la position moyenne des voisins (attraction à longue distance)

Et oui, avec ces 3 règles simples, des milliers d’individus peuvent se coordonner et voler de concert. Mais les oiseaux ne sont pas les seuls à se déplacer en groupes comme ça, les troupeaux d'animaux terrestres ou les bancs de poissons sont régis par les mêmes règles !
De plus, leurs mouvements et changements de direction ne sont pas aléatoires, mais dépendent directement de nombreux facteurs pas forcément connus. Bien sûr, nous avons quelques-uns évidents, comme les vents, reliefs, prédateurs, mais un certain nombre ne sont pas connus et découverts grâce à l'expérience et l'observation. Nous avons donc bien à faire à un système complexe, mais pas compliqué.
Pour la petite histoire, il a été observé qu'un mouvement de seulement 5% des individus était suffisant pour décider de la direction du groupe entier !

Pour ce qui est d'un système compliqué mais pas complexe, on peut y ranger les formules mathématiques telles que ceci :
> x²+2(y-0.8*abs(x)0.4)² = 1

Qui donne des choses intéressantes.

![Graphique en cœur]({attach}coeur_equation.webp){.image}
[Source](https://trustmyscience.com/equation-amour-saint-valentin-calculatrice-graphique/)

Nous avons donc bien un système compliqué de par sa résolution, mais non complexe car tous les inconnus sont connus et que sa résolution n'est pas évidente. De plus, son faible nombre d'éléments est un bon indicateur de sa simplicité. Attention, « simple » ne veut pas dire « facile ».

## FAQ

> Un système est-il exclusivement soit l’un soit l’autre ?

Absolument pas ! Il est même bien plus courant que ce soit les deux à la fois. Le critère à regarder est le nombre d’interactions et leur exhaustivité

| Interactions / Difficulté | Facile | Difficile|
|--------------------------|--------|----------|
| Peu | Évident | Compliqué |
| Beaucoup | Complexe | Complexe & Compliqué |
| Indéfinit | Complexe | Chaos ? |

> Un système complexe peut-il être compliqué et inversement ?

Oui ! Les deux sont souvent liés et c’est bien pour cela qu’ils sont répertoriés en tant que synonymes. Mais ce n’est pas une obligation, un système avec beaucoup d’éléments et des interactions simples ne pourra être qualifié de compliqué. De même, un système avec peu d’éléments mais des relations difficiles à résoudre sera compliqué mais pas complexe.

> Peut-on transformer un système d'un type en un autre ?

Tout à fait ! Mais à ma connaissance, pas dans tous les sens.
Rendre un système complexe en compliqué c'est tout à fait faisable, pour cela l'empirisme est la meilleure des alliés. Sachant que le système comporte des inconnus, avec l'expérience et une analyse a posteriori, il est normalement possible de découvrir toutes ces variables qui nous manquaient. Donc, on se retrouve avec un système d'inconnus connus, soit un système au pire compliqué au mieux évident.

> Et les antonymes de chacun, ça serait quoi ?

Pour ça je vais m'appuyer sur le site du [Centre National de Ressource Textuelles et Lexicals](https://www.cnrtl.fr) et tout particulièrement sa fonction de [recherche d'antonyme](https://www.cnrtl.fr/antonymie/). Sans surprise, on retrouve certains mêmes antonymes pour les 2 termes :

| Termes | Antonymes |
|-----------|--------|
| [Complexe](https://www.cnrtl.fr/antonymie/complexe) | brut, incomplexe, rudimentaire, sommaire, élémentaire, clair, simple |
| [Compliqué](https://www.cnrtl.fr/antonymie/compliqu%C3%A9) | aisé, facile, sobre, élémentaire, clair, simple|

Donc faites votre choix.

## Bénéfices

Maintenant que vous connaissez les subtilités entre ces 2 termes, vous vous demandez peut-être en quoi cela va vous aider dans votre travail de tous les jours, hormis employer le bon terme pour la bonne situation. Et bien, il va vous permettre d'aborder n'importe quel type de problématique ou situation avec l'approche la plus appropriée bien plus rapidement.

* Problème ou situation compliquée : faites-vous aider par un expert, il vous évitera une montée en compétence et économisera du temps.
* Problème ou situation complexe : privilégiez des personnes ayant une meilleure réactivité pour faire face aux inconnus qui apparaitraient sans prévenir (qui a dit agile ?). Si possible, explorez plusieurs pistes en parallèle, de plus l’empirisme et les itérations courtes sont les meilleurs moyens d’apprentissage et de décomplexification du sujet. [ref]Merci à [William Bartelett](https://www.linkedin.com/in/punkstarman/) de m’avoir aidé à compléter ce point. [/ref]

Bien sûr, dans la vraie vie, ce n'est jamais blanc ou noir, mais toujours un subtil mélange des deux. À vous alors de subdiviser la problématique pour mieux affecter les bonnes ressources aux bons endroits. Si malgré vos efforts le mélange reste indissociable, alors créez une équipe à l'image de la problématique.

## Ouverture

Si vous n'en avez toujours pas assez, sachez qu'il existe tout un cadre conceptuel d'aide à la prise de décision qu'est [Cynefin](https://fr.wikipedia.org/wiki/Cadre_conceptuel_Cynefin). Je vous dirais juste que « complexe » et « compliqué » sont deux termes piliers de ce cadre et qu'une bonne compréhension est indispensable pour l'aborder. Je ne vais plus m'étendre que ça sur le sujet, car il est extrêmement riche et mériterait un ou plusieurs billets à lui seul.
