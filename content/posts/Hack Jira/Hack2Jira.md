Title: Hack 2 Jira
Date: 2024-08-12
Modified: 2024-08-12
Tags: Jira, Atlassian, Workflow, Flux de travail
Keyword: Jira, Atlassian, Workflow, Flux de travail
Category: Outils, Jira
Slug: Hack-2-Jira
Author: Matthieu
Description: Jira et ses flux de travail peuvent devenir un enfer, mais ce n’est pas une fatalité avec un peu d’ingéniosité.
Summary: Jira et ses flux de travail peuvent devenir un enfer, mais ce n’est pas une fatalité avec un peu d’ingéniosité.
Status: published
Cover: /images/Jira-workflow.jpg

# Préambule
J’utilise « Hack » dans [le sens noble du terme](https://fr.wikipedia.org/wiki/Hack#Rapport_%C3%A0_l'informatique). La traduction la plus communément admise est « bidouillage », donc très loin du mec encapuchauné derrière un écran où défile plein de lignes en vert sur fond noir. Je vais donc vous expliquer l’utilisation non conventionnel que je fais des workflows Jira. Si vous pensiez trouver une astuce pour passer super-admin de l’instance … passez votre chemin.

# Contexte
Jira est un outil incontournable dans le monde de la gestion de projet. Il a réussi à s’imposer comme la référence dans de nombreux domaines. Et c’est mérité ! Flexible, personalisable, fiable, très outillé (rapports, graphiques, plug-in, connecteurs, …). Il peut rapidement devenir le compagnon indispensable de toute une organisation pour le suivi de ses activités. Une véritable tour de contrôle panaoptique.  
Cependant, un point de frustration que j’ai souvent rencontré dans les équipes est la contrainte des flux de travail. Lors de la mise à l’echelle du produit, la tentation est grande de vouloir harmoniser et contrôler un minimum son utilisation. On peut y trouver des avantages mais souvent au prix de la création d’instances de décisions et d’administrations qui s’alourdissent inexorablement. 🤢

## L’enfer, c’est les flux.
Il n’est pas rare de trouver des flux de travail sous Jira qui ont demandé des semaines, voire des mois de travail, à de nombreuses personnes pour se mettre d’accord sur quelque chose de satisfaisant. On se retrouve alors avec des choses comme ça 😱:

![WF support]({attach}JIRA-Workflow-dedales.jpg){.image .width-75}
source : https://abhilashshukla.com/business/best-practices/jira-workflow-for-software-development-and-quality-analyst-qa-teams/

Je ne leur jette pas la pierre, car cela correspond à leur schéma de pensée et Jira induit fortement ce genre de solution. J’ai moi même cédé cette facilité quand on m’a demandé de mettre en place des équipes. Mais finalement, j’ai rapidement été confronté à plusieurs limitations qui m’ont fait pester contre l’outil.

* Complexité de mise en place 
* Rigidité du processus
* Manque d’adhésion des équipes
* Standardisation à outrance
* Administration chronophage
* …

Il faut savoir que potentiellement, chaque projet Jira peut tout à fait avoir son propre flux de travail, ses propres types de tickets, ses propres priorités, ses propres gestions de droits et profils, etc. La contrepartie est une administration plus complexe pour chaque projet et pour ceux qui hébergent leur propre instance, type grand compte, les performances en sont fortement impactées. Forcément, face à ces limitations, surtout cette dernière, un gros effort d’harmonisation, mais surtout de limitation est mis en place. Ce qui fait perdre la majorité des avantages cités avant.

# Ma vision
Je ne dis pas que ma vision est la meilleure, mais après avoir usé et abusé des possibilités de cet outil, j’en reviens toujours à quelque chose de plus simple et adaptable. Après une énième complainte des flux mis en place, je me suis pris un moment de réflexion pour essayer de comprendre pourquoi cet outil si prometteur est tant détesté et si l’on ne pourrait pas renverser la vapeur. Et puis je me suis souvenu de la première des valeurs du [manifest agile](https://agilemanifesto.org/iso/fr/manifesto.html) :
> Les individus et leurs interactions plus que les processus et les outils

Et c’est bien là le souci, on a fait de Jira l’alpha et l’oméga de la gestion de projet. Tout, ou presque, doit y être tracé, noté, décrit, expliqué. Tout ça pour obtenir de jolis graphiques qui orneront les revues de sprint. Pour parfaire ce suivi, les flux de travail sont indispensables, avec l’imputation du temps, les commentaires forcés (donc vide de sens), les multiples champs personnalisés, etc.  
Imposer un flux de travail à une équipe est, d’une certaine manière, un signe qu’on ne lui fait pas confiance sur le suivi des processus. On l’infantilise sur la gestion du cycle de vie des tickets. On va donc à l’encontre de la bonne pratique de responsabilisation et d’autogestion des équipes.  

Il est nécessaire de revenir à l’essence de cet outil, qui nous permet de ne pas se perdre dans l’avancement du produit, sans fioriture, de manière simple (KISS), fluide. Mais pour cela, il faut opérer une rupture radicale dans son utilisation et les flux de travail sont en première ligne. Alors faisons les choses bien ! Dynamitons complètement ces fichus flux ! 🤯

## 1ère idée
Elle est très simple à mettre en place et à utiliser. Elle use et abuse de la transition « Toutes » 😈
![Transition Toutes]({attach}Toutes.jpg){.image}

C’est une transition particulière dans Jira. Elle n’a pas d’origine précise, mais ouvre la possibilité d’accéder depuis n’importe quel point du flux de travail à l’étape pointée. Très utile pour des états d’attente ou d’annulation puisqu’elle peut intervenir à n’importe quel moment.  
Et bien tout repose sur cette propriété ! Mettons tout un tas d’état accessible tous le temps depuis n’importe quel point de ce non-flux de travail !

![WF alpha]({attach}WF_alpha_jira.jpg){.image .width-75}

Le résultat est à peu de chose près un trello (produit du même éditeur). 😜

J’y vois plusieurs avantages :

* Facilité de mise en place (administration simplifiée)
* Évolution sans impact sur l’existant (rétro-compatibilité)
* Configuration Jira d’un seul système de flux pour toutes les équipes (mise à l’échelle)
* Personalisation complète des flux par projet via le tableau des tâches (acceptabilité)

Mais n’est pas exempte de défaut non plus :

* Nombreux états inutiles, visibles et accessibles
* « Perte » de tickets plus facile si non mappée dans les colonnes.
* Menu de transition dans le détail d’un ticket trèèèès long.

Personnellement, je trouve que la balance penche très fortement du côté des avantages.  
Très enthousiaste par cette idée et récemment catapulté responsable des pratiques Jira sur ma mission, je fais le tour des chefs à plumes pour vendre mon idée. Autant vous dire que ça n’a pas déchainé un enthousiasme exubérant 😅  
Coté administrateur Jira, ils étaient circonspects sur l’idée, car elle sortait complètement du cadre. Coté chef, on m’a gentillement rappelé qu’il fallait tout de même sortir certaines métriques de Jira et que certaines étapes devaient être incontournables pour avoir quelques données fiables.

## 2ème essai
Je suis donc retourné sur ma planche à dessin pour satisfaire au mieux toutes ces parties prenantes.  
Après avoir pas mal retourné et torturé l’outil de conception de Jira j’ai finalement réussi à obtenir quelque chose de convaincant et répondant aux exigences de chacun.
<!-- img class="image width-75" id=WF2 src='{attach}WF_Draw.jpg' onmouseout=WF2.src='{attach}WF_Jira.jpg' onmouseover=WF2.src='{attach}WF_alpha_jira.jpg'></img -->

![WF Jira deuxième]({attach}WF_Draw.jpg){.image .width-75}
<i>NB : si une ligne n’a pas de flèche, c’est qu’elle est bi-directionnelle</i>

Sur ce flux, il y a 4+1 étapes incontournables : Backlog, Todo, Doing, Done + Canceled. Ce dernier état est un peu à part, car c’est une voie de garage sans grand intérêt pour notre sujet. Les 4 étapes principales sont toutes accessibles par la transition «Toutes», donc il est tout à fait possible de passer de Backlog à Done, même si ce n’est pas l’objectif.  
On voit clairement le découpage en 3 parties distinctes de ce flux et le passage de l’une à l’autre de ces parties peut uniquement se faire via nos 4 étapes indispensables. Par section, on constate aussi que toutes les étapes sont reliées entre elles de manière bi-directionnel. Ce qui veut dire que depuis n’importe quel état d’une section, on peut passer à n’importe quel autre état du même type. On simule, autant que possible, la transition « Toutes », en limitant cette possibilité à une section du flux.  
Pour passer d’un type d’état à l’autre, on est donc bien obligé de passer par l’une de nos 4 étapes essentielles … objectif atteint.

* Coté avantages, on reprend à l’identique celles de la première idée et, du fait de l’obligation de passer par certaines étapes, on améliore la fiabilité de la métrologie et des statistiques.  
* Coté inconvénient, on réduit sensiblement la longeur du menu des transitions dans le détail des tickets, le nombre d’état accessible est lui aussi un peu plus limité.

Donc, au final, on n’y est pas vraiment perdant et personne n’a vraiment eu de choses à redire dessus. Donc la mise en production a été accepté sans trop de mal.

## Utilisation

Visuellement, pour l’utilisation de tous les jours, ça donne ça :

![Tableau Kanban]({attach}Full-Kanban.jpg){.image .width-75}

Plus de limitation sur le déplacement des tickets et, détail intéressant, dans la colonne « Terminé », on a les états « Done » et « Canceled » bien identifiables. C’est un effet collatéral intéressant d’avoir mis ces deux états accessibles tout le temps, il n’est plus possible de placer des tickets dans l’état « Canceled » par mégarde.  
Sur les flux plus *classiques*, l’état « Canceled »  a souvent la transition « Toutes », alors que le « Done » était uniquement accessible via le chemin imposé. Du coup, quand on souhaitait *terminer* un ticket dans un état qui ne pouvait normalement pas y accéder ET que l’état « Canceled » était mappé dans cette même colonne, la colonne était tout de même accessible, mais pas pour l’état que l’on supposait … et j’ai souvent récupéré des tickets abandonnés à tort.  
Avec ma solution, ça n’est plus possible.

# Autres changements notables

## Priorités

Un comportement par défaut de Jira qui me semble trompeur est le positionnement par défaut des tickets à « Medium / Moyen » lors de leur création. Comment faire la distinction entre les tickets nouvellement créés et potentiellement avec une priorité encore non positionnée de ceux réellement à ce niveau. J’ai donc fait modifier le système de priorité comme suivant, en y intégrant une valeur « Not prioritized / Non priorisé » comme valeur par défaut.
![Prio Jira]({attach}Jira_prio.jpg){.image .width-25}

En soi, rien de révolutionnaire, mais un petit ajustement bienvenu.

##  Support

Je considère les tickets de support en bordure du projet et peux faire l’objet d’un flux de travail particulier et plus adapté. Pourquoi je dis « bordure du projet », c’est que les personnes susceptibles de créer un ticket de ce type ne font pas obligatoirement partie de l’équipe, qu’elles n’ont pas forcément l’habitude d’utiliser cet outil et qu’il faut donc leur faciliter la tâche le plus possible en les guidant un maximum. Donc, dans ce cas, hors de question de leur laisser changer l’état du ticket de manière trop libre.

![WF Support]({attach}WF-support.jpg){.image .width-50}

# Retour d’expérience

Après quelques semaines de mise en place sur un projet pilote, je n’ai pas eu beaucoup d’ajustement à faire. Deux états au niveau de la réalisation, des automatismes sur les transitions, mais rien de plus. En ayant fait un peu la promotion de ce flux, d’autres projets ont demandé à y passer … puis il est devenu le flux de référence pour le programme, ce qui a fortement incité les autres projets à y passer.  
Encore quelques semaines sont passées et l’ensemble des projets en est content. Dernièrement, j’en ai aussi parlé en dehors du programme et certaines personnes avaient l’air intéressées, ce qui est encourageant pour la suite.
Chose intéressante, les projets voyant que l’on pouvait *vraiment* adapter l’outil pour le rendre plus agréable, en sont venus à me demander d’autres modifications. Par exemple, les champs bien trop nombreux et d’une utilité douteuse.  

# Conclusion

Encore une fois, je ne sais pas si ce que j’ai mis en place sera adapté à votre situation. Dans mon cas, j’en suis satisfait et les équipes le semblent aussi, donc pourquoi se priver 😁.  
Hésitez pas à me faire un retour si vous vous êtes inspiré des flux présentés ici, je serais très heureux d’en discuter de vive voix 😄.
