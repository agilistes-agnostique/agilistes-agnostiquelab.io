Title: Équité vs Égalité
Date: 2023-03-25
Tags: vocabulaire, justice, équité, égalité
Category: Parlons peu, mais parlons bien
Slug: Équité-vs-Égalité
Author: Matthieu
Summary: Dans un monde plus harmonieux, on veut souvent plus d'égalité. Mais est-elle la plus adéquate ? Quid de l'équité ? Y a-t-il des situations où l'égalité est préférable et à d'autres moments l'équité ? Et la justice dans tout cela ? La morale est-elle préférable ? Défrichons tout cela ensemble …
Status: published
Cover: /images/Équité-vs-Égalité.jpg
Cover_Source: https://unsplash.com/photos/woman-in-dress-holding-sword-figurine-yCdPU73kGSc

Pour ce deuxième billet de [la série](https://anargile-leanbertaire.info/articles/Parlons-peu-mais-parlons-bien.html), penchons-nous sur les termes « Équité » et « Égalité ». Ils semblent plus clairement distincts des termes de notre premier billet ( « [Complexe VS Compliqué](https://anargile-leanbertaire.info/articles/Complexe-vs-Compliqu%C3%A9.html) »), mais leur utilisation peut s’avérer tout aussi subtile dans le choix de l’un ou l’autre.
Tout est parti d’une simple image qui m’a interpellé :

![baseball]({attach}baseball.jpg){.image .width-75}
[Source](https://www.storybasedstrategy.org/the4thbox)

Mais revenons aux bases et déroulons notre analyse.

## Étymologie

* Équité : Du latin aequitas
* Égalité : Du latin aequalitas 

Première surprise pour ces 2 mots qui nous semblent si distincts, ils partagent la même racine latine. Leur utilisation serait donc plus subtile qu’on ne le pense ? Nous allons voir ça.

*Aequita* est un dérivé de *aequalis*, communément traduit par « égal », c’est le contexte où il est utilisé qui nous guide sur son sens profond et nous fait comprendre toutes les subtilités de ce terme passe-partout.

* Temporel : « Contemporain, … »
* Qualité : « semblable, pareil, uni, … »
* Mesure : « de même niveau, semblable, … »
* Nature :  « Constant, invariable, uniforme, … »

Pour l’anecdote, *Aequitas* est lui-même un dérivé de *Aequo* comme dans l’expression « exaequo ».

## Définitions

Allons voir ce que donnent les définitions actuelles de ces termes.

### Équité

> Disposition de l’esprit consistant à accorder à chacun ce qui lui est dû. Manière de résoudre les litiges qui consiste à reconnaître impartialement le droit de chacun, sans faire acception de personne et sans obéir à d’autres principes que ceux de la justice distributive.
[Source](https://www.dictionnaire-academie.fr/article/A9E2430)

Nous avons là des notions de justice, de dû, de litige, d’impartialité. Ça en fait un terme lourd de sens.

### Égalité

> Qualité de ce qui est égal. Qualité de ce qui ne varie pas
[Source](https://www.dictionnaire-academie.fr/article/A9E0541)

Bon, un petit tour pour savoir ce qu’ils disent du terme « égal » semble nécessaire.

> Semblable soit en nombre, en quantité, soit en nature, en qualité. Semblable à lui-même, qui est toujours le même, qui ne présente pas d’inégalités. Personne qui a le même mérite, les mêmes droits, le même rang social qu’une ou plusieurs autres.

Donc un sens plus facile à appréhender et utiliser.

## Éclairage

![philo]({attach}philo.jpg){.image}
[Source](https://www.lalibre.be/debats/opinions/2011/12/26/fiche-philo-egalite-vs-equite-N2NDNMZBWND5PHD5LJU6UFRJEM/)

L’égalité semble le terme le plus simple à utiliser et il l’est bien. Son application pratique suit une logique simple et rapide à mettre en place. Dans notre mode de pensée, on rapprocherait ça du mode intuitif, réactif.

On pourrait se dire que l’équité est préférable à l’égalité, plus *juste*, mais pas forcément. 
Imaginons qu’un train est bloqué, avec ses passagers, durant plusieurs heures. La compagnie ferroviaire propose une compensation pour ce temps « perdu ». Le réflexe premier serait d’affecter la même compensation à tous, alors que l’équité pourrait être de différencier la 1ère et 2ème classe. Les premiers ayant payé plus cher leur billet.

L’équité nécessite un travail d’observation, d’analyse et de réflexion bien supérieur à l’égalité. Le mode cognitif, analytique de notre cerveau. En plus de cela, le sens de l’équité n’est pas unique et uniforme, chacun pouvant avoir ses propres critères et priorités pour juger de cette notion. L’égalité faisant fi de toutes ces problématiques, elle peut devenir acceptable quand le temps nous est compté … dans l’attente d’une meilleure solution plus équitable.

### Exemples

Commençons par un exemple simple. La remise de carburant à la pompe que le gouvernement a instaurée pendant un temps était parfaitement égalitaire. Son remplacement par un chèque carburant a lui été équitable, car modulé suivant un critère bien défini.

Dans le domaine mathématique, l’égalité au sens arithmétique du terme est une évidence, mais l’équité peut-elle s’y aventurer ? Eh bien oui ! C’est un domaine de recherche actif dont l’un de ses meilleurs représentant est le problème du « [Partage équitable](https://fr.wikipedia.org/wiki/Partage_%C3%A9quitable) » ou toute la problématique est de trouver un moyen ou algorithme pour qu’un partage soit le plus équitable possible. Une résolution simple pour 2 joueurs est « je coupe, tu choisis ». Mais on peut complexifier le jeu en ajoutant des joueurs et pourquoi pas des priorités différentes. Ça vous parait abstrait et pas vraiment utile ? Alors imaginez la galère que ça a dû être de découper Berlin entre les Russes, Américains, Anglais et Français après la Seconde Guerre mondiale.

![Berlin]({attach}Berlin_Blockade-map.jpg){.image .width-25 }

« Liberté Égalité Fraternité » la devise de la république française comprend le terme « égalité », on pourrait se demander si « équité » ne serait pas plus adapté, mais il ne faut pas oublier qu’elle fait allusion à l’égalité face au droit. Domaine où ce terme et son sens est tout à fait adaptés.

## FAQ

>Ces deux termes s’excluent-ils ?

Faire une distribution de croissant avec comme règle 1 par personne est égalitaire tout en étant équitable. Donc non les 2 termes ne s’excluent pas forcément.

>Leur domaine d’utilisation est-il le même ?

Comme montré avec l’exemple des mathématiques, il n’y a pas de domaine où l’égalité ou l’équité n’a pas sa place, ils restent souvent complémentaires.

>L’expression « C’est pas juste » est-elle pertinente ?

Les enfants adorent cette expression pour exprimer une grande frustration. Cependant, ils font plus référence à leur morale face à une situation qui semble déséquilibrée. La distinction entre le bien et le mal est un exercice difficile, c’est pour cela que la justice, avec les lois, existe.
Donc, dans l’utilisation qu’on lui connaît, cette expression est trompeuse, mais reste pertinente pour se faire comprendre.

## Bénéfices

Dans des projets en environnement complexe, où la réactivité est aussi essentielle que les bonnes décisions, savoir distinguer l’équité de l’égalité est indispensable. Remettre en cause des options trop simplistes ou trop compliquées est une capacité précieuse.
Vos fonctionnalités prendront toute leur valeur si vous arrivez à exploiter correctement les particularités de vos personas (en espérant que ces dernièrs soient les plus justes possibles).
Votre capacité à sortir une réponse rapidement en concevant des solutions égalitaires vous permettra de prendre l’avantage sur vos concurrents et contenter vos utilisateurs.

## Ouverture

On rapproche souvent l’équité et la justice alors qu’il y a une division nette entre les deux. La justice applique la loi … et rien d’autre. Si une loi est absurde ou même immorale, il sera « juste » de l’appliquer. Par exemple, pour nos spectateurs du début d’article, la justice ressemblerait plutôt à ça :

![tickets]({attach}tickets.jpg){.image .width-25}

Les lois sont un ensemble de règles souvent écrites, imposées ou non, partagées par tous les membres d’une même communauté.

J’ai fait allusion à la morale en parlant de justice et ces deux notions sont bien plus proches qu’on ne le pense. La morale est un ensemble de règles, souvent orales ou tacites, respectées (de force ou non). Mais contrairement aux lois, elles sont extrêmement variables suivant de très nombreux critères (époque, culture, religion, localisation, météo, écologique, …). Vous n’avez peut-être pas la même morale que votre voisin, mais ça ne vous empêche pas de vivre côte à côte.

Mais ça mériterait d’y consacrer un autre article à part entière …

![sélection]({attach}sélection.jpg){.image .width-50}
