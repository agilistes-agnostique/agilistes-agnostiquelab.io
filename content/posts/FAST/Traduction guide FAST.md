Title: Traduction guide FaST 2.12
Date: 2023-11-05
Tags: FaST, traduction
Keyword: FaST, traduction
Slug: Traduction-guide-FaST-2.12
Author: Matthieu
Description: Traduction complète en français du guide FaST 2.12. Envie de changer de Scrum ? Venez découvrir la traduction complète en français du guide FaST !
Summary: Envie de changer de Scrum ? Venez découvrir la traduction complète en français du guide FaST !
Status: published
Cover: /images/ExtendendedLogoOnBlack2.png

![Logo]({attach}ExtendendedLogoOnBlack.png){.image .width-75}

FaST est un cadre de travail assez récent proposé par [Ron Quartel](https://www.linkedin.com/in/rquartel/) en [2019](https://ronquartel.medium.com/embracing-chaos-34223ad75c1e) au [Agile Boston(https://www.agileboston.org/uncategorized/the-open-leadership-symposium). Il a fait [un billet explicatif](https://ronquartel.medium.com/embracing-chaos-34223ad75c1e) de sa genèse. 

![Ron Quartel]({attach}ronTeach.jpg){.image .width-75}

Une version 2 publiée en mars 2021 l'a fait [plus largement connaître](https://claudeaubry.fr/post/2021/fast/) en France. Des traductions [espagnole, allemande, polonaise et slovaque](https://fluid.scaling.tech/fast-guide) existent dans la version actuelle (2.12 juillet 2022) mais rien en français. Enfin, pas tout à fait, on trouvait bien le très bon [article de Robin Béraud-sudreau](https://coach-agile.com/2021/12/fluid-scaling-technology-for-agile-and-agile-at-scale/), mais qui est une traduction partielle et adaptée d'une ancienne version (2.1).

La présentation par [Alexandre Boutin](https://www.youtube.com/watch?v=hjU1G-pSlgk) à Agile Grenoble 2022, me l'a vraiment fait découvrir. Je n'avais fait qu'entendre parler du sujet avant. Dans sa conférence, Alexandre, regrettait le peu de retour d'expérience sur ce cadre de travail. La graine était plantée dans mon esprit. Depuis, j'ai bien potassé le sujet et je trouvais dommage qu'il n'existe pas une version française du guide, ce qui faciliterait grandement sa popularisation et son expérimentation.

Ce manque est maintenant comblé ! Je me suis relevé les manches, sorti [mon plus beau clavier](http://bepo.fr/wiki/Accueil) et apporte ma contribution avec cette traduction intégrale et officielle !
[Lien local de téléchargement du Guide FaST]({attach}FASTGuide2.12 - FRENCH.pdf)  ou [sur le site officiel](https://fluid.scaling.tech/fast-guide).

Je tiens tout particulièrement à remercier les personnes suivantes pour leur aide :

- [Jean-Yves Klein](https://www.linkedin.com/in/jean-yves-klein), pour son aide à la traduction et relecture
- [Romain Valluy](https://www.linkedin.com/in/romainvalluy), pour son œil avisé de relecteur
- [Alexandre Boutin](https://www.agiletoyou.com/), pour m'avoir fait découvrir ce cadre et son aide à la relecture
- [Laurent Thomas](https://www.linkedin.com/in/laurent-thomas-389b51195/), pour son expertise dans les traductions


Pour ceux que ça intéresserait, voilà le [lien public](https://sheet.zoho.eu/sheet/open/c2zep22f4e9d364204e9ba633e23e05e02a5b) (en lecture seule) du document de travail qui a été utilisé. Vous y trouverez mes interrogations et doutes quant aux traductions de <del>certains</del> pleins de terme.

Les traductions des termes les plus notables sont :

- Product Manager => Responsable produit
- Team Steward => Hôte d'équipe
- Feature Steward => Référent fonctionnel
- Discovery Tree => Arbre d'exploration
- Marketplace => Bourse du travail
- The Collective Agreement => Accord du collectif
- FaST Meeting => Assemblée FaST
- Personal Mastery => Comportement professionnel
- Product map => Cartographie du produit

Je sais que certaines traductions soulèveront de nombreux commentaires, mais c'est un choix tout à fait assumé et je me ferais une joie d'en discuter avec ceux qui le souhaitent.
En attendant, bonne lecture et peut-être expérimentation :-)
