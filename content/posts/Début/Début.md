Title: Début de l’aventure
Author: Matthieu
Date: 2023-01-01
Category: Admin
Tags: Autre
Slug: debut-de-l-aventure
Summary: Un billet très rapide pour marquer le début de cette tribune et y décrire son but.
Cover: /images/debut-de-l-aventure.jpg
Cover_Source: https://unsplash.com/photos/pen-on-book-_CGxNOLM1gQ


En ces temps où la vidéo est roi et la brièveté la norme (coucou tik tok), j'ouvre ce blog à contre-pied. Je pense que pour des sujets compliqués tels que la gestion de projets/changements/production/humaine, la notion de temps est totalement différente. Il est nécessaire de prendre le temps de lire et comprendre les sujets dans toute leur complexité, pour cela un médium écrit est le plus adapté, on a le temps de réfléchir à ce que l'on vient de lire, d'aller voir les références insérées par l'auteur, de construire sa critique (positive) en revenant sur des passages précis de ce que l'on vient de lire.

> Mais pourquoi un blog ? Facebook/LinkedIn/Medium ne suffisent pas ? Tu vas avoir du mal à attirer du monde ! Personne ne va te lire !

Hé bien non ! Je ne trouve pas ces supports adéquats. On se retrouve noyé dans un océan d'articles en tout genre, de qualités variables et en retrouver un en particulier est une tâche ardue. Alors que si on regroupe tout dans un site spécifique, on a une bonne idée de ce que l'on va y trouver et y faire des recherches est bien plus facile (mettre en place un moteur de recherche est sur ma liste d’amélioration). De plus, on peut ignorer ou non cette tribune sans se la voir imposée suite aux caprices d’un algorithme obscur. Pour ce qui est de l'affluence sur le site, ce n'est pas ça qui me motive et je préfère avoir une petite audience avec des lecteurs de qualité, que passer des heures à trier les commentaires pertinents du reste.

Pour ceux qui se demandent à quoi rime le titre de ce blog «Anargile & Leanbertaire», il s’agit de croisement entre « anarchisme + agile » et « lean + libertaire ». On y trouve des similitudes assez fortes sur le type d’organisation proné. En cherchant un peu sur le net, on trouve un certain nombre d’articles sur ce sujet.

Donc, je vous souhaite la bienvenue sur ce site qui n’a que pour ambition de satisfaire au besoin d’écrire et publier de son initiateur et des autres auteurs qui voudront bien le rejoindre. Les sujets traités tourneront autour de « l’Agilité » et du « Lean » au sens large, tout en se permettant de déborder sur des sujets connexes qui auront du sens.

@+
