Title: Parlons peu, mais parlons bien
Date: 16/01/2023
Category: "Parlons peu, mais parlons bien"
Slug: Parlons-peu-mais-parlons-bien
Cover: /images/Parlons-peu-mais-parlons-bien.jpg
Cover_Source: https://unsplash.com/photos/gray-microphone-in-room-ALM7RNZuDH8
Tags: Introduction

Cet article inaugure une série, que j'espère à très long terme. L'objectif est de prendre 2 termes qui paraissent synonymes, mais qui, dans le détail de leurs différences, peuvent faire basculer une décision capitale. Le dicton « Parlons peu, mais parlons bien » me paraissait tout à fait adapté pour de multiples raisons que je vais détailler.

Dans n'importe quel domaine, la communication entre les personnes a une place extrêmement importante et conditionne souvent l'échec ou la réussite des projets. Au fil de mes expériences, je me suis rendu compte qu'énormément de problèmes et quiproquos étaient essentiellement dus à une incompréhension entre les interlocuteurs. Incompréhensions qui peuvent engendrer des sentiments de frustration, colère voire rancune ou animosité envers autrui. Un réflexe primaire serait de rapidement s'enfermer dans une démarche procédurière, administrative, distante voire conflictuelle qui va à l'encontre des valeurs agiles. Les effets étant dévastateurs pour le projet (ralentissement, retard, sous-qualité, abandon, etc.)

L'origine de ces incompréhensions est souvent que la signification des mots employés n’est pas forcément la même de part et d'autre. Cela peut se comprendre par l'utilisation de jargon ou abréviation métiers spécifique, mais parfois par la simple maîtrise de la langue elle-même. Tout le monde n'a pas un dictionnaire comme livre de chevet (pourtant super efficace comme somnifère). Il est donc utile que dès le début on se mette d'accord sur un certain nombre de termes sensibles (ex : livraison, PV, anomalie, etc.). Si en plus vous mettez ça par écrit dans votre documentation (qui est tenue à jour bien évidemment), vous ferez le bonheur des nouveaux arrivants et souvent éviterez des longs débats inutiles. Bien évidemment, les définitions ne sont pas immuables et peuvent tout à fait évoluer au fil du contexte du projet.

Les plus perspicaces d'entre vous auront pressenti qu'il ne faut pas se limiter à la seule définition de certains termes. En effet, là où le vocabulaire est la vision micro des relations, il faut élargir son horizon et aussi définir les processus, sans trop en faire non plus. Les définitions de prêt et terminé (« Définition of ready/done ») en sont un très bon exemple. Mais nous sortons du cadre de la série d'articles que je souhaite mener.

Je vous propose donc un premier billet sur le couple "Complexe VS Compliqué" qui, vous le verrez, vous ouvre la voie vers d'autres notions bien utiles dans le conseil et suivi des projets.
