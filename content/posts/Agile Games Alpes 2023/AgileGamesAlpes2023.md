Title: Agile Games Alpes 2023
Date: 2023-09-19
Tags: Agile Game Alpes, Serious Game
Keyword: jeux, Alpes, Agile, Game
Slug: Agile-Games-Alpes-2023
Author: Matthieu
Description: Petit retour en images de l’édition 2023 de Agile Games Alpes
Summary: Petit retour en images de l’édition 2023 de Agile Games Alpes
Status: published
Cover: /images/AGA23.jpg

![AGA23]({attach}AGA23.jpg){.image .width-75}
Agile Games Alpes est destiné a tous les agilistes en manque d’occasion pour tester de nouveaux jeux, faire des «crash test», expérimenter, animer tout type de jeux sérieux (ou pas).
Cet évènement n’est pas si vieux (#4), mais est assez connu et reconnu pour qu’[un simple billet](https://www.linkedin.com/posts/alison-jorre-%F0%9F%8F%B4%E2%80%8D%E2%98%A0%EF%B8%8F-5bb5a4a9_aga23-agile-billetterie-activity-7043529160706732032-UGMr?utm_source=share&utm_medium=member_desktop) sur les réseaux sociaux soit suffisant pour faire sauter la billetterie le jour J.
Pour ceux qui ne connaissent pas, cela se passe dans les Alpes sur 2 jours, ce qui est suffisant pour épuiser les participants en général ;) et c’est TOUT ! Enfin, il y a quand même le gîte et le couvert qui est prévu. Mais pour le reste, c’est un forum ouvert de jeux.

![Panneau des jeux]({attach}Panneau_jeux.jpg){.image .width-75}

Cette année, le lieu choisi était [le camping de Savel](https://www.openstreetmap.org/way/832199380) à Mayres-Savel, sur le bord du Drac.
Nous y avons été accueillis par l’équipe de non-orga qui nous a expliqué les règles du jeu de ces 48h.

| | | 
| - |-|
| ![Panneau accueil]({attach}Panneau_accueil.jpg){.image .fit} | ![Panneau des règles]({attach}Règles.jpg){.image .fit} |

Je ne vais pas vous lister tous les jeux qui y ont été joués, car ils sont trop nombreux et je n’ai participé qu’à une toute petite partie. Donc, voilà quelques photos en vrac pour vous donner envie de venir l’année prochaine.

| | | 
| - |-|
| ![abris extérieur]({attach}abris_ext.jpg){.image .fit} | ![Alex_BPO]({attach}Alex_BPO.jpg){.image .fit} |
| ![BigPayOff]({attach}BigPayOff.jpg){.image .fit} | ![extérieur]({attach}extérieur.jpg){.image .fit} |
| ![Grenoble plage]({attach}Grenoble_plage.jpg){.image .fit} | ![lego4Unfix]({attach}lego4Unfix.jpg){.image .fit} |
| ![piscine]({attach}piscine.jpg){.image .fit} | ![repas]({attach}repas.jpg){.image .fit} |
| ![streess]({attach}streess.jpg){.image .fit} | ![totem]({attach}totem.jpg){.image .fit} |

Comme d’habitude, l’ambiance y était très amicale et bienveillante. Le repas du jeudi soir nous a permis de nous retrouver dans un moment convivial, nous raconter ce que nous sommes devenus depuis l’année dernière (oui, il y a quelques habitués) et faire de nouvelles rencontres très enrichissantes. Cette première soirée a été relativement sage pour garder ses forces pour la suite des évènements, mais des instruments de musique ont été de sortie pour animer le tout.

Le lendemain matin, après un bon petit dej, les participants s’auto-organisent sous l’œil bienveillant des non-organisateurs. Le programme se fait tout seul en une poignée de minutes, on trouve de tout, des « ice breaker » à la chaîne, des jeux sur l’inutilité des planifications à long terme, des jeux plus ludiques ou très sérieux, des légos, d’autres introspectifs et bien plus encore. Le repas du midi nous permet une pause bien méritée avant de reprendre les activités.

Le lieu étant bien équipé, des baigneurs ont profité de la piscine tant que le soleil était de la partie. Des canadairs sont même passés faire coucou dans la rivière tout à coté. Impressionnant de voir ces engins raser l’eau quelques secondes avant de repartir dans les airs. Aucun feu n’étant a déploré dans les environs, il semble que cela était un entraînement. Plus tard dans l’après-midi, le billard a été mis à contribution. Le temps estival permettant de profiter de l’immense terrasse et de ses activités environnantes. Certains se sont même aventurés en canoë sur la Drac !

Le repas du soir sonne la fin des hostilités et des moments de détente pour le reste de la soirée. Je trouve que cette année, cela est resté assez sage et ne s’est pas terminé au bout de la nuit.

Le lendemain, on sentait une baisse de forme, sauf ceux qui ont fait leur footing matinal dans ce merveilleux décor. D’autre sont allés un peu plus tard sur une passerelle himalayenne non loin de là. Mais le gros des troupes a vaillamment continué ses expérimentations jusqu’au repas du midi. Enfin cette dernière après midi a été courte, sachant qu’un certain nombre des participants venait de loin et avait de la route à faire avant de sortir de cette bulle idyllique entre agilistes.

![Photo de groupe]({attach}groupe.jpg){.image .fit}

Merci à tous les participants pour cette bouffée d’oxygène et d’inspiration durant ce trop court laps de temps.
