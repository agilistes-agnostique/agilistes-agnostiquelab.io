Title: Le «(story) point» doit mourir
Author: Matthieu
Date: 2023/01/03
OriginalDate: 2020/05/13
Category: Estimation
Tags: story point, chiffrage, poker planning
Slug: le-story-point-doit-mourir
Summary: Le «story point» est devenu «main stream» avec toutes les dérives qui vont avec. Voyons ensemble comment mettre un coup de pied dans la fourmilière, sans, non plus, nous faire bannir des séances de planifications et estimations.
Cover: /images/le-story-point-doit-mourir.jpg
Cover_Source: https://unsplash.com/photos/revolver-statue-XWC5q9_Xp0o

Le « story point » est devenu « main stream » avec toutes les dérives qui vont avec. Voyons ensemble comment mettre un coup de pied dans la fourmilière, sans, non plus, nous faire bannir des séances de planifications et estimations.

# Revenons aux temps des estimations pré-agile

## Jour / mois Homme

Dans le monde de la gestion de projet informatique, il y a le classique « jour-homme ». Cette unité de mesure magique qui permet de juger de la performance d’une équipe, et surtout de ses membres, et ce individuellement ! Quel confort ! Il suffit de remplir consciencieusement son très cher tableau Excel, ou tout autre outil de gestion de projet, pour voir ses graphiques se mettre à jour et, si besoin, la remontée d’alerte sur le « retard » de l’équipe/membre.

Le suivi budgétaire et de planning n’est pas un mal, bien au contraire si l'on veut s’éviter de mauvaises surprises. Mais sa finalité a été pervertie pour en faire un outil de micro-management et de reproche.

Le corollaire de ce « jour-homme » est le « mois-homme », unité utilisée en macro-gestion pour évaluer la charge de travail globale d’un projet. Avec une date de fin de projet décidée en amont, une simple division permet de dimensionner la taille de l’équipe à recruter pour tenir les délais ... en théorie. C’est là que le très célèbre livre le mythe du « mois-homme » nous explique en long, en large et en travers que cette pratique n’est pas réaliste. La loi de Brooks est issue de ce livre, et dit en substance :

* « Ajouter des personnes à un projet en retard accroît son retard »

Les métaphores pour expliquer, encore mieux, cette loi empirique sont souvent les suivantes :

* « Neuf femmes ne font pas un enfant en un mois. »
* « En étant 300 dans une cuisine pour faire un œuf au plat, il ne sera pas possible de servir le plat 300 fois plus vite. »

## Confusion charge & délai

* Charge : Temps nécessaire pour la réalisation effective
* Délai : Temps nécessaire avant d'avoir la capacité à faire

Chiffrer en jour-homme favorise grandement l’erreur qui consiste à penser que la charge et le délai vont être identiques.

> (Chef)-Combien de temps pour cette tâche ?  
> (Exécutant)-Heuu 2 jours ?  
> (C) -Non, c’est trop ! Je t’en donne 1 ! À demain  
> (E) -Mais ... ( C sort sans attendre la réponse) et les 5 autres tâches que j’ai en cours, j’en fais quoi ?

*Toute ressemblance avec des événements réels ne serait que purement volontaire.*

Cette confusion est un avantage si l'on souhaite garder un flou artistique autour du lien entre la charge de travail et sa livraison. On mélange 2 incertitudes qui ne font que se multiplier, ce qui incite a insérer une marge d'erreur assez énorme pour pallier tout imprévu.

## ETP

L' «ETP» (équivalent temps plein) , est une autre unité de mesure, surtout en vogue dans le milieu des chefs ou directeurs de projet. Elle est bien pratique, car elle permet d’évaluer rapidement le coût global d’un projet, ainsi que son dimensionnement.

C'est un sujet de fierté que d'avoir «plein d'ETP» sous ses ordres (toute ressemblance avec le jeu « c'est qui qui a la plus grosse » ne serait que fortuite). L'ETP peut aussi permettre de fixer des objectifs aux collaborateurs. Par exemple, un chef de projet, suivant son grade, doit avoir entre X et Y ETP sous sa coupe ; un directeur de projet entre A et B, sinon ce collaborateur n'est pas « rentable ».

Cette unité a aussi un autre avantage très intéressant pour cette population. Elle permet de cacher la répartition saucissonnée du temps consacré au projet réparti entre différentes personnes : `1 archi à 10% + 2 dev à 60% + 2 stagiaires (100%) + 1 apprenti (50%) = 3,8 ETP` ! Je vous laisse deviner l'état du projet après 6 mois de cycle en V.

## Bref

Tout cela permet de :

* séparer le bon grain de l’ivraie, le bon exécutant du mauvais, le bon développeur du mauvais,
* monter des « dream team », ces équipes vendues à prix d’or dont chaque membre a prouvé dans ses projets précédents qu’il avait des performances bien supérieures à ses congénères,
* mettre en avant les meilleurs gestionnaires avec leurs plus beaux graphiques, leurs feuilles de calcul les plus complexes pour détecter la moindre variation de productivité de chacun des exécutants concernés.

# La révolution du (story) point

Avec les nouvelles méthodes de projet, le chiffrage en «story point» est apparu.

> (Responsable) Quelle horreur, on supprime l’outil principal de mesure de la productivité !  
> (Exécutant) Quel confort, on enlève la pression du mauvais chiffrage à tenir !

Dans l’optique de responsabilisation des équipes et d’allègement du micro-management, le story point a été assez rapidement adopté dans toutes les organisations voulant faire de l’agile. Finalement, la transition n’était pas si difficile. On ne fait que remplacer l’unité, de « jh » en « point », et comble du bonheur, on peut avoir une correspondance 1 pour 1 (tout autre ratio simple à convertir) .

Les avantages de cette nouvelle unité de mesure étaient clairement de rompre tout lien entre le chiffrage fait et une possible date de livraison.

Le point permet également de valoriser plus facilement la complexité de réalisation. Passer 2 jours sur une réalisation ne veut absolument pas dire que la quantité de code produit sera la même que pour la réalisation précédente, de même taille. Mais annoncer 2 points rend la confusion charge /délai bien plus difficile, voire impossible.

## Le facteur de focalisation

Cette notion est, entre autres, présentée dans le très connu « Scrum depuis les tranchées ». En substance, l'auteur explique que le point doit représenter un « jour-homme idéal », c'est-à-dire une journée de boulot sans repos ni interruption ! Puis faire la comparaison rapide avec la vélocité réelle de l'équipe pour avoir un `%` de focalisation. On en déduit très logiquement que plus ce `%` est élevé, plus l'équipe est dans des conditions idéales de travail. Donc, une grande partie des efforts d'amélioration de l'équipe (au sens large) est souvent orientée sur l'amélioration de ce chiffre.

Vous voyez venir les dérives possibles ?

Actuellement, cette notion n'est guère plus utilisée et ce, grâce au travail des coachs qui martèlent que la correspondance « point <=> jour » est une mauvaise pratique ... à juste titre.

# Sortir de sa zone de confort

Maintenant que le point est maîtrisé, commun, perverti, il serait peut-être temps de passer à autre chose. Mais n’allons pas trop vite ou trop loin, il ne faudrait pas que les fragiles avancées et l’élan d’amélioration initié avec les points ne soient réduits à néant par un refus de la direction/DSI/manager qui se sentirait encore plus perdu.

## L'unité personnalisée

Nous arrivons au point central de ce billet de blog.

L'idée est d'effectuer un simple changement sémantique, on abandonne le terme « point » pour… (roulement de tambour) … **ce que vous voulez** !

Oui, personnalisez le terme ! Appropriez-vous le ! Jouez-en ! Jonglez avec !

Concrètement, un projet dont l'objet principal est l'assurance habitation pourrait prendre le terme « brique ». Celui qui gère la fabrication de voitures peut très logiquement prendre « voiture ». Donc, vous ne parlez plus dans une unité abstraite, loin de tout, mais avec un terme plus proche du métier, porteur de sens. Sachant que bien souvent les projets portent des noms pas forcément évocateurs, c'est un élément qui permettra de mieux situer le contexte du projet.

## Pourquoi ce (simple) changement de sémantique ?

Vous allez peut-être me répliquer que ce changement infime n'aura pas d'influence, que ce n'est « que » de la sémantique. Vous aurez peut-être raison dans certains cas, mais pour les autres, cela peut tout changer. C'est ce que l'on appelle un « nudge », un « coup de pouce ». Les objectifs derrière cette idée sont multiples :

* Ludique : Le fait de choisir son unité en début de projet, participe à la construction de l'équipe et à sa cohésion.
* Casser les codes : Il faut régulièrement sortir de sa zone de confort pour explorer de nouvelles voies qui, à terme, peuvent apporter beaucoup à l'équipe/projet/entreprise.
* Éviter les comparaisons : Avoir le point comme unité de mesure sur tous les projets incite, volontairement ou non, à faire des comparaisons inter-équipe, à chercher des corrélations. Alors qu'il est impossible de comparer des choux et des carottes, des briques et des trottinettes.

## Un pas de plus ?

Mais ne nous arrêtons pas en si bon chemin. Ne pourrait-on pas se passer des points et garder juste l'unité ? Pourquoi se limiter à 1 seul terme ? Pourquoi ne pas se faire une échelle de valeur complète ? Si je reprends les exemples précédents, on pourrait obtenir quelque chose comme ça :

| Équivalence point | Assurance habitation | Fabrication voiture|
| :---------------- | :------------------- | :----------------- |
|0| poussière | pied|
|1| sable | mono-cycle|
|2| gravier | vélo|
|3| abri | Solex|
|5| cabane | moto|
|8| refuge | voiture|
|13| maison | van|
|20| villa | bus|
|40| immeuble | bus anglais|
|100| gratte-ciel | voiture supersonique|
|∞|ascenseur-spatial |fusée-interplanétaire|

L’équivalence en point est là uniquement pour se donner une idée, mais notre nouvelle échelle de point peut tout à fait en être complètement décorrélée. J’ai une illustration parfaite qui démontre que cette étape est à la portée de tous ! Une ESN bien connue a édité un jeu de cartes pour le poker planning qui ressemble à ça : ![Cartes PP]({attach}Cartes_PP.jpeg) On y retrouve une échelle de valeur, certes correspondant aux classiques points, mais l’idée est là.

## Inconvénients

Évidemment, cette solution n’est pas parfaite, elle a quelques défauts, mais ils ne me semblent pas insurmontables.

* Déstabilisation : Encore une nouvelle méthode d’évaluation ! Déjà que les points n’ont pas été faciles à adopter et utiliser correctement, là même les développeurs vont avoir du mal à s’y faire.

Je ne serais pas aussi catégorique. Certes, il va falloir de l’accompagnement, mais à terme, cela devrait devenir naturel. Pour assurer la transition et l’intégration de nouvelles personnes, adosser un descriptif rapide de ce que représente chaque unité aide énormément. Pour tout ce qui est de la problématique de suivi et prévision, cela n’est pas si éloigné de ce qui était fait avec les points, utiliser la météo de la veille devrait permettre de faire des projections raisonnables, peut-être pas à très long terme, mais suffisamment en ce qui concerne les méthodes agiles.

* Tâtonnement : Comment on fait en pratique ? On se base sur quoi ? Quel atelier on doit faire ?

Demandez à votre Scrum Master ou votre coach, chaque recette doit être unique. Ce n’est pas une pratique forcément facile et à mettre dans toutes les mains, mais si vous arrivez à ce niveau, vous saurez quoi faire.

* Outils de suivi : Et mon outil de gestion de projet ! Il fait comment pour me générer mes graphes ? Mon burndown ! Mon burnup !

C’est en effet un soucis plus compliqué. Il est nécessaire d’adapter les outils pour qu'ils puissent exploiter ce type d’échelle non numérique tout en évitant de faire apparaître clairement une conversion en numérique justement. Actuellement je ne connais qu’une seule solution qui permettrait ça. Le management visuel ! Prenez un papier & crayon et faites-le vous même. Mais je suis d’accord, pas facile pour des équipes distantes.

## Pour aller encore plus loin

Et pourquoi ne pas aller plus loin encore ? Ne pourrait-on pas succomber au mouvement séduisant du « No Estimate » ? La marche est encore trop haute pour beaucoup d’entreprises. C’est pour cela qu’un premier pas avec une unité personnalisée à chaque projet est une option acceptable et qui va dans le bon sens.

# Conclusion

Faire de l’agilité, à l’état de l’art, est souvent très compliqué et inconcevable pour beaucoup de structures. Se libérer des « jour/mois hommes » et passer aux « story point » est déjà une bonne initiative, mais il ne faut pas tomber dans les travers décrits plus haut. Une fois que l’entreprise a acquis cette culture de l’agilité et veut pousser plus loin l'expérience, casser les comparaisons possibles entre points est une première marche atteignable sans trop d'effort avant d'essayer de se jeter dans le bain du «No estimate».

PS : Ce texte a été initialement écrit en mai 2020, je l’ai volontairement laissé tel quel, car finalement, je partage encore cette opinion.
