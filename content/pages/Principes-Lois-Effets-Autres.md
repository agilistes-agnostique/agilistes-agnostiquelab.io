Title: "Liste de principes, lois et effets utiles"
Date: 2023-05-28
Modified: 2023-07-31
Tags: principes, lois, effets
Keyword: principes, lois, effets, culture
Category: management
Slug: Principes-Lois-Effets-Autres
Author: Matthieu
Description: "liste de divers principes, lois, effets et autres axiomes pouvant aider à la gestion de projets"
Summary: "Avec un peu d’expérience sur un sujet il est facile de dégager de grand principe de fonctionnement. C’est un peu les dictons du monde moderne. Ces principes ou lois s’exerce aussi dans le monde de l’entreprise. Je vais tenter de vous en lister quelques un des plus intéressant, sans rentrer dans le détail (chacun pouvant faire l'objet l'un long billet), mais assez pour que vous puissiez les reconnaitre et les comprendre."
Status: draft
Cover: https://images.unsplash.com/photo-1553484771-cc0d9b8c2b33?auto=format&fit=crop&w=400&q=80

Page regroupant tout les principes, effets, lois et autres axiomes intéressant à connaitre pour la gestion de projets. Les explication ne sont absoluments pas complète mais devrait vous servir de pointeurs pour approfondir le sujet.

# Avant propos

Précision de vocabulaire pour comprendre de quoi on parle :
* [Loi](https://fr.wiktionary.org/wiki/loi) : Règle, obligations écrites, prescrites ou tacites, auxquelles les personnes se doivent de se conformer. (Sciences) Postulat ou énoncé vrai sous certaines conditions. (Physique) Ce qui règle l’ordre du monde physique. 

* [Principe](https://fr.wiktionary.org/wiki/principe) : Première règle d’un art ou d'une discipline. (Sciences) Loi que certaines observations ont d’abord rendue vraisemblable et à laquelle on a donné ensuite la plus grande généralité. (Courant) Règle de conduite d'une personne ou d'un groupe.

En science « dure » la distiction est assez simple puisqu'une loi ne souffre pas d'exception *(a moins que les cas particuliers ne soient intégrés à la loi)*.
Dans notre cas la différence est plus subtile. D’un côté avec les *lois* on a une règle de fonctionnement venant *d’en haut*, dicté de manière arbitraire et parfois suite à une intuition, alors que le *principe* vient *d’en bas* dans le sens ou c’est par l’accumulation d’observations ou d’expérimentations qu’on le façonne. Mais au final on se retrouve dans la même situation avec une directive qui nous indique un fonctionnement. La frontière étant assez fine, la répartition entre les 2 est assez aléatoire. J’ai pris le parti de suivre ce qui est le plus usité, même si j'ai constaté que le terme « Loi » était souvent préféré sans raison particulière.

* [Effet](https://fr.wiktionary.org/wiki/effet) : Ce qui est produit par quelque cause. Là on est plus sur la mise en évidence de levier de comportement. Parfois activable sur commande, parfois implicitement ou automtiquement.

Un autre mot qui découle de l’utilisation de ces termes est « Corollaire ».

* [Corollaire](https://fr.wiktionary.org/wiki/corollaire) : Du latin « corollarium » qui veux dire « petite couronne ». C’est donc une extension directe de la loi / principe / effet susnommé.

# Principes
* [Principe Pareto](https://fr.wikipedia.org/wiki/Principe_de_Pareto) ou « la règle du 80/20 » : Principe assez simple qui dit que « 80 % des effets sont le produit de 20 % des causes ». Si on applique ça au monde du travail, vous obtiendrez 80% du résultat attendu avec 20% de l’effort. Son corollaire étant que les 20% restant vous prendront 80% de votre effort, donc clairement à remettre en question pour s'assure que le ration bénéfice / effort en vaut la chandelle. (« As-t-on vraiment besoin de cette fonctionnalité ? »)

<u>Outils en lien :</u> Des outils de priorisation classiques peuvent aider à se concentrer sur les 20% les plus utiles. Par exemple la [matrice d'Eisennhower](https://fr.wikipedia.org/wiki/Matrice_d%27Eisenhower). Des outils se concentrant sur la valeur métiers serait aussi intéressant, tel un [MoSCoW](https://fr.wikipedia.org/wiki/M%C3%A9thode_MoSCoW) ou le plus polémique [WSJF](https://wikiagile.fr/index.php?title=WSJF_(SAFe))

* [Principe de Peter](https://fr.wikipedia.org/wiki/Principe_de_Peter) : Vous avez surement déjà rencontré quelqu'un que vous pensez ne pas avoir les compétences suffisantes pour son poste. C'est l'illustration parfaite de ce principe qui dit que « dans une hiérarchie, tout employé a tendance à s'élever à son niveau d'incompétence », avec pour corollaire que « avec le temps, tout poste sera occupé par un employé incapable d'en assumer la responsabilité ». 
Pour être un peu plus concret, un bon dev/ouvrier/commercial/… ne fait pas forcément un bon manager/responsable de service/directeur d'agence/… Ce sont souvent des métiers différents et si l'envie de la personne concernée n'est pas là il ne faut pas l'y obliger. Malheureusement ce genre de promotion est souvent l'unique moyen d'améliorer sa situation (financière, influence, reconnaissance, …) au sein de l'entreprise. Des solutions existent. La plus facile est de *promouvoir* un *incompétent* a un poste d'apparence plus prestigieux mais en réalité moins impactant pour l'entreprise. Solution que je trouve *cache misère* et qui fait porter un poids mort à l'entreprise (un peu mieux expliqué [ici]( https://eventuallycoding.com/2013/11/27/principe-de-peter-et-corollaire-darchimede)). Heureusement des idées plus pertinentes sont déjà en place chez certains, comme des parcours spécialisés qui englobe l'ensemble d'une carrière, sans changer de domaine ([voir ce billet](https://eventuallycoding.com/2021/06/24/senior-avec-6-ans-dexperience-et-apres))
Si vous voulez approfondir ce principe, [le livre original](https://www.babelio.com/livres/Peter-Le-principe-de-Peter/1271105) est toujours édité.
	* [Couplage avec la loi normale](https://fr.wikipedia.org/wiki/Principe_de_Peter#La_d%C3%A9foliation_hi%C3%A9rarchique) : (La loi normale étant expliqué plus bas) Heureusement tous les postes ne sont pas occupés par des « incompétents » et statistiquement on retrouve la répartition normale suivante :
		* 10 % d'employés sont super-compétents
		* 20 % d'employés sont compétents
		* 40 % d'employés sont modérément compétents
		* 20 % d'employés sont incompétents
		* 10 % d'employés sont super-incompétents

	* [Extension Dilibert](https://fr.wikipedia.org/wiki/Principe_de_Dilbert) : Vous connaissez peut-être (le comic strip Dilibert)[http://www.dilbert.com/] qui illustre toute les dérives possibles de la mauvaise gestion d'entreprise via son personnage principal Dilibert. Le principe s'énonce ainsi : « Les gens les moins compétents sont systématiquement affectés aux postes où ils risquent de causer le moins de dégâts : ceux de managers. » 

<u>Outils en lien :</u> Un moyen très simple est de ne pas avoir de hiérarchie ^^. Cependant au delà d'une certaine taille d'organisation ça devient indispensable. À ce moment là on peut s'inspirer de l'holacratie avec l'[élection sans candidat](https://instantz.org/election-sans-candidat/). Un accompagnement et formation sont 2 leviers évidents pour contrer ce principe.

* [Principe du plafond de verre](https://fr.wikipedia.org/wiki/Plafond_de_verre) : Principe de plus en plus médiatisé et souvent lié au critère de parité, mais son application totalement générique. Il désigne le fait que les postes les plus *prestigieux* soient réservés à une caste bien particulière. Le critère de sélection pouvant être très divers, le plus visible étant le sexe, mais on peut aussi trouver la couleur de peau, la nationalité, le cursus scolaire/type de diplôme, la religion, etc. Il est impossible de faire un listing exhaustif, donc je vous laisse imaginer la suite. Bien sûr le *critère* de sélection peut être multiplet et en mixer plusieurs avec une pondération propre.
	* [Extension falaise de verre](https://fr.wikipedia.org/wiki/Falaise_de_verre) : Principe moins connus mais tout aussi pernicieux. C’est au moment où les rats quittent le navire que l’on constate ce principe. Ces fameux postes inaccessibles le deviennent miraculeusement lors de période de crise grave. Les postes très exposés médiatiquement tel que la politique en donne [tout un florilège]( https://fr.wikipedia.org/wiki/Falaise_de_verre#Exemples_politiques) (ex : Marissa Mayer@Brexit 2016). Mais les grandes entreprises ne sont pas épargnées non plus (ex : Marissa Mayer@Yahoo 20212) ou les musées (ex : Laurence des Cars@Louvres 2021). Ce principe a été établis suite à [l’étude]( https://onlinelibrary.wiley.com/doi/abs/10.1002/9781118785317.weom110287) de [Clara Kulich]( https://www.unige.ch/fapse/psychosociale/collaborateurs/kulich) qui trouvait étrange [la statistique du Times]( https://www.psychologie.uni-frankfurt.de/58476021/Generic_58476021.pdf) de sous performance des entreprises dirigées par des femmes. Merci à elle.

<u>Outils en lien :</u>Sujet délicat sans réponse magique. Dans le domaine de l'IT la parité est souvent quasiment impossible à avoir, c'est donc aux managers d'avoir un point d'attention voire une métrique spécifique sur le sujet, sans pour autant tomber dans la *discrimitation positive* exagérée qui pourrait être male perçue par les 2 parties.

# Lois
* [Loi de Murphy](https://fr.wikipedia.org/wiki/Loi_de_Murphy) ou Loi de l'emmerdement maximal (LEM) : C’est ma petite préférée celle-là. « Si cela peut mal se passer, cela arrivera » On doit cette formulation à [George Nichols](https://fr.wikipedia.org/wiki/George_Nichols), celle du [professeur Murphy](https://fr.wikipedia.org/wiki/Edward_A._Murphy_Jr.) étant « Si ce gars a la moindre possibilité de faire une erreur, il la fera » Tout cela à cause d'un assistant qui monta à l'envers TOUS les capteurs de l'expérience en cours. Il y avait au moins une certaine constance dans son action.
Il y a plein de corolaire et d'extension à cette loi, mais si je devais en retenir 2 particulièrement ça serais celles-là :
	* Corolaire de Finagle « Tout ce qui peut aller mal le fera au pire moment. »
	* Loi des séries : « Les emmerdes c’est comme les cons, ça vole toujours en escadrille. »  Jacques Chirac
Je vous fais grâce de vous les expliquer, elles sont assez parlantes d'elle-même.



* [Loi de puissance](https://fr.wikipedia.org/wiki/Loi_de_puissance) : Très utilisé en sciences « dur », elle a cependant une application concrète en sociologie. Elle permette de décrire tous les phénomènes qui présentent une invariance d'échelle. Par exemple les sites de type wikis suivent la règle des 90-9-1 : 90 % de la population utilisatrice ne contribue pas ; 9% sont des contributeurs occasionnels et 1% des visiteurs du site contribue régulièrement. Ces proportions ne changeant pas que vous vous appeliez « Wikipédia » ou « LeWikiDuTressageDeRoseau ». Ou plus près de notre domaine qu’est la gestion de projet Agile, que vous gériez une équipe de 9 personnes ou 900, la répartition de vos problématiques ne changeront qu’à la marge.

* [Loi de Pareto](https://fr.wikipedia.org/wiki/Loi_de_Pareto) : Quoi ? Encore lui ? Eh bien oui. Le principe ayant une application assez générique, la loi elle est plus rigoureuse et est utilisé en sciences physique et sociales. C’est un type particulier de la loi de puissance. Cet outil met en évidence la loi des 80/20. Autrement dit, agir sur 20 % de causes permet de résoudre 80 % du problème. Le Pareto est utile pour identifier sur quelle cause agir en priorité pour améliorer de façon significative la situation. 

* [Loi normale](https://fr.wikipedia.org/wiki/Loi_normale) (ou courbe de Gauss) : Issue du domaine des probabilités et statistique elle permet de modéliser des phénomènes natures issus de plusieurs évènements aléatoires. Prenez l’espérance mathématique (le résultat le plus probable) (noté : μ) d’un phénomène croisé avec son écart type (σ) et vous obtenez la répartition des résultats. En image ça donne ça :
![Distribution normal](https://upload.wikimedia.org/wikipedia/commons/thumb/7/74/Normal_Distribution_PDF.svg/720px-Normal_Distribution_PDF.svg.png){.image}
La hauteur des courbes représentant le nombre d’occurrence du résultat obtenu. 
Dans un processus de transformation (agile) d’une organisation vous pourriez en déduire le nombre de personne contre, neutre ou favorable avec un petit échantillon du personnel de l’entreprise de base, puis une extrapolation à l’ensemble de l’entreprise.

* [Loi de Parkinson](https://fr.wikipedia.org/wiki/Loi_de_Parkinson) : Énoncé en 1955 par Cyril Northcote Parkinson dans un article publié dans la revue *The Economist*, elle a été formulé ainsi « Le travail s’étend de manière à remplir le temps disponible pour son achèvement. […] ». Quel que soit le temps que vous (vous) accordez pour la réalisation d’une tâche, l’intégralité de ce temps sera consommée. C’est l’une des raisons pour lequel Scrum et d’autre méthodes Agiles se fixe des bulle temporel (timebox) et un rythme particulier. En plus d’éviter les effets tunnels, ça évite surtout de se perdre dans des détails (Pareto) de moindre importance.
	* [Corolaire futilité de Parkinson](https://fr.wikipedia.org/wiki/Loi_de_futilit%C3%A9_de_Parkinson) : L’idée est que les organisations donnent une importance bien plus important sur des détails secondaires qu’au réel problème de fond. Typiquement dans une réunion pour la construction d’un abri à vélo, les discutions autour de sa couleur occupera bien plus de temps que son emplacement ou sa capacité. Cela peut s’expliquer par le fait que s’exprimer sur des caractéristiques essentielles implique une prise de responsabilité proportionnel et donc une mise en cause directe si un mauvais choix a été fait. Alors que la couleur n’implique qu’une responsabilité minime et au pire avoir « mauvais goût ».

* [Loi de Hofstadter](https://fr.wikipedia.org/wiki/Loi_de_Hofstadter) : Elle vient en complément de la loi de Parkinson et s’énonce ainsi « Il faut toujours plus de temps que prévu, même en tenant compte de la loi de Hofstadter. ». Je vous laisse en déduire les implications dans la planification et estimation des projets. Jetez un œil au *Facteur Pi* un peu plus bas si vous n’avez pas encore perdu la foi dans ce genre d’exercice.

* [Loi de Douglas]() : C'est la version « physique » de la loi de Parkinson, qui peut être résumé par l'addage « Plus on a de la place, plus on s’étale ». Que l'on ai un petit bureau d'écolier ou un grand bureau d'angle de 3m, on aura toujours tendence à en occuper l'intégralité de la surface disponible. *L'origine de cette loi est assez brumeuse et je n'ai pas trouvé pourquoi elle porte ce nom*

* [Loi de Carlson](https://www.helloworkplace.fr/loi-carlson/) : On doit cette loi à [Sune Carlson](https://en.wikipedia.org/wiki/Sune_Carlson) qui dans son livre « Executive Behaviour » publié en 1951 qui dit : « un travail réalisé en continu prend moins de temps et d’énergie que lorsqu’il est réalisé en plusieurs fois ». Cela exprime une évidence sur le fait que plus l'on est intérrompu et moins nous sommes efficace. La [technique du pomodoro](https://fr.wikipedia.org/wiki/Technique_Pomodoro) est souvent cité pour combattre cette loi. Je vous conseil aussi le « Jeu des prénoms » ([1](https://docs.google.com/presentation/d/18VsHRMqxQshqrFPVp_oCD8w6WDzJSMZaizMLeYvdHWg/edit#slide=id.g518d6b0fe_0_0))([2](https://oyomy.fr/2022/09/le-jeu-des-prenoms/)) pour expérimenter cette loi.

* [Loi de Laborit](https://www.helloworkplace.fr/loi-laborit/) : Ou « loi du moindre effort », nous la devons à un Français, [Henri Laborit](https://fr.wikipedia.org/wiki/Henri_Laborit). La [Matrice d'Eisenhower](https://fr.wikipedia.org/wiki/Matrice_d%27Eisenhower) est un très bon outil pour palier au problèmes qu'entrainerait cette loi.

* [loi d'Illich](https://www.observatoire-ocm.com/management/loi-illich/) :

* [Loi de Segal](https://en.wikipedia.org/wiki/Segal%27s_law) [Fr] : 

* [Loi de Fraisse](https://www.hellowork.com/fr-fr/medias/loi-fraisse-activite-gestion-temps.html): Nous viendrait du psychologe Français, [Paul Fraisse](https://fr.wikipedia.org/wiki/Paul_Fraisse).

* [Loi de Taylor](http://pragma-tic.blogspot.com/2013/08/lois-generales-du-temps-la-loi-de-taylor.html) :

* [Loi de Swoboda-Fliess-Teltscher](https://nospensees.fr/la-loi-de-swoboda-fliess-teltscher-et-les-cycles-biologiques/) : Même si cette « Loi » a été démenti depuis je préfère la cité puisqu'on la rencontre encore assez souvent. Succintement, ces 3 professeurs pensait avoir identifier des cycle dans les performence intéllectuel

* [La loi du mouvement de Newton](https://fr.wikipedia.org/wiki/Lois_du_mouvement_de_Newton) : Pourquoi parler de Newton et d'une loi physique ici ? (c'est pas celle de la gravité) Et bien c'est qu'elle s'applique aussi au comportement humain. La loi s'énnonce ainsi : 
> « Tout corps persévère dans l'état de repos ou de mouvement uniforme en ligne droite dans lequel il se trouve, à moins que quelque force n'agisse sur lui, et ne le contraigne à changer d'état. »

Pour l'appliquer dans notre domaine il suffit de la changer que légèrement pour obtenir ceci :
> « Tout ~~corps~~ *comportement* persévère dans l'état de repos ou de mouvement ~~uniforme en ligne droite~~ dans lequel il se trouve, à moins que quelque force n'agisse sur lui, et ne le contraigne à changer d'état. »

Et en effet si on ne fait rien pour combattre une mauvaise habitude elle perdurera. De même pour changer la culture d'une équipe/entreprise il faut lui ancrer une habitude, qui ne peut être fait qu'en impulsant un mouvement. En tant qu'agent du changement appliquer une force inférieur à l'inertie du système est voué à l'échec, mais cet effort doit être dosé un fonction de l'acceptabilité du changement attendu.

La loi de Kotter

La loi de Brooks

Loi de Perls

Loi de l’alternance

# Effets

* [Effet Domino](https://fr.wikipedia.org/wiki/Effet_domino) : Un des plus connu et plus simple. Il s'agit d'une réaction en chaine d'évènements de même ampleur, physiquement ou thématiquement proche. Ces changements peuvent être voulus, maitrisés, délimités, … ou pas.

* [Effet Papillon](https://fr.wikipedia.org/wiki/Effet_papillon) : On pourrait le considérer comme un corrolaire de l'effet Domino. La différence majeur est l'introduction du ratio entre la suite d'évènements. L'idée est qu'en changeant très légèrement les conditions initiale d'un système, le résultat peu être tout autre. C'est Edward Lorenz qui est retenu comme le père de cette expression, l'ayant utilisé comme titre d'une conférence scientifique en 1972, sous la forme suivante :

> « Le battement d'ailes d'un papillon au Brésil peut-il provoquer une tornade au Texas ? »


* [Effet Dunning Kruger](https://fr.wikipedia.org/wiki/Effet_Dunning-Kruger) : Aussi connu sous le nom d’effet de « sur-confiance », qui n’est autre qu’un biais cognitif de surestimation de la maitrise d’un sujet. Des citations me viennent à l’esprit pour illustrer cet effet.

> « L’ignorant affirme, le savant doute, le sage réfléchit. » 
*Aristote*

> « Ceux qui savent ne parlent pas, ceux qui parlent ne savent pas. Le sage enseigne par ses actes, non par ses paroles. »
*Lao Tseu*

> « Certaines personnes écoutent la moitié, comprennent un quart, mais explique le double. » 
*Inconnu*

Une jolie image permet de bien comprendre aussi :
![Effet Dunning-Kruger](https://upload.wikimedia.org/wikipedia/commons/7/75/2019-06-19_effet_dunning_kruger.png){.image .width-75}

* [Effets Pygmalion](https://fr.wikipedia.org/wiki/Effet_Pygmalion) :

* [Effets Golem](https://fr.wikipedia.org/wiki/Effet_Golem) :

* [Effets Hawthorne](https://fr.wikipedia.org/wiki/Effet_Hawthorne) :

# Autres
Ces autres cas ne sont pas énoncés sous forme de principe ou lois mais restent tout de même intéressant à connaitre.

* [Facteur Pi](https://wikiagile.fr/index.php?title=La_constante_PI_dans_l%27estimation_logicielle) : On parle bien du chiffre 3,14… Alistair Cockburn lui-même en a fait un [article sur son blog](http://alistair.cockburn.us/The+magic+of+pi+for+project+managers) ([Fr](https://www.les-traducteurs-agiles.org/2017/12/06/la-magie-de-pi.html). Ce facteur essaye de deviner le coût réel d’un projet suivent l’estimation des dev. Pour ça il suffit de prendre le chiffre donné par ces derniers et le multiplier par π, π², ou √π (lisez l’article précédent pour choisir). En général il en résulte un projet à succès qui a fini dans les temps voir en avance !


* [Rasoir d’Ockham](https://fr.wikipedia.org/wiki/Rasoir_d%27Ockham) : Aussi désigné par principe de simplicité, principe d'économie ou principe de parcimonie. Mais le rasoir étant plus rependu je l’ai préféré au classement dans les « Principes ». Mais au fait pourquoi « rasoir » ? Rien à voir avec la tonsure du moine Ockham qui l’a formulé en 1319, mais comme le principe philosophique d’élimination (« raser »).
> Pluralitas non est ponenda sine necessitate
> (les multiples ne doivent pas être utilisés sans nécessité)

Une reformulation plus moderne serait « les hypothèses suffisantes les plus simples doivent être préférées ».
Intuitivement des dictons viennent aussi à l’esprit pour l’illustrer, comme « L'explication la plus simple est généralement la bonne » ou « Pourquoi chercher compliqué quand plus simple suffit ? »

* [Rasoir de Hanlon](https://fr.wikipedia.org/wiki/Rasoir_de_Hanlon) : C’est presque un corolaire au rasoir précédent et s’énonce ainsi :
> « Ne jamais attribuer à la malveillance ce que la bêtise suffit à expliquer. »

Une reformulation savoureuse l’illustre tout aussi bien.
> « Toujours préférer l'hypothèse de la connerie à celle du complot. La connerie est courante. Le complot exige un esprit rare »

*Michel Rocard*
N’en déplaise aux amateurs aux théories du complot, il est très difficile d’en échafauder et [encore plus de les conserver]( https://www.slate.fr/story/113255/complots-rester-secrets)([En]( https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0147905)).

* [Nombre de Dunbar](https://fr.wikipedia.org/wiki/Nombre_de_Dunbar) : 150 ! C’est le nombre totalement arbitraire et avec une *petite* variance de 50%, qu’a sorti le chercheur Robin Dunbar et faisant établissant une corrélation entre la taille du néocortex cortex et le nombre d’individus dans des groupes de primates … extrapolé aux humains. Dans le genre *estimation au doigt mouillé* on est pas mal. Bref, ça serait le nombre de relation de *confiance* qu’un individu pourrait entretenir.
Heureusement d’autre études un poil plus sérieuse ([Twitter](https://www.numerama.com/business/18932-la-taille-du-cerveau-determine-le-nombre-d-amis-sur-twitter.html) / [Facebook](https://www.abc.net.au/news/science/2016-01-20/150-is-the-limit-of-real-facebook-friends/7101588)) l’on confirmé … [ou pas](https://royalsocietypublishing.org/doi/10.1098/rsbl.2021.0158).

* [Hypothèse de la vitre brisée](https://fr.wikipedia.org/wiki/Hypoth%C3%A8se_de_la_vitre_bris%C3%A9e) : Plus rependu en science social, je pense qu’elle a toute sa place dans notre domaine. Cette analogie explique qu’intuitivement les individus sont moins enclins au respect s’ils constatent une dégradation du sujet en question. Inversement, les incivilités diminueront en corrélation au niveau de soin apporté. 
Pourquoi s’embêter à refaire toute une partie de code si un petit *if* dans un coin permet de faire ce que je veux ? Un ticket mal écrits de plus n’est pas grave au vu du tas déjà présent … non ?
Vous aurez compris les exemples. Il est donc important d’être rigoureux, de ne tolérer que le strict minimum d’écart, mais surtout d’être réactif face à ces incivilités.

* [Management des 3%](https://hugues.le-gendre.com/almanach/09/20-manager-pour-les-3-pourcents/) : Notre métier étant très humain, je trouve cet effet intéressant malgré le fait que l’on parle de management. Il tente de palier au fait de vouloir trouver des règles qui puisse régir toutes les personnes et processus. Il part du principe que dans toute organisation il y aura forcément un certain nombre de personnes qui tenteront de contourner ou mettre à leur profit une règle ou une absence de règle. Chercher à bloquer ces *3%* de *déviants* consumera bien plus que *3%* d’énergie, mais surtout génèrera une grande frustration de la part des 97% restant se sentant inutilement encadrer par une montagne de procédure et règles. 

* [Théorie de l'étiquetage](https://fr.wikipedia.org/wiki/Th%C3%A9orie_de_l%27%C3%A9tiquetage) : 
