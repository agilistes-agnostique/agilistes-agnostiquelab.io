#!/usr/bin/env python
# -*- coding: utf-8 -*- #

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

# If your site is available via HTTPS, make sure SITEURL begins with https://
SITEURL = 'https://anargile-leanbertaire.info'
RELATIVE_URLS = False

FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/{slug}.atom.xml'

DELETE_OUTPUT_DIRECTORY = True

# Conf pour le suivi des visites
MATOMO_URL='stats.anargile-leanbertaire.info'
MATOMO_SITE_ID='3'

# Pour l’instant pas de lien social ou lien de contact
SOCIAL = (
#    ('twitter', 'https://twitter.com/'),
#    ('linkedin', 'https://www.linkedin.com'),
#    ('github', 'https://github.com/'),
#    ('gitlab', 'https://gitlab.com/'),
#    ('facebook', 'https://facebook.com'),
#    ('instagram', 'https://instagram.com')
)

CONTACT = (
#    ('envelope', '<a href="mailto:contact@lerteco.fr?Subject=Contact&Body=Bonjour,%0AJe%20voudrais%20en%20apprendre%20plus%20sur%20votre%20blog">contact@lerteco.fr</a>'),
#    ('phone','+01 345-345-345'),
#    ('home', '42 rue de l’oublie, Moncug')
)
